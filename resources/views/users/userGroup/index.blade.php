@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
              <div class="card-header">
                  <h4 class="card-title float-left">Users Group</h4>
                  <a href="{{url('users/group/create')}}" class="btn btn-sm btn-danger float-right" style="color:white;"><span class="fa fa-plus fa-sm"></span>&nbsp;&nbsp;Add Group</a>
              </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" style="width:100%">
                    <thead style="">
                      <tr>
                        <th>No</th>
                        <th>Role</th>
                        <th>Deskripsi</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

function checkDeleteAkses(id){

  $.ajax({
      url : "{{ url('/users/group/checkingDelete') }}/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        console.log(data['info']);
        if(data['info']=="Error"){

          $.confirm({
            title: data['info'],
            content: data['alert'],
            type: data['colors'],
            icon: data['icons'],
            typeAnimated: true,
            buttons: {
                close: function () {
                }
              }
            });

        }else{
          deletes(id);
        }
      },
      error: function (jqXHR, textStatus, errorThrown){
          var errorMsg = 'Ajax request failed stock with: ' + errorThrown;
          console.log(errorMsg);
      }
  });

}

function deletes(id){
  $.confirm({
      title: 'Confirmation',
      content: 'Delete this record ?',
      icon: 'fas fa-exclamation-triangle',
      buttons: {
	  cancel: function () {
          },
          confirm: function () {

		$.ajax({
                url: "{!! url('users/group/countUsers') !!}/" + id,
                data: {},
                dataType: "json",
                type: "get",
              success:function(datas)
                {
                  console.log(datas);
                  if(datas=="ada"){
                    $.confirm({
                        title: 'Information',
                        content: "Can't delete User Group. Users already exist!",
                        buttons: {
                            ok: function () {
                            },
                        }
                    });
                  }else{

                    location.href="{{url('users/group/delete')}}" + "/" + id;
                  }

                }
            });

          }
      }
  });
}

$(document).ready(function() {

var table = $("#lookup").dataTable({
  processing: true,
  serverSide: true,
  "lengthMenu": [[15,25,50,100], [15,25,50,100]],
  "pageLength": 15,
  ajax:{
    url: "{{ url('users/group/getUsers') }}",
    dataType: "json",
    type: "GET",
    error: function(){  // error handling
      $(".lookup-error").html("");
      $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="4">No data found in the server</th></tr></tbody>');
      $("#lookup_processing").css("display","none");

    }
  },
  columns: [
            {data: 'id'},
            {data: 'role_name'},
            {data: 'description'},
            {data: 'id'}
        ],
        columnDefs: [
          {
            "targets": [0],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).text(row+1);
            },
            orderable: false
          },
          {
            "targets": [3],
            "createdCell": function (td, cellData, rowData, row, col) {
              $(td).empty();
              $(td).addClass("text-center");
              if(rowData.id==1 || rowData.id=="1"){  
              }else{
                  $(td).append($('@include('inc.button.btnEditDelete')'));
              }
            },
            orderable: false
          },
        ],
    createdRow: function ( row, data, index ) {
        $(row).attr('id','table_'+index);
    }
      });

    $('.table').on('click','.btn-edit-record', function(){
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData()

        location.href="{{url('users/group/edit/')}}" + "/" + data[index].id;
    });

    $('.table').on('click','.btn-remove-record', function(){
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];
        var data = table.fnGetData()

        checkDeleteAkses(data[index].id);
    });
});
</script>
@endsection
