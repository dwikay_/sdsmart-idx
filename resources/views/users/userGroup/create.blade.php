@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Administrator</h4>
              <p class="card-description">
                User Group
              </p>
              <form id="fpro" class="forms-sample" action="{{url('users/group/store')}}" method="post">
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Role Name</label>
                      <input type="text" class="form-control form-control-sm" id="role_name" name="role_name" placeholder="" required>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="exampleInputEmail1" class="col-form-label">Description</label>
                      <input type="text" class="form-control form-control-sm" id="description" name="description" placeholder="" required>
                    </div>
                  </div>
                  <div class="col-md-12 grid-margin stretch-card mt-4">
                    <div class="card">
                      <div class="card-body">
                        <p class="card-description">
                          Users Privilege
                        </p>
                            <div class="box-tab" id="mtab2">
                                              <ul class="nav nav-tabs tab-basic">
                                                  <li class="nav-item active" id="91"><a class="nav-link active" href="#pospengaturan" data-toggle="tab">Monitoring</a></li>
                                                  <li class="nav-item" id="92"><a class="nav-link" href="#pospenjualan" data-toggle="tab">Management Jobs</a></li>
                                                  <li class="nav-item" id="93"><a class="nav-link" href="#posinventory" data-toggle="tab">Log Manager</a></li>
                                                  <li class="nav-item" id="94"><a class="nav-link" href="#poslaporan" data-toggle="tab">Administrator</a></li>
                                              </ul><br>
                                              <div class="tab-content text-center">
                                                  <div class="tab-pane fade in active show" id="pospengaturan">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m">
                                                              <thead>
                                                              <tr class="text-white" style="background:#A33333">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                              <tr id='minimal-checkbox-9999999991'>
                                                                  <td>Jobs</td>
                                                                  <td><input name="9999999991_read" type="checkbox" id="minimal-checkbox-9999999991" value="9999999991"></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999991" value="all" name="all" onChange="check(9999999991)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                                  <div class="tab-pane fade in" id="pospenjualan">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m" id="tableConfig">
                                                              <thead>
                                                              <tr style="background:#A33333" class="text-white">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                              <tr id='minimal-checkbox-9999999992'>
                                                                  <td>DB Config</td>
                                                                  <td><input class="9999999992_read" name="9999999992_read" type="checkbox" id="minimal-checkbox-9999999992" value="9999999992"></td>
                                                                  <td><input class="9999999992_create" name="9999999992_create" type="checkbox" id="minimal-checkbox-9999999992" value="9999999992"></td>
                                                                  <td><input class="9999999992_update" name="9999999992_update" type="checkbox" id="minimal-checkbox-9999999992" value="9999999992"></td>
                                                                  <td><input class="9999999992_delete" name="9999999992_delete" type="checkbox" id="minimal-checkbox-9999999992" value="9999999992"></td>
                                                                  <td><input type="checkbox" id="all-9999999992" value="all" name="all" onChange="check(9999999992)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-9999999993'>
                                                                  <td>Task Configuration</td>
                                                                  <td><input class="9999999993_read" name="9999999993_read" type="checkbox" id="minimal-checkbox-9999999993" value="9999999993"></td>
                                                                  <td><input class="9999999993_create" name="9999999993_create" type="checkbox" id="minimal-checkbox-9999999993" value="9999999993"></td>
                                                                  <td><input class="9999999993_update" name="9999999993_update" type="checkbox" id="minimal-checkbox-9999999993" value="9999999993"></td>
                                                                  <td><input class="9999999993_delete" name="9999999993_delete" type="checkbox" id="minimal-checkbox-9999999993" value="9999999993"></td>
                                                                  <td><input type="checkbox" id="all-9999999993" value="all" name="all" onChange="check(9999999993)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-9999999994'>
                                                                  <td>Manual Job</td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input name="9999999994_update" type="checkbox" id="minimal-checkbox-9999999994" value="9999999994"></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999994" value="all" name="all" onChange="check(9999999994)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                                  <div class="tab-pane fade in" id="posinventory">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m">
                                                              <thead>
                                                              <tr class="text-white" style="background:#A33333">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                              <tr id='minimal-checkbox-9999999995'>
                                                                <td>View Log Jobs</td>
                                                                <td><input name="9999999995_read" type="checkbox" id="minimal-checkbox-9999999995" value="9999999995"></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td><input type="checkbox" id="all-9999999995" value="all" name="all" onChange="check(9999999995)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-9999999996'>
                                                                  <td>Backup Log Jobs</td>
                                                                  <td><input name="9999999996_read" type="checkbox" id="minimal-checkbox-9999999996" value="9999999996"></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-9999999996" value="all" name="all" onChange="check(9999999996)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                                  <div class="tab-pane fade in" id="poslaporan">
                                                      <div class="table-responsive">
                                                          <table class="table table-striped table-bordered responsive no-m">
                                                              <thead>
                                                              <tr class="text-white" style="background:#A33333">
                                                                  <th class="">Module</th>
                                                                  <th class="text-center">Read</th>
                                                                  <th class="text-center">Create</th>
                                                                  <th class="text-center">Update</th>
                                                                  <th class="text-center">Delete</th>
                                                                  <th class="text-center">Check All</th>
                                                              </tr>
                                                              </thead>
                                                              <tbody class="">
                                                              <tr id='minimal-checkbox-9999999997'>
                                                                  <td>User Management</td>
                                                                  <td><input class="9999999997_read" name="9999999997_read" type="checkbox" id="minimal-checkbox-9999999997" value="9999999997"></td>
                                                                  <td><input class="9999999997_create" name="9999999997_create" type="checkbox" id="minimal-checkbox-9999999997" value="9999999997"></td>
                                                                  <td><input class="9999999997_update" name="9999999997_update" type="checkbox" id="minimal-checkbox-9999999997" value="9999999997"></td>
                                                                  <td><input class="9999999997_delete" name="9999999997_delete" type="checkbox" id="minimal-checkbox-9999999997" value="9999999997"></td>
                                                                  <td><input type="checkbox" id="all-9999999997" value="all" name="all" onChange="check(9999999997)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-9999999998'>
                                                                  <td>User Group</td>
                                                                  <td><input class="9999999998_read" name="9999999998_read" type="checkbox" id="minimal-checkbox-9999999998" value="9999999998"></td>
                                                                  <td><input class="9999999998_create" name="9999999998_create" type="checkbox" id="minimal-checkbox-9999999998" value="9999999998"></td>
                                                                  <td><input class="9999999998_update" name="9999999998_update" type="checkbox" id="minimal-checkbox-9999999998" value="9999999998"></td>
                                                                  <td><input class="9999999998_delete " name="9999999998_delete" type="checkbox" id="minimal-checkbox-9999999998" value="9999999998"></td>
                                                                  <td><input type="checkbox" id="all-9999999998" value="all" name="all" onChange="check(9999999998)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-9999999999'>
                                                                <td>User Activities Log</td>
                                                                <td><input name="9999999999_read" type="checkbox" id="minimal-checkbox-9999999999" value="9999999999"></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td><input type="checkbox" id="all-9999999999" value="all" name="all" onChange="check(9999999999)" /></td>
                                                              </tr>
                                                              <tr id='minimal-checkbox-99999999910'>
                                                                  <td>Backup User Activities Log</td>
                                                                  <td><input name="99999999910_read" type="checkbox" id="minimal-checkbox-99999999910" value="99999999910"></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td></td>
                                                                  <td><input type="checkbox" id="all-99999999910" value="all" name="all" onChange="check(99999999910)" /></td>
                                                              </tr>
                                                              </tbody>
                                                          </table>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                    </div>
                  </div>
                </div>
                    <div class="col-md-12 mt-10 text-right">
                      <a class="btn btn-sm btn-danger btn-save" id="btn-save" style="color:white"><span class="fa fa-floppy-o"></span>&nbsp;Save Group</a>
                      <a class="btn btn-sm btn-primary" href="{{url('users/group')}}" style="color:white"><span class="fa fa-arrow-left"></span>&nbsp;Cancel</a>
                    </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

$('input[type="checkbox"]').css('cursor','pointer');

$('#tableConfig').on('click', '.9999999992_create', function() {$('.9999999992_read').prop('checked',true);});
$('#tableConfig').on('click', '.9999999992_update', function() {$('.9999999992_read').prop('checked',true);});
$('#tableConfig').on('click', '.9999999992_delete', function() {$('.9999999992_read').prop('checked',true);});

$('#tableConfig').on('click', '.9999999993_create', function() {$('.9999999993_read').prop('checked',true);});
$('#tableConfig').on('click', '.9999999993_update', function() {$('.9999999993_read').prop('checked',true);});
$('#tableConfig').on('click', '.9999999993_delete', function() {$('.9999999993_read').prop('checked',true);});

$('#tableConfig').on('click', '.9999999997_create', function() {$('.9999999997_read').prop('checked',true);});
$('#tableConfig').on('click', '.9999999997_update', function() {$('.9999999997_read').prop('checked',true);});
$('#tableConfig').on('click', '.9999999997_delete', function() {$('.9999999997_read').prop('checked',true);});

$('#tableConfig').on('click', '.9999999998_create', function() {$('.9999999998_read').prop('checked',true);});
$('#tableConfig').on('click', '.9999999998_update', function() {$('.9999999998_read').prop('checked',true);});
$('#tableConfig').on('click', '.9999999998_delete', function() {$('.9999999998_read').prop('checked',true);});

function check(no) {
            document.getElementById('minimal-checkbox-'+no).addEventListener('change', function(e) {
                var el = e.target;
                var inputs = document.getElementById('minimal-checkbox-'+no).getElementsByTagName('input');

                if (el.id === 'all-'+no) {
                    for (var i = 0, input; input = inputs[i++]; ) {
                        input.checked = el.checked;
                    }
                } else {
                    var numChecked = 0;

                    for (var i = 1, input; input = inputs[i++]; ) {
                        if (input.checked) {
                            numChecked++;
                        }
                    }
                    inputs[0].checked = numChecked === inputs.length - 1;
                }
            }, false);
        }

        $('#btn-save').on('click', function(){
          $('#fpro').submit();
        });
</script>
@endsection
