@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Administrator</h4>
              <p class="card-description">User Activities Log</p>
              <div class="form-group row">
                <div class="col-md-2">
                  <label class="control-label">Start Date</label>
                  <input class="form-control form-control-sm col-md-12" style="" name="startdate" placeholder="Start Date" title="Start Date" id="startdate">
                  <input class="form-control form-control-sm col-md-2" name="startdates" type="hidden" id="startdates">
                </div>
                <div class="col-md-2">
                  <label class="control-label">End Date</label>
                  <input class="form-control form-control-sm col-md-12" style="" name="enddate" id="enddate" placeholder="End Date" title="End Date">
                  <input class="form-control form-control-sm col-md-2" name="enddates" type="hidden" id="enddates">
                </div>
                <div class="col-md-2">
                  <a class="btn btn-sm btn-info text-white btn-search" id="btnsearch" style="margin-top:20px"><span class="fa fa-search"></span></a>
                </div>

              </div>
            </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" style="width:100%">
                    <thead style="">
                      <tr>
                        <th>No</th>
                        <th>Username</th>
                        <th>User Group</th>
                        <th>Activity</th>
                        <th>Activity Description</th>
                        <th>Date</th>
                        <th>Time</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

var table = $("#lookup").dataTable({
  columns:
    [
      {data: 'id'},
      {data: 'username'},
      {data: 'group'},
      {data: 'activity'},
      {data: 'activityDescription'},
      {data: 'date'},
      {data: 'time'}
    ],
  });


function getDate(){
    var yest = new Date(new Date());
    var month = yest.getMonth();
    var year = yest.getFullYear();
    var date = yest.getDate();
    var startDate = new Date(year, month, date);
    $('#startdate').datepicker({
        locale: 'id',
        format: 'dd-M-yy',
        autoclose: true
    });
    $('#startdates').datepicker({
        locale: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true
    });

    $('#enddate').datepicker({
        locale: 'id',
        format: 'dd-M-yy',
        autoclose: true
    });
    $('#enddates').datepicker({
        locale: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true
    });

    $('#startdate').datepicker('setDate', startDate);
    $('#startdates').datepicker('setDate', startDate);
    $('#enddate').datepicker('setDate', startDate);
    $('#enddates').datepicker('setDate', startDate);
}

  $('#btnsearch').on('click', function(){

      var startdate = $('#startdates').val();
      var enddate = $('#enddates').val();

      if(startdate==null||startdate==""){
        $.confirm({
            title: 'Information',
            content: 'Start Date Cant empty',
            buttons: {
                ok: function () {
                },
            }
        });
      }else if(enddate==null||enddate==""){
        $.confirm({
            title: 'Information',
            content: 'End Date Cant empty',
            buttons: {
                ok: function () {
                },
            }
        });
      }else{
        loadData();
      }
  });
  function convertMoment(date){
    var dates = moment(date).locale('id').format('YYYY-MM-DD');
    $('#startdates').val(dates);
    // return dates;
   }
   function convertMoment1(date){
     var dates = moment(date).locale('id').format('YYYY-MM-DD');
     $('#enddates').val(dates);
     // return dates;
    }

  $('.btn-get').on('click', function(){
      $('#modal-closeprice').modal();
      $('.btn-ok').attr('id','btngetsaham')
  });

  $(document).ready(function() {
    getDate();
    var tanggal = $('#startdates').val();
    var tanggal1 = $('#enddates').val();
    var tipeEfek = $('#tipeEfek').val();
    loadData(tanggal,tipeEfek);
  });

  $('#startdate').on('change', function(){
      convertMoment($('#startdate').val());
  });

  $('#enddate').on('change', function(){
      convertMoment1($('#enddate').val());
  });

  $('#tipeEfek').on('change', function(){
      loadData();
  });

  function loadData(){

    $('#lookup').dataTable().fnDestroy();


    var tanggal = $('#startdates').val();
    var tanggal1 = $('#enddates').val();
    var taskId = $('#taskId').val();

    var table = $("#lookup").dataTable({
      processing: true,
      serverSide: true,
      autoWidth: false,
      "lengthMenu": [[15,25,50,100], [15,25,50,100]],
      "pageLength": 15,
      ajax:{
        url: "{{ url('users/activitiesLog/getIndex') }}",
        dataType: "json",
        type: "POST",
        data: {
          startDate: tanggal,
          endDate: tanggal1
        },
        error: function(){  // error handling
          $(".lookup-error").html("");
          $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="7">No data found in the server</th></tr></tbody>');
          $("#lookup_processing").css("display","none");
        }
      },
      columns: [
        {data: 'id'},
        {data: 'username'},
        {data: 'group'},
        {data: 'activity'},
        {data: 'activityDescription'},
        {data: 'date'},
        {data: 'time'}
      ],
      columnDefs: [
              {
                  "targets": [0],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(row+1);
                  },
                  orderable: false
              },
              {
                  "targets": [1],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                  },
              },
              {
                  "targets": [2],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                  },
              },
              {
                  "targets": [3],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                  },
              },
              {
                  "targets": [4],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                  },
              },
              {
                  "targets": [5],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                  },
              },
              {
                  "targets": [6],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                  },
              }
          ],
          createdRow: function ( row, data, index ) {
              $(row).attr('id','table_'+index);
          },

      drawCallback: function(settings) {
             initAutoNumeric();
          },
      });
  }

</script>
@endsection
