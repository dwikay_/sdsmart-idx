@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Administrator</h4>
              <p class="card-description">
              User Management
              </p><br>
              <form class="forms-sample" id="fpro">
                {{csrf_field()}}
                <div class="row">

                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label text-right">Name</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control form-control-sm" placeholder="" name="nama" id="nama">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label text-right">Username</label>
                        <div class="col-sm-4">
                          <select class="form-control form-control-sm" placeholder="" name="email" id="email">
                            <option disabled value="" selected></option>
                            @foreach($userLdap as $users)
                            <option value="{{$users['uid']}}">{{$users['cn']}}</option>
                            @endforeach
                          </select>
                          <!-- <input type="text" class="form-control form-control-sm" placeholder="" name="email" id="email"> -->
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label text-right">User Role</label>
                        <div class="col-sm-4">
                          <select class="form-control form-control-sm"  id="role" name="role">
                            @foreach($role as $roles)
                            <option value="{{$roles->id}}">{{$roles->role_name}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12" style="display:none">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label text-right">Password</label>
                        <div class="col-sm-4">
                          <input class="form-control form-control-sm" type="password" placeholder="" name="password" id="password">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12" style="display:none">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label text-right">Retype Password</label>
                        <div class="col-sm-4">
                          <input class="form-control form-control-sm" type="password" placeholder="" name="confirm" id="confirm">
                        </div>
                      </div>
                    </div>

                  <div class="col-md-12 mt-10 text-right">
                    <a class="btn btn-sm btn-danger btn-save" style="color:white"><span class="fa fa-floppy-o"></span>&nbsp;Save User</a>
                    <a class="btn btn-sm btn-primary btn-cancel" href="{{url('users/management')}}" style="color:white"><span class="fa fa-arrow-left"></span>&nbsp;Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

  $('#email').select2();

  $('.btn-save').on('click', function(){
      var password = $('#password').val();
      var confirm = $('#confirm').val();
      var nama = $('#nama').val();
      var email = $('#email').val();
      if(nama==""){
        $.alert({
          title: 'Error',
          content: 'Name cant be empty!',
          icon: 'fa fa-times'
        });
      }
      else if(email==""){
        $.alert({
          title: 'Error',
          content: 'Username cant be empty!',
          icon: 'fa fa-times'
        });
      }
      // else if(password==""){
      //   $.alert({
      //     title: 'Error',
      //     content: 'Password cant be empty!',
      //     icon: 'fa fa-times'
      //   });
      // }
      // else if(confirm==""){
      //   $.alert({
      //     title: 'Error',
      //     content: 'Password confirmation cant be empty',
      //     icon: 'fa fa-times'
      //   });
      // }
      else{
      //
      //   if(password!=confirm){
      //     $.alert({
      //       title: 'Error',
      //       content: 'Password doesnt match!',
      //       icon: 'fa fa-times'
      //     });
      //   }
      //
      //   else{
      //
      //     if(password.length >= 6){
      //
      //       var uppercase = /[a-z]/;
      //       var lowercase = /[A-Z]/;
      //       var number = /[0-9]/;
      //       var spesChar = /[!@#\$%\^&]/;
      //       var valid = number.test(password) && uppercase.test(password) && lowercase.test(password) && spesChar.test(password); //match a letter _and_ a number
      //
      //       if(valid==true){
      //
              $('#fpro').attr('action','{{url('users/management/store')}}')
              $('#fpro').attr('method','post')
              $('#fpro').submit();

      //       }else{
      //         $.alert({
      //           title: 'Error',
      //           content: 'Password must using alphanumeric ,uppercase, lowercase and special simbols !',
      //           icon: 'fa fa-times'
      //         });
      //       }
      //
      //     }else{
      //       $.alert({
      //         title: 'Error',
      //         content: 'Password must more than 6 character !',
      //         icon: 'fa fa-times'
      //       });
      //
      //     }
      //   }
      }
  });
</script>
@endsection
