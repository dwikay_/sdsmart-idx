<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {font-family: Arial, Helvetica, sans-serif;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 270px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}
.overlay {
  background: rgba(0,0,0,0.5);
}
/* #loading {
  width: 350px;
  height: 50px;
  line-height: 50px;
  text-align: center;
  position: absolute;
  top: 50%;
  left: calc(50%);
  -webkit-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
  text-transform: uppercase;
  font-weight: 900;
  color: #cc0023;
  z-index: 1000;
  display: none;
}

#loading::before,
#loading::after {
  content: "";
  display: block;
  width: 15px;
  height: 15px;
  background: #cc0023;
  position: absolute;
  -webkit-animation: load 1s infinite alternate ease-in-out;
          animation: load 1s infinite alternate ease-in-out;
}

#loading::before {
  top: 0;
}

#loading::after {
  bottom: 0;
} */

#loading {
   width: 100%;
   height: 100%;
   top: 0;
   left: 0;
   position: fixed;
   display: block;
   /* opacity: 0.7;
   background-color: #fff; */
   z-index: 99;
   text-align: center;
}

#loading-image {
    position: absolute;
    color: #000;
    font-size: 20px;
    top: 350px;
    left: 48%;
    z-index: 100;
}

.se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        background:#000;opacity:0.4;filter:alpha(opacity=40);
        z-index: 9999;
        background: center no-repeat #000;
    }

/* Modal Content */
/* .modal-loading {
  background-color: transparent;
  text-align: center
} */
</style>

</head>
<body>
<!-- <div class="overlay"> -->
<div>
  <div id="loading">
    <div id="loading-image"alt="Loading...">Please wait...</div>
  </div>
</div>
<!-- </div> -->
<!-- The Modal -->
<!-- <div id="loading" class="modal"> -->

  <!-- Modal content -->
  <!-- <div class="modal-loading">
    <img src="{{asset('img/5.gif')}}" id="gif">
  </div>
 -->
<!-- </div> -->
<script src="{{asset('js/jquery.min.js')}}"></script>
<script>
// Get the modal


// When the user clicks the button, open the modal


// When the user clicks on <span> (x), close the modal
// span.onclick = function() {
//   modal.style.display = "none";
// }

// When the user clicks anywhere outside of the modal, close it
// window.onclick = function(event) {
//   if (event.target == modal) {
//     modal.style.display = "none";
//   }
// }
</script>

</body>
</html>
