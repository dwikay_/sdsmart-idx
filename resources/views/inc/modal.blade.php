<div class="modal" id="{{ $modalId }}" tabindex="-1" role="dialog">
  <div class="modal-dialog {{$modalClass}}" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">{{ $modalTitle }}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      @if($isForm == 1)
        {!! Form::open(['url' => $formurl, 'id' => $formId, 'files' => true]) !!}
      @endif
      <div class="modal-body">
        @yield('modalContent')
      </div>
      <div class="modal-footer">
        @if (trim($__env->yieldContent('buttons')))
          <h1>@yield('buttons')</h1>
        @else
        <!-- button -->
        @endif
      </div>
      @if($isForm == 1)
        {!! Form::close() !!}
      @endif
    </div>
  </div>
</div>
