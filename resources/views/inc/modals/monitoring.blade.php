@extends('inc.modal',['modalId'=>'modal-monitoring','modalTitle'=>'Manual Running Jobs','modalClass'=>'modal-lg','isForm'=>0])

@section('modalContent')
<div class="row">
  <div class="col-md-12 text-right">
    <!-- <a class="btn btn-sm btn-primary btn-job" style="color:white;"><span class="fa fa-plus fa-sm"></span>&nbsp;&nbsp;Entry Day Process</a>
    <a class="btn btn-sm btn-success btn-job" style="color:white;"><span class="fa fa-plus fa-sm"></span>&nbsp;&nbsp;Intraday Process</a> -->

  </div>
  <div class="col-md-12 mt-10">
     <div class="form-group row">
      <label class="col-sm-3 col-form-label">Task Name</label>
      <div class="col-sm-6">
        <input type="text" class="form-control form-control-sm" id="TASKNAME" name="TASKNAME" value="" placeholder="" readonly="readonly">
        <input type="hidden" class="form-control form-control-sm" id="TASKID" name="TASKID" value="" placeholder="" readonly="readonly">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-3 col-form-label">Process Type</label>
      <div class="col-sm-6">
        <input type="text" class="form-control form-control-sm" id="PROSES_TYPE" name="PROSES_TYPE" value="" placeholder="" readonly="readonly">
      </div>
    </div>
  </div>
  <div class="col-md-12">
     <div class="form-group row">
      <label class="col-sm-3 col-form-label">Interval</label>
      <div class="col-sm-6">
        <input type="text" class="form-control form-control-sm " id="INTERVAL" name="INTERVAL" value="" placeholder="" readonly="readonly">
      </div>
    </div>
  </div>
  <div class="col-md-12">
     <div class="form-group row">
      <label class="col-sm-3 col-form-label">DB Source</label>
      <div class="col-sm-6">
        <input type="text" class="form-control form-control-sm " id="DBSOURCE" name="DBSOURCE" value="" placeholder="" readonly="readonly">
      </div>
    </div>
  </div>
  <div class="col-md-12">
     <div class="form-group row">
      <label class="col-sm-3 col-form-label">Table Source</label>
      <div class="col-sm-6">
        <input type="text" class="form-control form-control-sm " id="TBLSOURCE" name="TBLSOURCE" value="" placeholder="" readonly="readonly">
      </div>
    </div>
  </div>
  <div class="col-md-12">
     <div class="form-group row">
      <label class="col-sm-3 col-form-label">DB Destination</label>
      <div class="col-sm-6">
        <input type="text" class="form-control form-control-sm " id="DBDEST" name="DBDEST" value="" placeholder="" readonly="readonly">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-3 col-form-label">Table Destination</label>
      <div class="col-sm-6">
        <input type="text" class="form-control form-control-sm" id="TBLDEST" name="TBLDEST" value="" placeholder="" readonly="readonly">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-3 col-form-label">Date</label>
      <div class="col-sm-6">
        <input type="text" class="form-control form-control-sm" id="DATE" name="DATE" value="" placeholder="" readonly="readonly">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-3 col-form-label">Count Record</label>
      <div class="col-sm-6">
        <input type="text" class="form-control form-control-sm" id="COUNTRECORD" name="COUNTRECORD" value="" placeholder="" readonly="readonly">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-3 col-form-label">Status</label>
      <div class="col-sm-6">
        <input type="text" class="form-control form-control-sm" id="STATUS" name="STATUS" value="" placeholder="" readonly="readonly">
      </div>
    </div>
  </div><br>
  <div class="col-md-12 text-right mt-3">
    <a class="btn btn-sm btn-success btn-start" style="color:white;"><span class="fa fa-toggle-right fa-sm"></span>&nbsp;&nbsp;Start Job</a>
    <a class="btn btn-sm btn-danger btn-stop" style="color:white;"><span class="fa fa-ban fa-sm"></span>&nbsp;&nbsp;Stop Job</a>
  </div>
</div>
@overwrite
@section('script')
	@parent
	<script>

  $('.borders')
  .css('border-left','1px solid #DEE2E7')
  .css('border-right','1px solid #DEE2E7')
  .css('border-top','1px solid #DEE2E7');


  $('#TASKID').val("");
  $('#TASKNAME').val("");
  $('#PROSES_TYPE').val("");
  $('#INTERVAL').val("");
  $('#DBSOURCE').val("");
  $('#TBLSOURCE').val("");
  $('#DBDEST').val("");
  $('#TBLDEST').val("");
  $('#DATE').val("");
  $('#COUNTRECORD').val("");
  $('#STATUS').val("");

  $('.btn-start').on('click', function(){
    $.confirm({
        title: 'Confirmation',
        content: 'Start this job ?',
        buttons: {
            cancel: function () {
            },
            confirm: function () {
              $.ajax({
                  url: "{!! url('monitoring/monJobs/start-job') !!}/" + $('#TASKID').val(),
                  data: {},
                  dataType: "json",
                  type: "get",
                success:function(data)
                  {
                    $.confirm({
                        title: 'Information',
                        content: data,
                        buttons: {
                            ok: function () {
                              location.href="{{url('monitoring/monJobs')}}";
                            },
                        }
                    });
                    console.log(data);
                  }
              });
            },
        }
    });
  });

  $('.btn-stop').on('click', function(){
    $.confirm({
        title: 'Confirmation',
        content: 'Stop this job ?',
        buttons: {
            cancel: function () {
            },
            confirm: function () {
              $.ajax({
                  url: "{!! url('monitoring/monJobs/cancel-job') !!}/" + $('#TASKID').val(),
                  data: {},
                  dataType: "json",
                  type: "get",
                success:function(data)
                  {
                    $.confirm({
                        title: 'Information',
                        content: data,
                        buttons: {
                            ok: function () {
                              location.href="{{url('monitoring/monJobs')}}";
                            },
                        }
                    });
                  }
              });
            },
        }
    });
  });

  function loadData(data){

    $('#modal-monitoring').find('.modal-footer').remove();
    $('#TASKID').val(data.TASKID);
    $('#TASKNAME').val(data.TASKNAME);
    $('#PROSES_TYPE').val(data.PROSES_TYPE);
    $('#INTERVAL').val(data.INTERVAL+"s");
    $('#DBSOURCE').val(data.DBSOURCE);
    $('#TBLSOURCE').val(data.TBLSOURCE);
    $('#DBDEST').val(data.DBDEST);
    $('#TBLDEST').val(data.TBLDEST);
    $('#COUNTRECORD').val(data.COUNTRECORD);
    var dates = moment(data.DATE).locale('en').format('d-MMM-YY');
    $('#DATE').val(data.DATE);

    if(data.STATUS=="Pending" || data.STATUS=="Stop" || data.STATUS=="Exception"){
      $('#STATUS').removeClass("bg-success");
      $('#STATUS').removeClass("bg-warning");
      $('#STATUS').val(data.STATUS);
      $('#STATUS').addClass("bg-danger");
      $('#STATUS').addClass("text-white");
      $('#btnInitial').css('display','block');
      $('#btnInitial').addClass("text-white");
    }
    else if(data.STATUS=="Complete" || data.STATUS=="Finish"){
      $('#STATUS').val(data.STATUS);
      $('#STATUS').removeClass("bg-warning");
      $('#STATUS').removeClass("bg-danger");
      $('#STATUS').addClass("bg-success");
      $('#STATUS').addClass("text-white");
      $('#btnInitial').css('display','none');
    }
    else if(data.STATUS=="On Progres"){
      $('#STATUS').val(data.STATUS);
      $('#STATUS').removeClass("bg-success");
      $('#STATUS').removeClass("bg-danger");
      $('#STATUS').removeClass("text-white");
      $('#STATUS').addClass("bg-info text-white");
      $('#STATUS').addClass("text-black");
      $('#btnInitial').css('display','none');
    }
  }
  </script>
@endsection
