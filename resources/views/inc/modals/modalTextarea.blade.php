@extends('inc.modal',['modalId'=>'modalTextarea','modalTitle'=>'Query Table','modalClass'=>'modal-md','isForm'=>0])

@section('modalContent')
<div class="row">
  <!-- <label class="col-sm-3">Query</label> -->
  <div class="col-md-12">
     <div class="form-group">
       @if($task==null)
       <textarea type="text" class="form-control form-control-sm" id="queryText" name="queryText" placeholder="" style="height:100px"></textarea>
       @else
       <textarea type="text" class="form-control form-control-sm" id="queryText" name="queryText" placeholder="" style="height:100px">{{$task->mapTable->SCRIPTQUERY}}</textarea>
       @endif

      <input type="hidden" id="idTableTxt" name="idTableTxt">
      <label class="control-labeel pt-1" style="color:red"><i>*Query! 'LIMIT' is not allowed</i></label>
    </div>
  </div>
  <div class="col-md-12 text-right mt-3">
    <a class="btn btn-sm btn-success btn-saveQuery" style="color:white;"><span class="fa fa-floppy-o fa-sm"></span>&nbsp;&nbsp;Save Query</a>
    <a class="btn btn-sm btn-danger btn-cancelQuery" style="color:white;"><span class="fa fa-ban fa-sm"></span>&nbsp;&nbsp;Cancel</a>
  </div>
</div>

@overwrite
@section('script')
	@parent
	<script>

  $('.query-input').on('change', function(){
    // alert("test")
      submitQuery();
  });

  function submitQuery(){
      var str = $('form').serialize();
      console.log(str);
      $.ajax({
        url: "{!! url('mapping/table/admTableUpdate') !!}?" + str,
        data: {},
        dataType: "json",
        type: "get",
        success:function(data)
          {
            console.log(data);
          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
              console.log(errorMsg);
          }
        });
  }

  $('.btn-saveQuery').on('click', function(){

    var textQuery = $('#queryText').val();

    var queryRownum = "SELECT %s FROM %s WHERE DATE = :DATE AND  InsertId >= :FIRSTROWNUM AND InsertId < :ENDROWNUM";
    var queryTimestamp = "SELECT %s FROM %s  WHERE DATE = :DATE and TIME >= :TIMESTART and TIME <= :TIMEEND";
    var queryInUp = "SELECT %s FROM %s  WHERE DATE = :DATE";

    if($('#proses_type').val()==1||$('#proses_type').val()=="1"){

      if($('#data_type').val()=="0" || $('#data_type').val()==0){

          if(textQuery==queryRownum){
            var querys = queryTimestamp;
          }else{
            var querys = textQuery;
          }

      }else{

          if(textQuery==queryRownum){
            var querys = queryRownum;
          }else{
            var querys = textQuery;
          }

      }

    }else{

        if(textQuery==queryRownum){
          var querys = queryInUp;
        }else{
          var querys = textQuery;
        }
    }

    $('#queryText').val(querys);
    $('#modalTextarea').modal('hide');

  });
  $('.btn-cancelQuery').on('click', function(){

    var queryRownum = "SELECT %s FROM %s WHERE DATE = :DATE AND  InsertId >= :FIRSTROWNUM AND InsertId < :ENDROWNUM";
    var queryTimestamp = "SELECT %s FROM %s  WHERE DATE = :DATE and TIME >= :TIMESTART and TIME <= :TIMEEND";
    var queryInUp = "SELECT %s FROM %s  WHERE DATE = :DATE";

    if($('#proses_type').val()==1||$('#proses_type').val()=="1"){

      if($('#data_type').val()=="0" || $('#data_type').val()==0){
        var querys = queryTimestamp;
      }else{
        var querys = queryRownum;
      }
    }else{
      var querys = queryInUp;
    }

    $('#queryText').val(querys);
    $('#modalTextarea').modal('hide');

  });
</script>
@endsection
