@extends('inc.modal',['modalId'=>'modal-mapping','modalTitle'=>'Mapping Field','modalClass'=>'modal-lg','isForm'=>0])

@section('modalContent')
<div class="row">
  <div class="col-md-12">
     <div class="form-group row">
      <label class="col-sm-3 col-form-label">DB Source Name</label>
      <div class="col-sm-3">
        <input type="text" class="form-control form-control-sm" id="dbSourceName" readonly name="dbSourceName" placeholder="">
        <input type="hidden" id="taskIdModal" name="taskIdModal">
      </div>
      <label class="col-sm-3 col-form-label">DB Target Name</label>
      <div class="col-sm-3">
        <input type="text" class="form-control form-control-sm" id="dbTargetName" readonly name="dbTargetName" placeholder="">
      </div>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group row">
      <label class=" col-sm-3 col-form-label">DB Source Table</label>
      <div class="col-sm-3">
        <input type="text" class="form-control form-control-sm" id="dbSourceTable" readonly name="dbSourceTable" placeholder="">
      </div>
      <label class=" col-sm-3 col-form-label">DB Target Table</label>
      <div class="col-sm-3">
        <input type="text" class="form-control form-control-sm" id="dbTargetTable" readonly name="dbTargetTable" placeholder="">
      </div>
    </div>
  </div>
</div>

	<div class="row">

    <div class="col-md-12 grid-margin stretch-card mt-4">
      <div class="card">
        <div class="card-body">
              <div class="box-tab" id="mtab2">
                  <ul class="nav nav-tabs tab-basic">
                      <li class="nav-item active" id="91"><a class="nav-link active" href="#pospengaturan" data-toggle="tab">Mapping Table</a></li>
                  </ul><br>
                        <div class="tab-content text-center">
                          <div class="col-sm-12" style="">
                            <button type="button" class="btn btn-sm btn-success float-right" id="btn-add-field"><i class="fa fa-plus fa-sm"></i> Field</button>
                          </div>
                                    <div class="tab-pane fade in active show" id="pospengaturan">
                                        <div class="table-responsive">
                                          <table class="table table-sm table-striped table-bordered" id="lookup" width="100%">
                                            <thead class="text-center" style="border:1px solid #000">
                                              <tr class="">
                                                <th class="borders">Source Field Name</th>
                                                <th class="borders">Source Field Type</th>
                                                <th class="borders">Target Field Name</th>
                                                <th class="borders">Target Field Type</th>
                                                <th class="borders">Action</th>
                                              </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                          </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
      </div>
    </div>
  </div>

	</div>
@overwrite
@section('script')
	@parent
	<script>

  $('.borders')
  .css('border-left','1px solid #DEE2E7')
  .css('border-right','1px solid #DEE2E7')
  .css('border-top','1px solid #DEE2E7');

  $(document).ready(function() {

    var lookup = $('#lookup').DataTable({
          autoWidth: true,
          'bSort': false,
          'bPaginate': false,
          "scrollX": true,
          'scrollY': '100vh',
          "scrollCollapse": true,
          'searching' : false,
          'fnInitComplete': function(){
              $('#lookup').parent().css('max-height','calc(100vh - 500px)').css('height','calc(100vh - 500px)');
          },
          'bInfo': false,
          'drawCallback': function(){

          },
          columns: [
            {data: 'SOUCEFIELD', width: "100px"},
            {data: 'SOUCEFIELDTYPE', width: "100px"},
            {data: 'TARGETFIELD', width: "100px"},
            {data: 'TARGETFIELDTYPE', width: "100px"},
            {data: 'Action', width: "20px"},
          ],
          columnDefs: [
            {
              "targets": [0],
              "createdCell": function (td, cellData, rowData, row, col) {
                // console.log(cellData)
              },
              orderable: false
            },
          ],
      });


    $('#btn-add-field').on('click',function(){
        addFieldRecord();
    });

    function addFieldRecord(){



      var row = $('<tr>')
        .addClass('tableMap')
        .attr('id','row_'+no)
        .append($('<td>')
            .append($('<select>')
                .attr('id','source_'+no+'_sourceField')
                .attr('name',"sourceField[]")
                .addClass('form-control form-control-sm form-control-chosen source-field')
            )
        )
        .append($('<td>')
            .append($('<input>')
                .attr('id','source_'+no+'_sourceFieldType')
                .attr('readonly',"readonly")
                .addClass('form-control form-control-sm form-control-chosen source-field-type')
                .attr('type','text')
            )
        )
        .append($('<td>')
            .append($('<select>')
                .attr('id','target_'+no+'_targetField')
                .attr('name',"targetField[]")
                .addClass('form-control form-control-sm target-field')

            )
        )
        .append($('<td>')
            .append($('<input>')
                .attr('id','target_'+no+'_targetFieldType')
                .attr('readonly',"readonly")
                .addClass('form-control form-control-sm target-field-type')
                .attr('type','text')
            )
        )
        .append($('<td>')
            .append($('<btn>')
              .addClass('btn btn-danger btn-remove-field')
              .append($('<span>')
                .addClass('fa fa-times')
              )
            )
        ).addClass('text-center');

      lookup.row.add(row).draw( true );
      initAutoNumeric();

      var stockIdListField = [];
      var stockIdListField1 = [];

      $.each($(".source-field option:selected"), function(){
          stockIdListField.push(parseInt($(this).val()));
          // console.log($(this).val());
      });

      $.each($(".target-field option:selected"), function(){
          stockIdListField1.push(parseInt($(this).val()));
          // console.log($(this).val());
      });
      if(dataFieldSource.length>0){
        dataFieldSource.forEach(function(currentValue,index){
            if($.inArray(currentValue.id, stockIdListField)  === -1 ){
              $('#source_'+no+'_sourceField').append($('<option>', {
                  value: currentValue,
                  text : currentValue
              }));
            }
        });
      }else{
        $.confirm({
            title: 'Information',
            content: 'No table found on DB Source',
            buttons: {
                ok: function () {
                },
            }
        });
      }


      if(dataFieldTarget.length>0){
        dataFieldTarget.forEach(function(currentValue,index){
            if($.inArray(currentValue.id, stockIdListField1)  === -1 ){
              $('#target_'+no+'_targetField').append($('<option>', {
                  value: currentValue,
                  text : currentValue
              }));
            }
        })
      }else{
        $.confirm({
            title: 'Information',
            content: 'No table found on DB Target',
            buttons: {
                ok: function () {
                },
            }
        });
      }

      refreshStockCodeListField();

      no++;
    }

    function refreshStockCodeListField(){
        var stockIdListField = [];
        var stockIdListField1 = [];

        $.each($(".source-field option:selected"), function(){
            stockIdListField.push($(this).val());
            // console.log($(this).val());
        });
        $.each($(".target-field option:selected"), function(){
            stockIdListField1.push($(this).val());
            // console.log($(this).val());
        });

        $.each($(".source-field"), function(){
            var stockid = $(this).val()

            $(this)
                .find('option')
                .remove()

            var id = $(this).attr('id');

            dataFieldSource.forEach(function(currentValue,index){
                $('#'+id).append($('<option>', {
                    value: currentValue.COLUMN_NAME,
                    text : currentValue.COLUMN_NAME
                }).attr('data-type',"")
              );
            });

            $(this).val(stockid);

            stockIdListField.forEach(function(currentValue,index){
                if(stockid != currentValue){
                    $("#"+id+" option[value='"+currentValue.COLUMN_NAME+"']").remove();
                }
            });
        });

        $.each($(".target-field"), function(){
            var stockid = $(this).val()

            $(this)
                .find('option')
                .remove()

            var id = $(this).attr('id');

            dataFieldTarget.forEach(function(currentValue,index){
                $('#'+id).append($('<option>', {
                    value: currentValue.COLUMN_NAME,
                    text : currentValue.COLUMN_NAME
                }));
            });

            $(this).val(stockid);

            stockIdListField1.forEach(function(currentValue,index){
                if(stockid != currentValue){
                    $("#"+id+" option[value='"+currentValue.COLUMN_NAME+"']").remove();
                }
            });
        });
    }


    $('#lookup').on('click', '.btn-remove-field', function() {
            var elem = $(this).parents('tr');
            $.confirm({
                title: 'Confirmation',
                content: 'Delete this row ?',
                buttons: {
                    cancel: function () {
                    },
                    confirm: function () {
                        lookup.row(elem).remove().draw( true );
                        refreshStockCodeListField();
                    },
                }
            });
        });

  });

  function loadFieldSource(sourceTable){
    $.ajax({
        url : '{{ url('mapping/table/getColumn') }}/' + $('#selectSource').val() + "/" + sourceTable,

        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
           dataFieldSource = data;
        },
        error: function (jqXHR, textStatus, errorThrown){
            var errorMsg = 'Ajax request failed table with: ' + errorThrown;
            console.log(errorMsg);
        }
    });

  }

  function loadFieldTarget(targetTable){
    $.ajax({
        url : '{{ url('mapping/table/getColumn') }}/' + $('#selectTarget').val() + "/" + targetTable,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {
           dataFieldTarget = data;
        },
        error: function (jqXHR, textStatus, errorThrown){
            var errorMsg = 'Ajax request failed table with: ' + errorThrown;
            console.log(errorMsg);
        }
    });
  }

  $(document).ready(function() {

    $('#lookup').on('change', '.source-field', function() {

        var elem = $(this).parents('tr');
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];

        // console.log(data);

        var source = $('#source_'+index+'_sourceField').val();
        var sourceType = $('#source_'+index+'_sourceFieldType').val();
        var target = $('#target_'+index+'_targetField').val();
        var targetType = $('#target_'+index+'_targetFieldType').val();

        // console.log(tr);

        // $.ajax({
        //   url: "{!! url('mapping/table/getColumn') !!}/" + $('#selectSource').val() + "/" + sourceTable,
        //   data: {},
        //   dataType: "json",
        //   type: "get",
        //   success:function(data)
        //     {
        //       var dbscname = $('#valueSource').val();
        //       $('#dbSourceName').val(dbscname)
        //       $('#dbSourceTable').val(sourceTable)
        //       $('#taskIdModal').val(taskId)
        //       loadFieldSource(sourceTable);
        //
        //     }
        //   })

        });


  });
  </script>
@endsection
