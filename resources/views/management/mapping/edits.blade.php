@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Mapping Table</h4>
              <p class="card-description">
                Mapping Table Configuration
              </p><br>
              <form class="forms-sample" action="#" method="post">
                {{csrf_field()}}
                <div class="row">

                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Task Name Mapping</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm mapping-input" placeholder="" value="{{$task->TASKNAME}}" name="taskName">
                        <input type="hidden" name="taskid" value="{{$task->TASKID}}" id="taskid">
                      </div>
                      <label class=" col-sm-2 col-form-label">Process Type</label>
                      <div class="col-sm-4">
                        <select class="form-control form-control-sm mapping-input" name="proses_type" id="proses_type">
                          <option disabled selected></option>
                          <option value="0" {{ $task->ISINSERT == "0" ? 'selected' : ''}}>Insert</option>
                          <option value="1" {{ $task->ISINSERT == "1" ? 'selected' : ''}}>Insert and Update</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Interval Task</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm mapping-input" value="{{$task->TASKINTERVAL}}" onkeypress="return justnumber(event, false)" placeholder="" value="" name="taskInterval">
                      </div>
                      <label class=" col-sm-2 col-form-label">Data Type</label>
                      <div class="col-sm-2">
                        <select class="form-control form-control-sm" name="data_type" id="data_type">
                          <option disabled selected></option>
                          <option value="0" {{ $task->ISROWNUM == "0" ? 'selected' : ''}}>Timestamp</option>
                          <option value="1" {{ $task->ISROWNUM == "1" ? 'selected' : ''}}>Rownum</option>
                        </select>
                      </div>
                      <div class="col-sm-2" style="margin-left:-2%">
                        @if($task->ISROWNUM=="1")
                        <input type="text" style="display:none" class="form-control form-control-sm mapping-input" placeholder="" id="timestampVal" name="timestampVal">
                        <input type="text" value="{{$type_data->ROWNUM}}" class="form-control form-control-sm mapping-input" placeholder="0" onkeypress="return justnumber(event, false)" id="rownumVal" name="rownumVal">
                        @else
                        <input type="text" value="{{$type_data->TIMESTART}}" style="display:none;width:119%" class="form-control form-control-sm mapping-input" placeholder="" id="timestampVal" name="timestampVal">
                        <input type="text" style="display:none" class="form-control form-control-sm mapping-input" placeholder="0" onkeypress="return justnumber(event, false)" id="rownumVal" name="rownumVal">
                        @endif

                      </div>
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Primary Key #1</label>
                      <div class="col-sm-4">
                        <input type="text" value="{{$task->PRIMARYKEY1}}" class="form-control form-control-sm mapping-input" placeholder="" id="primaryKey1" name="primaryKey1">
                      </div>
                      <label class=" col-sm-2 col-form-label">Primary Key #2</label>
                      <div class="col-sm-4">
                        <input type="text" value="{{$task->PRIMARYKEY2}}" class="form-control form-control-sm mapping-input" placeholder="" id="primaryKey2" name="primaryKey2">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12" style="margin-top:3%">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Database Source</label>
                      <div class="col-sm-4">
                        <select class="form-control form-control-chosen dbsource mapping-input" name="selectSource" id="selectSource">
                          <option disabled selected></option>
                          @foreach($database as $values)
                            @if($task->DBSOURCEID==null)
                            <option value="{{$values->DBCONNID}}">{{$values->DBNAME}}</option>
                            @else
                            <option value="{{$values->DBCONNID}}" {{ $task->DBSOURCEID == $values->DBCONNID ? 'selected' : ''}}>{{$values->DBNAME}}</option>
                            @endif
                          @endforeach
                        </select>
                        <input id="valueSource" type="hidden"></input>
                      </div>
                      <label class=" col-sm-2 col-form-label">Database Destination</label>
                      <div class="col-sm-4">
                        <select class="form-control form-control-chosen dbtarget mapping-input" name="selectTarget" id="selectTarget">
                          <option disabled selected></option>
                          @foreach($database as $values)
                          @if($task->DBDESTID==null)
                          <option value="{{$values->DBCONNID}}">{{$values->DBNAME}}</option>
                          @else
                          <option value="{{$values->DBCONNID}}" {{ $task->DBDESTID == $values->DBCONNID ? 'selected' : ''}}>{{$values->DBNAME}}</option>
                          @endif
                          @endforeach
                        </select>
                        <input id="valueTarget" type="hidden"></input>
                      </div>
                    </div>
                  </div>
                  <br>
                  <div class="col-sm-12" style="margin-top:20px">
                    <button type="button" class="btn btn-sm btn-success float-right" id="btn-add-record"><i class="fa fa-plus fa-sm"></i> Add Table</button>
                  </div>
                  <div class="col-12 table-responsive" style="margin-top:1%">
                    <table id="table-add" class="table table-sm table-striped table-bordered">
                      <thead style="border:1px solid #000">
                        <tr>
                          <th>Source Table</th>
                          <th>Destination Table</th>
                          <th class="text-center">Query</th>
                          <th class="text-center">Action</th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>

                  <div style="margin-top:5%" class="col-md-12 mt-10 text-right">
                    <a class="btn btn-sm btn-danger btn-save" style="color:white"><i class="fa fa-floppy-o fa-sm"></i>&nbsp;Save Mapping</a>
                    <a class="btn btn-sm btn-primary btn-cancel" style="color:white"><i class="fa fa-arrow-left fa-sm"></i>&nbsp;Cancel</a>
                  </div>
                </div>

            </div>
          </div>
        </div>
@endsection
@section('modal')
    @include('inc.modals.modalMapping')
    @include('inc.modals.modalTextarea')
@endsection
</form>
@section('script')
<script>

var dataEfek = [];
var no = 0;

var data_type = $('#data_type').val();
if(data_type=="1"){
  $('#rownumVal').css('display','block');
  $('#timestampVal').css('display','none');
}else{
  $('#rownumVal').css('display','none');
  $('#timestampVal').css('display','block');
}

$('.mapping-input').on('change', function(){
  // alert("test")
    submitForm();
});

function submitForm(){
    var str = $('form').serialize();
    console.log(str);
    $.ajax({
      url: "{!! url('mapping/table/admTaskUpdate') !!}?" + str,
      data: {},
      dataType: "json",
      type: "get",
      success:function(data)
        {
          console.log(data);
        },
        error: function (jqXHR, textStatus, errorThrown){
            var errorMsg = 'Ajax request failed table with: ' + errorThrown;
            console.log(errorMsg);
        }
      });
}
$('#data_type').on('change', function(){

  $('#rownumVal').val("");
  $('#timestampVal').val("");

  var types = $('#data_type').val();
  if(types==1){
    $('#rownumVal').css('display','block');
    $('#timestampVal').css('display','none');
  }else if(types==0){
    $('#rownumVal').css('display','none');
    $('#timestampVal').css('display','block');
  }

});
$(document).ready(function() {

  loadSource();
  loadTarget();
  checkTable();

  $('#timestampVal').datetimepicker({
      language: 'pt-BR'
    });

  $('#selectSource').on('change', function(){

    var tableMap = $('#table-add').DataTable();
    var countRow = tableMap.rows( '.tableMap' ).count()
    var valueSelect = $('#selectSource option:selected').html();

    $('#valueSource').val(valueSelect);


    if(countRow==0){
      loadSource();
    }else{
      $.confirm({
          title: 'Confirmation',
          content: 'Do you want to change table ?',
          buttons: {
              cancel: function () {
              },
              confirm: function () {
                tableMap.clear().draw();
                loadSource();
              },
          }
      });
    }


  });

  $('#selectTarget').on('change', function(){

    var tableMap = $('#table-add').DataTable();
    var countRow = tableMap.rows( '.tableMap' ).count()

    var valueTarget = $('#selectTarget option:selected').html();
    $('#valueTarget').val(valueTarget);

    if(countRow==0){
      loadTarget();
    }else{
      $.confirm({
          title: 'Confirmation',
          content: 'Do you want to change table ?',
          buttons: {
              cancel: function () {
              },
              confirm: function () {
                tableMap.clear().draw();
                loadTarget();
              },
          }
      });
    }

  });

});
  var tableadd = $('#table-add').DataTable({
        autoWidth: false,
        'bSort': false,
        'bPaginate': false,
        "scrollX": true,
        'scrollY': '100vh',
        "scrollCollapse": true,
        'searching' : false,
        'fnInitComplete': function(){
            $('#table-add').parent().css('max-height','calc(100vh - 500px)').css('height','calc(100vh - 500px)');
        },
        'bInfo': false,
        'drawCallback': function(){
        },
        columns: [
          {data: 'TBLSOURCE', width: "400px"},
          {data: 'TBLDEST', width: "400px"},
          {data: 'SCRIPTQUERY', width: "50px"},
          {data: 'MAPTBLID', width: "50px"},
        ],
    });

    $('#btn-add-record').on('click',function(){

        if($('#selectSource').val()==null){

          $.confirm({
              title: 'Information',
              content: 'Please select source Database',
              buttons: {
                  ok: function () {
                  },
              }
          });

        }else if($('#selectTarget').val()==null){

          $.confirm({
              title: 'Information',
              content: 'Please select target Database',
              buttons: {
                  ok: function () {
                  },
              }
          });

        }else{
          addRecord();
        }

    });

    $('#table-add').on('click', '.btn-query', function() {
        // alert("test");

        var elem = $(this).parents('tr');
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];

        var sourceTable = $('#stock_'+index+'_stockcode').val();
        var targetTable = $('#stock_'+index+'_quantity').val();
        var tableId = $('#stock_'+index+'_tableId').val();

        console.log(targetTable);
        if(sourceTable==null){

          $.confirm({
              title: 'Information',
              content: 'Please select Table Source',
              buttons: {
                  ok: function () {
                  },
              }
          });

        }else if(targetTable==null){
          $.confirm({
              title: 'Information',
              content: 'Please select Table Destination',
              buttons: {
                  ok: function () {
                  },
              }
          });
        }else{
          $('#idTableTxt').val(tableId);
          $('#modalTextarea').modal();
        }


        });

    $('#table-add').on('change', '.stock-code', function() {
      $(this).closest(".quantity").val(0);

      refreshStockCodeList();
    });

    function addRecord(){

      var row = $('<tr>')
        .addClass('tableMap')
        .attr('id','row_'+no)
        .append($('<td>')
            .append($('<select>')
                .attr('id','stock_'+no+'_stockcode')
                .attr('name',"sourceTable[]")
                .addClass('form-control form-control-sm form-control-chosen stock-code')
            )
        )
        .append($('<td>')
            .append($('<select>')
                .attr('id','stock_'+no+'_quantity')
                .attr('name',"targetTable[]")
                .addClass('form-control form-control-sm quantity')
                  .append($('<option>'))
                  .val("")
            )
            .append($('<input>')
              .attr('id','stock_'+no+'_tableId')
              .attr('name',"idTable[]")
              .attr('type','hidden')
              .attr('value','nan')
            )
        )


        .append($('<td>')
          .append($('<div>')
          .addClass('btn-group')
          .attr('role','group')
          .attr('arial-label','Basic Example')
              .append($('<a>')
              .attr('href','#')
              .addClass('btn btn-md btn-primary float-right btn-query')
              .attr('data-whatever','btnquery'+no)
              .css('color','white')
              .css('height','30px')
              .attr('title','Query Table')
              .append($('<span>')
                  .addClass('fa fa-pencil fa-xs')
                  .css('padding-top','40%')
                  // .css('padding-top','10%')
                  // .append('&nbsp;&nbsp;Mapping')
              )
            ) //endbtn1
      ).addClass('text-center'))

        .append($('<td>')
          .append($('<div>')
          .addClass('btn-group')
          .attr('role','group')
          .attr('arial-label','Basic Example')
              .append($('<a>')
              .attr('href','#')
              .addClass('btn btn-md btn-info float-right btn-map-record')
              .attr('data-whatever','btnmap_'+no)
              .css('color','white')
              .css('height','30px')
              .attr('title','Mapping Table')
              .append($('<span>')
                  .addClass('fa fa-window-restore fa-xs')
                  .css('padding-top','40%')
                  // .css('padding-top','10%')
                  // .append('&nbsp;&nbsp;Mapping')
              )
            ) //endbtn1
            .append($('<a>')
            .attr('href','#')
            .addClass('btn btn-md btn-danger float-right btn-remove-record')
            .css('color','white')
            .css('height','30px')
            .attr('title','Delete Row')
            .append($('<span>')
                .addClass('fa fa-times fa-xs')
                .css('padding-top','55%')
                .css('font-size','14px')
                // .css('padding-top','10%')
                // .append('&nbsp;&nbsp;Delete')
            )
          ) //endbtn2

      ).addClass('text-center'));

      tableadd.row.add(row).draw( true );
      initAutoNumeric();

      var stockIdList = [];
      var stockIdList1 = [];

      $.each($(".stock-code option:selected"), function(){
          stockIdList.push(parseInt($(this).val()));
      });

      $.each($(".quantity option:selected"), function(){
          stockIdList1.push(parseInt($(this).val()));
      });

      if(dataSource.length>0){
        dataSource.forEach(function(currentValue,index){
            if($.inArray(currentValue.id, stockIdList)  === -1 ){
              $('#stock_'+no+'_stockcode').append($('<option>', {
                  value: currentValue,
                  text : currentValue
              }));
            }
        });
      }else{
        $.confirm({
            title: 'Information',
            content: 'No table found on DB Source',
            buttons: {
                ok: function () {
                },
            }
        });
      }


      if(dataTarget.length>0){
        dataTarget.forEach(function(currentValue,index){
            if($.inArray(currentValue.id, stockIdList1)  === -1 ){
              $('#stock_'+no+'_quantity').append($('<option>', {
                  value: currentValue,
                  text : currentValue
              }));
            }
        })
      }else{
        $.confirm({
            title: 'Information',
            content: 'No table found on DB Destination',
            buttons: {
                ok: function () {
                },
            }
        });
      }

      refreshStockCodeList();

      no++;
    }

    function refreshStockCodeList(){
        var stockIdList = [];
        var stockIdList1 = [];

        $.each($(".stock-code option:selected"), function(){
            stockIdList.push($(this).val());
        });
        $.each($(".quantity option:selected"), function(){
            stockIdList1.push($(this).val());
        });

        $.each($(".stock-code"), function(){
            var stockid = $(this).val()

            $(this)
                .find('option')
                .remove()

            var ids = $(this).attr('id');

            $('#'+ids).append($('<option>')
              .attr('selected','selected')
              .attr('disabled','disabled')
              .val("")
            );

            dataSource.forEach(function(currentValue,index){
                $('#'+ids).append($('<option>', {
                    value: currentValue,
                    text : currentValue
                }));
            });

            $(this).val(stockid);

            stockIdList.forEach(function(currentValue,index){
                if(stockid != currentValue){
                    $("#"+ids+" option[value='"+currentValue+"']").remove();
                }
            });
        });

        $.each($(".quantity"), function(){

            var stockid = $(this).val()

            $(this)
                .find('option')
                .remove()

            var id = $(this).attr('id');
            $('#'+id).append($('<option>')
              .attr('selected','selected')
              .attr('disabled','disabled')
              .val("")
            );
            dataTarget.forEach(function(currentValue,index){

                $('#'+id).append($('<option>', {
                    value: currentValue,
                    text : currentValue
                }));
            });

            $(this).val(stockid);

            stockIdList.forEach(function(currentValue,index){
                if(stockid != currentValue){
                    $("#"+id+" option[value='"+currentValue+"']").remove();
                }
            });
        });
    }

    $('#table-add').on('click', '.btn-remove-record', function() {
            var elem = $(this).parents('tr');

            // var tr = $(this).closest('tr');
            // // console.log(tr);
            // var id = tr.attr('id').split('_');
            // var index = id[1];
            //
            // var tableId = $('#stock_'+index+'_tableId').val();

            $.confirm({
                title: 'Confirmation',
                content: 'Delete this row ?',
                buttons: {
                    cancel: function () {
                    },
                    confirm: function () {
                        tableadd.row(elem).remove().draw( true );
                        refreshStockCodeList();

                    },
                }
            });
        });

        $('#table-add').on('change', '.quantity', function() {

                var elem = $(this).parents('tr');

                var tr = $(this).closest('tr');
                var id = tr.attr('id').split('_');
                var index = id[1];

                var sourceTable = $('#stock_'+index+'_stockcode').val();
                var targetTable = $('#stock_'+index+'_quantity').val();
                var tableId = $('#stock_'+index+'_tableId').val();
                // alert(tableId);

                $.ajax({
                  url: "{!! url('mapping/table/updateTable') !!}/" + "target" + "/" + targetTable + "/" + tableId + "/" + $('#taskid').val(),
                  data: {},
                  dataType: "json",
                  type: "get",
                  success:function(data)
                    {
                      // console.log(data);
                      $('#stock_'+index+'_tableId').val(data[0]['tableId']);

                    }
                  });

            });

            $('#table-add').on('change', '.stock-code', function() {

                    var elem = $(this).parents('tr');

                    var tr = $(this).closest('tr');
                    var id = tr.attr('id').split('_');
                    var index = id[1];

                    var sourceTable = $('#stock_'+index+'_stockcode').val();
                    var targetTable = $('#stock_'+index+'_quantity').val();
                    var tableId = $('#stock_'+index+'_tableId').val();
                    // alert(tableId);

                    $.ajax({
                      url: "{!! url('mapping/table/updateTable') !!}/" + "source" + "/" + sourceTable + "/" + tableId + "/" + $('#taskid').val(),
                      data: {},
                      dataType: "json",
                      type: "get",
                      success:function(data)
                        {
                          // console.log(data);
                          $('#stock_'+index+'_tableId').val(data[0]['tableId']);

                        }
                      });

                });



    $('#table-add').on('click', '.btn-map-record', function() {

        var elem = $(this).parents('tr');

        var tr = $(this).closest('tr');
        var id = tr.attr('id').split('_');
        var index = id[1];

        var sourceTable = $('#stock_'+index+'_stockcode').val();
        var targetTable = $('#stock_'+index+'_quantity').val();
        var sourceName = $('#selectSource').val();
        var targetName = $('#selectTarget').val();
        var taskId = $('#taskid').val();


        $.ajax({
          url: "{!! url('mapping/table/getColumn') !!}/" + $('#selectSource').val() + "/" + sourceTable,
          data: {},
          dataType: "json",
          type: "get",
          success:function(data)
            {
              var dbscname = $('#valueSource').val();
              $('#dbSourceName').val(dbscname)
              $('#dbSourceTable').val(sourceTable)
              $('#taskIdModal').val(taskId)
              loadFieldSource(sourceTable);

            }
          });

          $.ajax({
            url: "{!! url('mapping/table/getColumn') !!}/" + $('#selectTarget').val() + "/" + targetTable,
            data: {},
            dataType: "json",
            type: "get",
            success:function(data)
              {
                var dbtgname = $('#valueTarget').val();
                $('#dbTargetName').val(dbtgname)
                $('#dbTargetTable').val(targetTable)

                loadFieldTarget(targetTable);
              }
            });

        $('#dbSourceName').val(sourceName);
        $('#dbTargetName').val(targetName);
        console.log(sourceName)
        $('#modal-mapping').modal();



        });

    function loadSource(){
      $.ajax({
          url : '{{ url('mapping/table/getTable') }}/' + $('#selectSource').val(),

          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataSource = data;
          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
              // console.log(errorMsg);
          }
      });
    }

    function loadTarget(){
      $.ajax({
          url : '{{ url('mapping/table/getTable') }}/' + $('#selectTarget').val(),
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataTarget = data;
          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
              // console.log(errorMsg);
          }
      });
    }
    function checkTable(){

      console.log($('#taskid').val());
      $.ajax({
        url: "{!! url('mapping/table/checkTable') !!}/" + $('#taskid').val(),
        data: {},
        dataType: "json",
        type: "get",
        success:function(data)
          {
            if(data[0].data!="empty"){
              // alert(data[0].data)
              // console.log(data[0].data)
                loadTable(data[0]);
            }


          }
        });

    }
    function loadTable(dataOnTable){ //loadTable from datatable
      console.log(dataOnTable.data)
      $('#table-add').dataTable().fnDestroy();

      tableEditAlokasi = $('#table-add').dataTable({
      data: dataOnTable.data,
      bPaginate: false,
      searching : false,
      bSort: false,
      bInfo: false,
      scrollX: true,
      scrollY: '100vh',
      scrollCollapse: true,
      autoWidth: false,
      columns: [
        {data: 'TBLSOURCE', width: "400px"},
        {data: 'TBLDEST', width: "400px"},
        {data: 'SCRIPTQUERY', width: "50px"},
        {data: 'MAPTBLID', width: "50px"},
      ],
      columnDefs: [
        {
          "targets": [0],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<select>')
                .attr('id','stock_'+no+'_stockcode')
                .attr('name',"sourceTable[]")
                .addClass('form-control form-control-sm form-control-chosen stock-code')
                .append($('<option>')
                .val(cellData)
                .text(cellData)
              )
            )

          },
        },
        {
          "targets": [1],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<select>')
                .attr('id','stock_'+no+'_quantity')
                .attr('name',"targetTable[]")
                .addClass('form-control form-control-sm quantity')
                  .append($('<option>')
                  .val(cellData)
                  .text(cellData)
                )
            )
            .append($('<input>')
              .attr('id','stock_'+no+'_tableId')
              .attr('name',"idTable[]")
              .attr('type','hidden')
              .attr('value',+rowData.MAPTBLID)
            );
          },
        },
        {
          "targets": [2],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<div>')
            .addClass('btn-group')
            .attr('role','group')
            .attr('arial-label','Basic Example')
                .append($('<a>')
                .attr('href','#')
                .addClass('btn btn-md btn-primary float-right btn-query')
                .attr('data-whatever','btnquery'+no)
                .css('color','white')
                .css('height','30px')
                .attr('title','Query Table')
                .append($('<span>')
                    .addClass('fa fa-pencil fa-xs')
                    .css('padding-top','40%')
                    // .css('padding-top','10%')
                    // .append('&nbsp;&nbsp;Mapping')
                )
              ) //endbtn1
        ).addClass('text-center');

          },
        },
        {
          "targets": [3],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<div>')
            .addClass('btn-group')
            .attr('role','group')
            .attr('arial-label','Basic Example')
                .append($('<a>')
                .attr('href','#')
                .addClass('btn btn-md btn-info float-right btn-map-record')
                .attr('data-whatever','btnmap_'+no)
                .css('color','white')
                .css('height','30px')
                .attr('title','Mapping Table')
                .append($('<span>')
                    .addClass('fa fa-window-restore fa-xs')
                    .css('padding-top','40%')
                    // .css('padding-top','10%')
                    // .append('&nbsp;&nbsp;Mapping')
                )
              ) //endbtn1
              .append($('<a>')
              .attr('href','#')
              .addClass('btn btn-md btn-danger float-right btn-remove-record')
              .css('color','white')
              .css('height','30px')
              .attr('title','Delete Row')
              .append($('<span>')
                  .addClass('fa fa-times fa-xs')
                  .css('padding-top','55%')
                  .css('font-size','14px')
                  // .css('padding-top','10%')
                  // .append('&nbsp;&nbsp;Delete')
              )
            ) //endbtn2

        ).addClass('text-center');

          },
        }
      ],
      drawCallback: function(settings) {
         initAutoNumeric();
      },
    });


    }

</script>
@endsection
