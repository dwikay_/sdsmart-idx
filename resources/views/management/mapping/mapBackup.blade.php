@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Task Configuration</h4>
              <p class="card-description">
                Task Configuration and Mapping Table
              </p><br>
              <!-- <form class="forms-sample" id="fpro" action="{{url('management/mapping/table/store')}}" method="post" enctype="multipart/form-data"> -->
              <form>
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Task Name</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" id="taskName" placeholder="" name="taskName">
                      </div>
                      <label class=" col-sm-2 col-form-label">Process Type</label>
                      <div class="col-sm-4">
                        <select class="form-control form-control-sm " name="proses_type" id="proses_type">
                          <option disabled selected></option>
                          <option value="1">Insert</option>
                          <option value="0">Insert and Update</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Interval Task</label>
                      <!-- <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" id="taskInterval" onkeypress="return justnumber(event, false)" placeholder="" value="" name="taskInterval">
                      </div>  -->
                      <div class="input-group col-sm-4">
                          <input type="text" title="Recomendation interval is 15 second" class="form-control" aria-label="Task Interval" name="taskInterval" id="taskInterval" onkeypress="return justnumber(event, false)" placeholder="" value="">
                          <div class="input-group-append bg-secondary border-primary">
                            <span class="input-group-text bg-transparent text-white">Second</span>
                          </div>
                        </div>
                      <label class=" col-sm-2 col-form-label" id="labelRownum" style="display:none">Rownum</label>
                      <div class="col-sm-2" style="display:none" id="divRownum">
                        <input type="text" style="display:none" class="form-control form-control-sm" onkeypress="return justnumber(event, false)" id="rownumVal" name="rownumVal" value="0">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12" style="">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">DB Source</label>
                      <div class="col-sm-4">
                        <select class="form-control form-control-chosen dbsource" name="selectSource" id="selectSource">
                          <option disabled selected></option>
                          @foreach($dbSource as $values)
                            <option id="{{$values->DBNAME}}" value="{{$values->DBCONNID}}">{{$values->DBNAME}}</option>
                          @endforeach
                        </select>
                        <input id="valueSource" type="hidden"></input>
                      </div>
                      <label class=" col-sm-2 col-form-label">DB Destination</label>
                      <div class="col-sm-4">
                        <select class="form-control form-control-chosen dbtarget" name="selectTarget" id="selectTarget">
                          <option disabled selected></option>
                          @foreach($dbTarget as $values)
                          <option id="{{$values->DBNAME}}" value="{{$values->DBCONNID}}">{{$values->DBNAME}}</option>
                          @endforeach
                        </select>
                        <input id="valueTarget" type="hidden"></input>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 table-responsive">
                    <table id="table-add" class="table table-sm table-striped table-bordered edTable">
                      <thead style="border:1px solid #000">
                        <tr>
                          <th>Source Table</th>
                          <th>Destination Table</th>
                          <th class="text-center">Query</th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>

                  <div class="col-md-12 text-right">
                    <a class="btn btn-sm btn-success" id="btn-add-field" style="color:white"><i class="fa fa-plus fa-sm"></i> Field</a>
                    <a class="btn btn-sm btn-primary" id="btn-add-field-all" style="color:white"><i class="fa fa-plus fa-sm"></i> All Field</a>
                  </div>

                  <div class="col-12 table-responsive">
                    <table id="table-field" class="table table-sm table-striped table-bordered tableMapField">
                      <thead style="border:1px solid #000">
                        <tr>
                          <th>No</th>
                          <th>PK</th>
                          <th>Field Source</th>
                          <th>Field Type Source</th>
                          <th>PK</th>
                          <th>Field Destination</th>
                          <th>Field Type Destination</th>
                          <th class="text-center">Action</th>
                        </tr>
                      </thead>
                      <tbody>

                      </tbody>
                    </table>
                  </div>

                  <div style="" class="col-md-12 mt-10 text-right">
                    <a class="btn btn-sm btn-danger btn-save" id="btn-save" style="color:white"><i class="fa fa-floppy-o fa-sm"></i>&nbsp;Save Task</a>
                    <a href="{{url('management/mapping/table')}}" class="btn btn-sm btn-primary btn-cancel" style="color:white"><i class="fa fa-arrow-left fa-sm"></i>&nbsp;Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('modal')
    @include('inc.modals.modalTextarea')
@endsection

@section('script')
<script>

var dataFieldTarget = [];
var dataFieldSource = [];
var no = 0;

$('#proses_type').on('change', function(){


  if($('#proses_type').val()=="1"){
    $('#labelRownum').css('display','block');
    $('#divRownum').css('display','block');
    $('#rownumVal').css('display','block');
    $('#rownumVal').val(0);
  }else{
    $('#labelRownum').css('display','none');
    $('#divRownum').css('display','none');
    $('#rownumVal').css('display','none');
    $('#rownumVal').val(0);
  }

});

$('#taskInterval').on('change', function(){
  if($('#taskInterval').val()>10){
    $('#taskInterval').removeClass('bg-danger')
    $('#taskInterval').removeClass('text-white')
  }else{
    $('#taskInterval').addClass('bg-danger')
    $('#taskInterval').addClass('text-white')
  }
});

$('.mapping-input').on('change', function(){
  // alert("test")
    submitForm();
});

$('#btn-save').on('click', function(){

    submitForm();

    // if($('#taskName').val()=="" || $('#taskName').val()==null){
    //
    // } else if ($('#taskName').val()=="" || $('#taskName').val()==null){
    //
    // } else if($('#taskName').val()!="" || $('#taskName').val()!=null){
    //
    //   if($('#taskInterval').val()>10){
    //     $('#taskInterval').removeClass('bg-danger')
    //     $('#taskInterval').removeClass('text-white')
    //   }else{
    //     $('#taskInterval').addClass('bg-danger')
    //     $('#taskInterval').addClass('text-white')
    //   }
    // } else if ($('#proses_type').val()=="" || $('#proses_type').val()==null){
    //
    // } else if ($('#selectSource').val()=="" || $('#selectSource').val()==null){
    //
    // } else if ($('#selectTarget').val()=="" || $('#selectTarget').val()==null){
    //
    // } else if($('#stock_0_stockcode').val()=="" || $('#stock_0_stockcode').val()==null){
    //
    // } else if($('#stock_0_quantity').val()=="" || $('#stock_0_quantity').val()==null){
    //
    // } else{
    //   $('#fpro').submit();
    // }
});
function submitForm(){

    var taskName = $('#taskName').val();
    var taskInterval = $('#taskInterval').val();
    var selectSource = $('#selectSource').val();
    var selectTarget = $('#selectTarget').val();
    var sourceTable = $('#stock_0_stockcode').val();
    var targetTable = $('#stock_0_quantity').val();
    var queryText = $('#queryText').val();
    var prosesType = $('#proses_type').val();
    var rownumVal = $('#rownumVal').val();

    var inputSource = [];
    $('.checkboxSource').each(function(){
      inputSource.push($(this).val());
    });

    var sourceField = [];
    $('.sourceFieldRecord').each(function(){
      sourceField.push($(this).val());
    });

    var sourceType = [];
    $('.source-field-type').each(function(){
      sourceType.push($(this).val());
    });

    //Target
    var inputTarget = [];
    $('.checkboxTarget').each(function(){
      inputTarget.push($(this).val());
    });

    var targetField = [];
    $('.targetFieldRecord').each(function(){
      targetField.push($(this).val());
    });

    var targetType = [];
    $('.target-field-type').each(function(){
      targetType.push($(this).val());
    });

    var dataPost = {
      taskNamePost : taskName,
      taskIntervalPost : taskInterval,
      prosesTypePost : prosesType,
      queryTextPost : queryText,
      rownumValPost : rownumVal,
      selectSourcePost : selectSource,
      selectTargetPost : selectTarget,
      sourceTablePost : sourceTable,
      targetTablePost : targetTable,
      inputSourcePost : inputSource,
      sourceFieldPost : sourceField,
      sourceTypePost : sourceType,
      inputTargetPost : inputTarget,
      targetFieldPost : targetField,
      targetTypesPost : targetType
    }

    console.log(dataPost);

    $.ajax({
      url: "{!! url('management/mapping/table/store') !!}",
      data: dataPost,
      dataType: "JSON",
      type: "POST",
      success:function(data)
        {
          if(data[0]['result']=="success"){
            location.href="{{url('management/mapping/table')}}";
          }
        },
        error: function (jqXHR, textStatus, errorThrown){
          var errorMsg = 'Ajax request failed: ' + errorThrown;
          console.log(errorMsg);
        }
      });
}

$(document).ready(function() {

  // loadSource(function(data){
  //   if(data.length >0){
  //     console.log("gagal")
  //     loadTarget(function(data){
  //       if(data.length >0){
  //
  //       }
  //     })
  //   }
  // });

  $('#timestampVal').datetimepicker({
      language: 'pt-BR'
    });

});

var tableadd = $('#table-add').DataTable({
      autoWidth: false,
      'bSort': false,
      'bPaginate': false,
      "scrollX": true,
      'scrollY': '100vh',
      "scrollCollapse": true,
      'searching' : false,
       processing: true,
      'fnInitComplete': function(){
          $('#table-add').parent().css('max-height','calc(100vh - 600px)').css('height','calc(80vh - 500px)');
      },
      'bInfo': false,
      'drawCallback': function(){
      },
      columns: [
        {data: 'TBLSOURCE', width: "400px"},
        {data: 'TBLDEST', width: "400px"},
        {data: 'SCRIPTQUERY', width: "50px"}
      ],
  });

  var tablefield = $('#table-field').DataTable({
        autoWidth: false,
        'bSort': false,
        'bPaginate': false,
        'searching' : false,
        processing: true,
        language: {
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        columns: [
          {data: 'NO', width: "5px"},
          {data: 'PRIMARYKEYSOURCE', width: "5px"},
          {data: 'FIELDSOURCE', width: "30%"},
          {data: 'TYPESOURCE'},
          {data: 'PRIMARYKEYDEST', width: "5px"},
          {data: 'FIELDDEST',width: "30%"},
          {data: 'TYPEDEST'},
          {data: 'ACTION', width: "50px"}
        ],
    });

$('#stock_0_stockcode').on('change', function(){
  loadFieldSource();
});
$('#stock_0_quantity').on('change', function(){
  loadFieldTarget();
});
$('#btn-add-field').on('click',function(){

    var tableField = $('#table-field').DataTable();
    var countRow = tableField.rows('.tableField').count();
    if($('#stock_0_stockcode').val()==null){

      $.confirm({
          title: 'Information',
          content: 'Please select Table Source',
          buttons: {
              ok: function () {
              },
          }
      });

    }else if($('#stock_0_quantity').val()==null){

      $.confirm({
          title: 'Information',
          content: 'Please select Table Destination',
          buttons: {
              ok: function () {
              },
          }
      });

    }else{
      loadFieldSource(function(dataSource){
        loadFieldTarget(function(dataTarget){
          addFieldRecord(0,0);
        });
      });

    }

});

$('#table-field').on('change', '.checkbox-source', function() {

        var elem = $(this).parents('tr');

        var tr = $(this).closest('tr');
        var id = tr.attr('id').split('_');
        var index = id[1];

        var checkBox = document.getElementById("check_"+index+"_source");
        var text = $('#input_'+index+'_source').val();
        if(checkBox.checked == true){
          $('#input_'+index+'_source').val("1");
        }else if(checkBox.checked == false){
          $('#input_'+index+'_source').val("0");
        }

    });

    $('#table-field').on('change', '.checkbox-target', function() {

            var elem = $(this).parents('tr');

            var tr = $(this).closest('tr');
            var id = tr.attr('id').split('_');
            var index = id[1];

            var checkBox = document.getElementById("check_"+index+"_target");
            var text = $('#input_'+index+'_target').val();
            if(checkBox.checked == true){
              $('#input_'+index+'_target').val("1");
            }else if(checkBox.checked == false){
              $('#input_'+index+'_target').val("0");
            }

        });

    $('#btn-add-field-all').on('click', function(){

      var tableField = $('#table-field').DataTable();
      var countRow = tableField.rows('.tableField').count();
      if($('#stock_0_stockcode').val()==null){

        $.confirm({
            title: 'Information',
            content: 'Please select Table Source',
            buttons: {
                ok: function () {
                },
            }
        });

      }else if($('#stock_0_quantity').val()==null){

        $.confirm({
            title: 'Information',
            content: 'Please select Table Destination',
            buttons: {
                ok: function () {
                },
            }
        });

      }else{
      tableField.clear().draw();

      loadFieldSource(function(dataSource){
        loadFieldTarget(function(dataTarget){
          var countSource = dataSource.length;
          var countTarget = dataTarget.length;

          if(countSource>countTarget){
            var counting = dataSource.length;
          }else{
            var counting = dataTarget.length;
          }

          for (var i = 1; i <= counting; i++) {
            addFieldRecord(countSource,countTarget);
          }
        });
      });

      }
    });

    $('#table-field').on('change', '.checkbox-target', function() {

            var elem = $(this).parents('tr');

            var tr = $(this).closest('tr');
            var id = tr.attr('id').split('_');
            var index = id[1];

            var checkBox = document.getElementById("check_"+index+"_target");
            var text = $('#input_'+index+'_target').val();
            if(checkBox.checked == true){
              $('#input_'+index+'_target').val("1");
            }else if(checkBox.checked == false){
              $('#input_'+index+'_target').val("0");
            }

        });

function addFieldRecord(countSource, countTarget){
  var stockIdListField = [];
  var stockIdListField1 = [];
  var row = $('<tr>')
    .attr('id','row_'+no)
    .addClass('tableMapField')
    .append($('<td>').text(no)
    )
    .append($('<td>')
        .append($('<input>')
            .attr('id','check_'+no+'_source')
            .attr('name',"checkSource[]")
            .attr('type','checkbox')
            .css('margin-left','-5px')
            .css('margin-top','-6px')
            .addClass('form-check-input checkbox-source')
        )
        .append($('<input>')
            .attr('id','input_'+no+'_source')
            .attr('name',"inputSource[]")
            .attr('type',"hidden")
            .addClass('form-check-input checkboxSource')
            .attr('value','0')
        )
    )
    .append($('<td>')
        .append($('<select>')
            .attr('id','source_'+no+'_sourceField')
            .attr('name',"sourceField[]")
            .css('width','100%')
            .addClass('form-control form-control-sm form-control-chosen sourceFieldRecord')
        )
    )
    .append($('<td>')
        .append($('<input>')
            .attr('id','source_'+no+'_sourceFieldType')
            .attr('name',"sourceType[]")
            .css('width','100%')
            .addClass('form-control form-control-sm source-field-type')
            .attr('type','text')
        )
    )
    .append($('<td>')
        .append($('<input>')
            .attr('id','check_'+no+'_target')
            .attr('name',"checkTarget[]")
            .attr('type','checkbox')
            .css('margin-left','-5px')
            .css('margin-top','-6px')
            .addClass('form-check-input checkbox-target')
        )
        .append($('<input>')
            .attr('id','input_'+no+'_target')
            .attr('name',"inputTarget[]")
            .addClass('form-check-input checkboxTarget')
            .attr('type',"hidden")
            .attr('value','0')
        )
    )
    .append($('<td>')
        .append($('<select>')
            .attr('id','target_'+no+'_targetField')
            .attr('name',"targetField[]")
            .css('width','100%')
            .addClass('form-control form-control-sm targetFieldRecord')

        )
    )
    .append($('<td>')
        .append($('<input>')
            .attr('id','target_'+no+'_targetFieldType')
            .attr('name',"targetType[]")
            .css('width','100%')
            .addClass('form-control form-control-sm target-field-type')
            .attr('type','text')
        )
    )
    .append($('<td>')
        .append($('<btn>')
          .addClass('btn btn-danger btn-remove-field')
          .append($('<span>')
            .addClass('fa fa-times')
          )
        )
    ).addClass('text-center');

  tablefield.row.add(row).draw(true);
  initAutoNumeric();

  $.each($(".sourceFieldRecord"), function(){
      stockIdListField.push($(this).val());
  });
  $.each($(".targetFieldRecord"), function(){
      stockIdListField1.push($(this).val());
  });

  if(dataFieldSource.length>0){
    var status = 0;
        dataFieldSource.forEach(function(currentValue,index){
          // console.log($.inArray(currentValue.id+'-'+currentValue.COLUMN_NAME, stockIdListField));
          if($.inArray(currentValue.id+'-'+currentValue.COLUMN_NAME, stockIdListField)  === -1 ){
            $('#source_'+no+'_sourceField').append($('<option>', {
              value: currentValue.id+'-'+currentValue.COLUMN_NAME,
              text : currentValue.COLUMN_NAME
            }));

              if(status==0){
                $('#source_'+no+'_sourceFieldType').val(currentValue.DATA_TYPE);
                status++
              }
          }
        });
  } else {
    $.confirm({
        title: 'Information',
        content: 'No table found on DB Source',
        buttons: {
            ok: function () {
            },
        }
    });
  }


  if(dataFieldTarget.length>0){
    var status = 0;
    dataFieldTarget.forEach(function(currentValue,index){
        if($.inArray(currentValue.id+'-'+currentValue.COLUMN_NAME, stockIdListField1)  === -1 ){
          $('#target_'+no+'_targetField').append($('<option>', {
              value: currentValue.id+'-'+currentValue.COLUMN_NAME,
              text : currentValue.COLUMN_NAME
          }));

          if(status==0){
            $('#target_'+no+'_targetFieldType').val(currentValue.DATA_TYPE);
            status++
          }
        }
    })
  }else{
    $.confirm({
        title: 'Information',
        content: 'No table found on DB Target',
        buttons: {
            ok: function () {
            },
        }
    });
  }

  // refreshStockCodeListField(no);

  no++;
}

function refreshStockCodeListField(no){
    var stockIdListField = [];
    var stockIdListField1 = [];

    $.each($(".sourceFieldRecord"), function(){
        stockIdListField.push($(this).val());
    });
    $.each($(".targetFieldRecord"), function(){
        stockIdListField1.push($(this).val());
    });

    $.each($(".sourceFieldRecord"), function(){
        var stockidSource = $(this).val()
        $(this)
            .find('option')
            .remove()

        var id = $(this).attr('id');

        dataFieldSource.forEach(function(currentValue,index){
          if($.inArray(currentValue.id+'-'+currentValue.COLUMN_NAME, stockIdListField)  === -1 || currentValue.id+'-'+currentValue.COLUMN_NAME == stockidSource){
            $('#'+id).append($('<option>', {
                value: currentValue.id+'-'+currentValue.COLUMN_NAME,
                text : currentValue.COLUMN_NAME
            })
          );
        }
        });

        $(this).val(stockidSource);

        stockIdListField.forEach(function(currentValue,index){
            if(stockidSource != currentValue){
                $("#"+id+" option[value='"+currentValue+"']").remove();
            }
        });

    });

    $.each($(".targetFieldRecord"), function(){
        var stockidTarget = $(this).val()

        $(this)
            .find('option')
            .remove()

        var ids = $(this).attr('id');

        dataFieldTarget.forEach(function(currentValue,index){
          if($.inArray(currentValue.id+'-'+currentValue.COLUMN_NAME, stockIdListField1)  === -1 || currentValue.id+'-'+currentValue.COLUMN_NAME == stockidTarget){
            $('#'+ids).append($('<option>', {
                value: currentValue.id+'-'+currentValue.COLUMN_NAME,
                text : currentValue.COLUMN_NAME
            })
          );
        }
        });

        $(this).val(stockidTarget);

        stockIdListField1.forEach(function(currentValue,index){
            if(stockidTarget != currentValue){
                $("#"+ids+" option[value='"+currentValue+"']").remove();
            }
        });
    });
}

$('#table-field').on('click', '.btn-remove-field', function() {
        var elem = $(this).parents('tr');
        $.confirm({
            title: 'Confirmation',
            content: 'Delete this row ?',
            buttons: {
                cancel: function () {
                },
                confirm: function () {
                    tablefield.row(elem).remove().draw( true );
                    // refreshStockCodeListField();
                },
            }
        });
    });

    $('#table-field').on('click', '.checkbox-source', function() {
            var elem = $(this).parents('tr');

            var tr = $(this).closest('tr');

            var id = tr.attr('id').split('_');
            var index = id[1];

            if($('#check_'+index+'_source:checked').length == $('#check_'+index+'_source').length){
              $('#check_'+index+'_target').prop('checked',true);
              $('#input_'+index+'_target').val("1");
            }else{
              $('#check_'+index+'_target').prop('checked',false);
              $('#input_'+index+'_target').val("0");
            }
        });

    $('#table-field').on('change', '.source-field-type', function() {
      var elem = $(this).parents('tr');

      var tr = $(this).closest('tr');

      var id = tr.attr('id').split('_');
      var index = id[1];

      var typeSource = $('#source_'+index+'_sourceFieldType').val();
      var typeTarget = $('#target_'+index+'_targetFieldType').val();

      if(typeSource=="undefined") {
      } else if(typeTarget=="undefined"){
      } else {
        if(typeSource!=typeTarget){
          $('#source_'+index+'_sourceFieldType').css('background','#eb4d4b');
          $('#target_'+index+'_targetFieldType').css('background','#eb4d4b');
        }else{
          $('#source_'+index+'_sourceFieldType').css('background','#2ecc71');
          $('#target_'+index+'_targetFieldType').css('background','#2ecc71');
        }
      }

  });
  $('#table-field').on('change', '.target-field-type', function() {
          var elem = $(this).parents('tr');

          var tr = $(this).closest('tr');

          var id = tr.attr('id').split('_');
          var index = id[1];

          var typeSource = $('#source_'+index+'_sourceFieldType').val();
          var typeTarget = $('#target_'+index+'_targetFieldType').val();

          if(typeSource=="undefined") {
          } else if(typeTarget=="undefined"){
          } else {
            if(typeSource!=typeTarget){
              $('#source_'+index+'_sourceFieldType').css('background','#eb4d4b');
              $('#target_'+index+'_targetFieldType').css('background','#eb4d4b');
            }else{
              $('#source_'+index+'_sourceFieldType').css('background','#2ecc71');
              $('#target_'+index+'_targetFieldType').css('background','#2ecc71');
            }
          }

});

$('#selectSource').on('change', function(){

  var tableMap = $('#table-add').DataTable();
  var countRow = tableMap.rows( '.tableMap' ).count();
  var valueSelect = $('#selectSource option:selected').html();
  $('#valueSource').val(valueSelect);

  var sourceTable = $('#selectSource').val();
  var sourceTables = document.getElementById("selectSource");
  var targetTable  = document.getElementById("selectTarget");
  var splitSource = sourceTable.split('_sourceTa_');


  if($('#selectSource').val()==null){

  }else if($('#selectTarget').val()==null){

  }
  else{
    if(countRow==0){
      loadSource2(function(dataSource){
        loadTarget2(function(dataTarget){
          addRecord();
        });
      });
    }
    else{
      $.confirm({
          title: 'Confirmation',
          content: 'Do you want to change table ?',
          buttons: {
              cancel: function () {
              },
              confirm: function () {
                tableMap.clear().draw();
                loadSource2(function(dataSource){
                  loadTarget2(function(dataTarget){
                    addRecord();
                  });
                });
              },
          }
      });
    }
  }
});

$('#selectTarget').on('change', function(){

  var tableMap = $('#table-add').DataTable();
  var countRow = tableMap.rows( '.tableMap' ).count()

  var valueTarget = $('#selectTarget option:selected').html();
  $('#valueTarget').val(valueTarget);

  if($('#selectSource').val()==null){

  }else if($('#selectTarget').val()==null){

  }else{

    if(countRow==0){
      loadSource2(function(dataSource){
        loadTarget2(function(dataTarget){
          addRecord();
        });
      });
    }
    else{
      $.confirm({
          title: 'Confirmation',
          content: 'Do you want to change table ?',
          buttons: {
              cancel: function () {
              },
              confirm: function () {
                tableMap.clear().draw();
                loadSource2(function(dataSource){
                  loadTarget2(function(dataTarget){
                    addRecord();
                  });
                });
              },
          }
      });
    }

  }

});

//CHANGE FIELD
$('.edTable').on('change','.stock_0_stockcode', function(){

  var tableMap = $('#table-field').DataTable();
  var countRow = tableMap.rows('.tableMapField').count()

  if($('#stock_0_stockcode').val()==null){

  }else if($('#stock_0_quantity').val()==null){

  }else{
    if(countRow==0){
    }
    else{
      $.confirm({
          title: 'Confirmation',
          content: 'Do you want to change table ?',
          buttons: {
              cancel: function () {
              },
              confirm: function () {
                tableMap.clear().draw();
              },
          }
      });
    }
  }
});

$('.edTable').on('change','.stock_0_quantity', function(){

  var tableMap = $('#table-field').DataTable();
  var countRow = tableMap.rows('.tableMapField').count()

  if($('#stock_0_stockcode').val()==null){

  }else if($('#stock_0_quantity').val()==null){

  }else{
    if(countRow==0){
    }
    else{
      $.confirm({
          title: 'Confirmation',
          content: 'Do you want to change table ?',
          buttons: {
              cancel: function () {
              },
              confirm: function () {
                tableMap.clear().draw();
              },
          }
      });
    }

  }

});


    $('#table-add').on('click', '.btn-query', function() {
        var elem = $(this).parents('tr');
        var tr = $(this).closest('tr');

        var id = tr.attr('id').split('_');
        var index = id[1];

        var sourceTable = $('#stock_'+index+'_stockcode').val();
        var targetTable = $('#stock_'+index+'_quantity').val();
        var tableId = $('#stock_'+index+'_tableId').val();

        if(sourceTable==null){

          $.confirm({
              title: 'Information',
              content: 'Please select Table Source',
              buttons: {
                  ok: function () {
                  },
              }
          });

        }else if(targetTable==null){
          $.confirm({
              title: 'Information',
              content: 'Please select Table Destination',
              buttons: {
                  ok: function () {
                  },
              }
          });
        }else{

          $('#idTableTxt').val(tableId);

          $('#modalTextarea').modal();
        }


        });

    $('#table-add').on('change', '.stock-code', function() {
      $(this).closest(".quantity").val(0);

      refreshStockCodeList();
    });

    function addRecord(){
      var row = $('<tr>')
        .addClass('tableMap')
        .attr('id','row_'+no)
        .append($('<td>')
            .append($('<select>')
                .attr('id','stock_'+no+'_stockcode')
                .addClass('stock_'+no+'_stockcode')
                .attr('name',"sourceTable[]")
                .addClass('form-control form-control-sm form-control-chosen stock-code')
                 .append($('<option>'))
                 .val("")
            )
        )
        .append($('<td>')
            .append($('<select>')
                .attr('id','stock_'+no+'_quantity')
                .addClass('stock_'+no+'_quantity')
                .attr('name',"targetTable[]")
                .addClass('form-control form-control-sm quantity')
                  .append($('<option>'))
                  .val("")
            )
            .append($('<input>')
              .attr('id','stock_'+no+'_tableId')
              .attr('name',"idTable[]")
              .attr('type','hidden')
              .attr('value','nan')
            )
        )


        .append($('<td>')
          .append($('<div>')
          .addClass('btn-group')
          .attr('role','group')
          .attr('arial-label','Basic Example')
              .append($('<a>')
              .attr('href','#')
              .addClass('btn btn-md btn-primary float-right btn-query')
              .attr('data-whatever','btnquery'+no)
              .css('color','white')
              .css('height','30px')
              .attr('title','Query Table')
              .append($('<span>')
                  .addClass('fa fa-pencil fa-xs')
                  .css('padding-top','40%')
              )
            )
      ).addClass('text-center'))

      tableadd.row.add(row).draw( true );
      initAutoNumeric();

      var stockIdList = [];
      var stockIdList1 = [];

      $.each($(".stock-code option:selected"), function(){
          stockIdList.push(parseInt($(this).val()));
      });

      $.each($(".quantity option:selected"), function(){
          stockIdList1.push(parseInt($(this).val()));
      });

      if(dataSource.length>0){
        dataSource.forEach(function(currentValue,index){
            if($.inArray(currentValue, stockIdList)  === -1 ){
              $('#stock_'+no+'_stockcode').append($('<option>', {
                  value: currentValue,
                  text : currentValue
              }));
            }
        });
      }else{
        $.confirm({
            title: 'Information',
            content: 'No table found on DB Source',
            buttons: {
                ok: function () {
                },
            }
        });
      }


      if(dataTarget.length>0){
        dataTarget.forEach(function(currentValue,index){
            if($.inArray(currentValue, stockIdList1)  === -1 ){
              $('#stock_'+no+'_quantity').append($('<option>', {
                  value: currentValue,
                  text : currentValue
              }));
            }
        })
      }else{
        $.confirm({
            title: 'Information',
            content: 'No table found on DB Destination',
            buttons: {
                ok: function () {
                },
            }
        });
      }

      refreshStockCodeList();

      no++;
    }

    function refreshStockCodeList(){
        var stockIdList = [];
        var stockIdList1 = [];

        $.each($(".stock-code option:selected"), function(){
            stockIdList.push($(this).val());
        });
        $.each($(".quantity option:selected"), function(){
            stockIdList1.push($(this).val());
        });

        $.each($(".stock-code"), function(){
            var stockid = $(this).val()

            $(this)
                .find('option')
                .remove()

            var ids = $(this).attr('id');
            $('#'+ids).append($('<option>')
              .attr('selected','selected')
              .attr('disabled','disabled')
              .val("")
            );

            dataSource.forEach(function(currentValue,index){
                $('#'+ids).append($('<option>', {
                    value: currentValue,
                    text : currentValue
                }));
            });

            $(this).val(stockid);
        });

        $.each($(".quantity"), function(){

            var qtyId = $(this).val()

            $(this)
                .find('option')
                .remove()

            var ids = $(this).attr('id');
            $('#'+ids).append($('<option>')
              .attr('selected','selected')
              .attr('disabled','disabled')
              .val("")
            );
            dataTarget.forEach(function(currentValue,index){

                $('#'+ids).append($('<option>', {
                    value: currentValue,
                    text : currentValue
                }));
            });

            $(this).val(qtyId);
        });
    }

    $('#table-add').on('click', '.btn-remove-record', function() {
            var elem = $(this).parents('tr');
            $.confirm({
                title: 'Confirmation',
                content: 'Delete this row ?',
                buttons: {
                    cancel: function () {
                    },
                    confirm: function () {
                        tableadd.row(elem).remove().draw( true );
                        refreshStockCodeList();

                    },
                }
            });
        });

        $('#table-add').on('change', '.quantity', function() {

                var elem = $(this).parents('tr');

                var tr = $(this).closest('tr');
                var id = tr.attr('id').split('_');
                var index = id[1];

                var sourceTable = $('#stock_'+index+'_stockcode').val();
                var targetTable = $('#stock_'+index+'_quantity').val();
                var tableId = $('#stock_'+index+'_tableId').val();
            });

            $('#table-add').on('change', '.stock-code', function() {

                    var elem = $(this).parents('tr');

                    var tr = $(this).closest('tr');
                    var id = tr.attr('id').split('_');
                    var index = id[1];

                    var sourceTable = $('#stock_'+index+'_stockcode').val();
                    var targetTable = $('#stock_'+index+'_quantity').val();
                    var tableId = $('#stock_'+index+'_tableId').val();

                });



    $('#table-add').on('click', '.btn-map-record', function() {

        var elem = $(this).parents('tr');

        var tr = $(this).closest('tr');
        var id = tr.attr('id').split('_');
        var index = id[1];

        var sourceTable = $('#stock_'+index+'_stockcode').val();
        var targetTable = $('#stock_'+index+'_quantity').val();
        var taskId = $('#taskid').val();

        if(sourceTable==null){

          $.confirm({
              title: 'Information',
              content: 'Please select Table Source',
              buttons: {
                  ok: function () {
                  },
              }
          });

        }else if(targetTable==null){
          $.confirm({
              title: 'Information',
              content: 'Please select Table Destination',
              buttons: {
                  ok: function () {
                  },
              }
          });
        }else{



        $.ajax({
          url: "{!! url('management/mapping/table/getColumn') !!}/" + $('#selectSource').val() + "/" + sourceTable,
          data: {},
          dataType: "json",
          type: "get",
          success:function(data)
            {
              var dbscname = $('#valueSource').val();
              $('#dbSourceTable').val(sourceTable)
              $('#taskIdModal').val(taskId)
              loadFieldSource(sourceTable);

            }
          });

          $.ajax({
            url: "{!! url('management/mapping/table/getColumn') !!}/" + $('#selectTarget').val() + "/" + targetTable,
            data: {},
            dataType: "json",
            type: "get",
            success:function(data)
              {
                var dbtgname = $('#valueTarget').val();
                $('#dbTargetTable').val(targetTable)

                loadFieldTarget(targetTable);
              }
            });

        var sourceSelect = $('#selectSource option:selected').html();
        var targetSelect = $('#selectSource option:selected').html();

        $('#dbSourceName').val(sourceSelect);
        $('#dbTargetName').val(targetSelect);
        $('#modal-mapping').modal();
      }

    });

    function callingTable(){

      loadSource(function(data){
        console.log(data);
        if(data.length<=0){
          loadTarget(function(data){
            if(data.length<=0){
              addRecord();
            }
          })
        }
      });

    }

    function loadSource(callback){
      $.ajax({
          url : '{{ url('management/mapping/table/getTable') }}/' + $('#selectSource').val() + "/" + "pgsql",

          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataSource = data;
             if(dataSource.length<0){
                callingTable();
             }
             callingTable();



          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
          }
      });
    }

    function loadSource2(callback){
      console.log()
      $.ajax({
          url : '{{ url('management/mapping/table/getTable') }}/' + $('#selectSource').val() + "/" + "pgsql",
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataSource = data;
             return callback(data);


          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
          }
      });
    }

    function loadTarget(callback){


      $.ajax({
          url : '{{ url('management/mapping/table/getTable') }}/' + $('#selectTarget').val() + "/" + "sqlsrv",
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataTarget = data;
             if(dataTarget.lengt>0){
             }
             callingTable();


          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
          }
      });
    }
    function loadTarget2(callback){


      $.ajax({
          url : '{{ url('management/mapping/table/getTable') }}/' + $('#selectTarget').val() + "/" + "sqlsrv",
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataTarget = data;
             return callback(data);


          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
          }
      });
    }

    function loadFieldSource(callback){
      $.ajax({
          url : '{{ url('management/mapping/table/getColumn') }}/' + $('#selectSource').val() + "/" + $('#stock_0_stockcode').val(),

          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataFieldSource = data;
             return callback(data);
          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
              console.log(errorMsg);
          }
      });

    }

    function loadFieldTarget(callback){
      $.ajax({
          url : '{{ url('management/mapping/table/getColumn') }}/' + $('#selectTarget').val() + "/" + $('#stock_0_quantity').val(),
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataFieldTarget = data;
             return callback(data);
          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
              console.log(errorMsg);
          }
      });
    }
</script>
@endsection
