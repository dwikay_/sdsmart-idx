@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
                @include('inc.button.btnReload')
                @include('inc.button.btnAdd', ['urls' => url('management/mapping/table/create')])
                <h4 class="card-title">Task Configuration</h4>
                <p class="card-description">Task Configuration and Mapping Table</p>
            </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped"style="width:100%">
                    <thead style="">
                      <tr>
                        <th>No.</th>
                        <th>Task Name</th>
                        <th>Task ID</th>
                        <th>Proses Type</th>
                        <th>Interval(s)</th>
                        <th>Server Name - DB Source</th>
                        <th>Table Source</th>
                        <th>Server Name - DB Destination</th>
                        <th>Table Destination</th>
                        <th>Created Date</th>
                        <th>Creted By</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

$(document).ready(function() {

  var table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    "scrollX": true,
    'scrollY': '100vh',
    "scrollCollapse": true,
    'autoWidth': true,
    'bSort': true,
    'bPaginate': true,
    "lengthMenu": [[15,25,50,100], [15,25,50,100]],
    "pageLength": 15,
    'searching' : true,
    ajax:{
      url: "{{ url('management/mapping/table/getIndex') }}",
      dataType: "json",
      type: "GET",
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="11">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    columns: [
      {data: 'TASKID'},
      {data: 'TASKNAME','searchable':true,'orderable':true},
      {data: 'TASKID'},
      {data: 'PROSES_TYPE'},
      {data: 'TASKINTERVAL'},
      {data: 'DBSOURCEID'},
      {data: 'TBLSOURCE'},
      {data: 'DBDESTID'},
      {data: 'TBLDEST'},
      {data: 'DATE'},
      {data: 'CREATED_BY'},
      {data: 'TASKID'}
    ],
    columnDefs: [
      {
        "targets": [0],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).text(row+1);
        },
        orderable: false
      },
      {
        "targets": [1],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
        },
        orderable: false
      },
      {
        "targets": [2],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
          
        },
        orderable: false
      },
      {
        "targets": [3],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
        },
        orderable: false
      },
      {
        "targets": [4],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
          $(td).addClass("text-center");
        },
        orderable: false
      },
      {
        "targets": [5],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
        },
        orderable: false
      },
      {
        "targets": [6],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
        },
        orderable: false
      },
      {
        "targets": [7],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
        },
        orderable: false
      },
      {
        "targets": [8],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
        },
        orderable: false
      },
      {
        "targets": [9],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
        },
        orderable: false
      },
      {
        "targets": [10],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
        },
        orderable: false
      },
      {
        "targets": [11],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).addClass('text-center');
          $(td).append($('@include('inc.button.groupED')'));

        },
        orderable: false
      }
    ],
    drawCallback: function(settings) {
           initAutoNumeric();
        },
        createdRow: function ( row, data, index ) {
            $(row).attr('id','table_'+index);
        },
    order: [[1,'asc']]
  });



  $('.table').on('click','.btn-edit-record', function(){
    var tr = $(this).closest('tr');

    var id = tr.attr('id').split('_');
    var index = id[1];
    var data = table.fnGetData();
    var idTask = data[index].TASKID;
    
    checkUpdateAkses(idTask);
    

});


function checkUpdateAkses(taskid){
  
  $.ajax({
      url : "{{ url('/management/mapping/table/checkingUpdate') }}/" + taskid,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        console.log(data['info']);
        if(data['info']=="Error"){

          $.confirm({
            title: data['info'],
            content: data['alert'],
            type: data['colors'],
            icon: data['icons'],
            typeAnimated: true,
            buttons: {
                close: function () {
                }
              }
            });

        }else{
         updates(taskid)
      }
    },
      error: function (jqXHR, textStatus, errorThrown){
          var errorMsg = 'Ajax request failed stock with: ' + errorThrown;
          console.log(errorMsg);
      }
  });

}

function updates(idTask){
    $.ajax({
      url : "{{ url('/management/mapping/table/Checking-editConfirm') }}/" + idTask,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        console.log(data[0]['result']);
        if(data[0]['result']=="not_allow"){

          $.confirm({
              title: 'Information',
              content: "Cannot edit TASK, TASK is Running!",
              buttons: {
                ok: function () {
                },
              }
           });

        }else{
          location.href="{{url('management/mapping/table/edit')}}"
          + "/" + idTask;
        }
      },
        error: function (jqXHR, textStatus, errorThrown){
            var errorMsg = 'Ajax request failed stock with: ' + errorThrown;
            console.log(errorMsg);
        }
       });
}
$('.table').on('click','.btn-remove-record', function(){
  var tr = $(this).closest('tr');

  var id = tr.attr('id').split('_');
  var index = id[1];
  var data = table.fnGetData();
  var taskid = data[index].TASKID;
  console.log("TASK ID : "+ taskid);
  checkDeleteAkses(taskid);

});

function checkDeleteAkses(taskid){
  
  $.ajax({
      url : "{{ url('/management/mapping/table/checkingDelete') }}/" + taskid,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        console.log(data['info']);
        if(data['info']=="Error"){

          $.confirm({
            title: data['info'],
            content: data['alert'],
            type: data['colors'],
            icon: data['icons'],
            typeAnimated: true,
            buttons: {
                close: function () {
                }
              }
            });

        }else{

     $.ajax({
      url : "{{ url('/management/mapping/table/Checking-deleteConfirm') }}/" + taskid,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        console.log(data[0]['result']);
        if(data[0]['result']=="not_allow"){

          $.confirm({
                            title: 'Information',
                            content: "Cannot delete TASK, TASK is Running!",
                            buttons: {
                                ok: function () {
                                },
                            }
                        });

        }else{
          deletes(taskid);
        }
      },
        error: function (jqXHR, textStatus, errorThrown){
            var errorMsg = 'Ajax request failed stock with: ' + errorThrown;
            console.log(errorMsg);
        }
       });

      }
    },
      error: function (jqXHR, textStatus, errorThrown){
          var errorMsg = 'Ajax request failed stock with: ' + errorThrown;
          console.log(errorMsg);
      }
  });

}

function deletes(taskid){

$.confirm({
      title: 'Confirmation',
      content: 'Delete task ?',
      buttons: {
          cancel: function () {
          },
          confirm: function () {

            $.ajax({
                url: "{!! url('management/mapping/table/checkDelete') !!}/" + taskid,
                data: {},
                dataType: "json",
                type: "get",
                  success:function(data)
                    {
                      console.log(data[0]['code']);
                      if(data[0]['code']=="00" || data[0]['code']==00 || data[0]['code']==33 || data[0]['code']=="33"){

                        location.href="{{url('management/mapping/table/delete')}}"
                        + "/" + taskid;

                      }else if(data[0]['code'] == "401" || data[0]['code'] == 401){
                        
			   $.confirm({
                            title: 'Information',
                            content: "Cannot delete TASK, TASK is Active!",
                            buttons: {
                                ok: function () {
                                },
                            }
                        });

                      }
			else{
                        
			   $.confirm({
                            title: 'Information',
                            content: data[0]['result'],
                            buttons: {
                                ok: function () {
                                },
                            }
                        });

                      }
                    }
              });
          },
      }
  });

}

  $('.btn-eksekusi').on('click', function(){

    $('#modal-eksekusi').modal();

  });

  $('#btnReload').on('click', function(){

      $.confirm({
          title: 'Confirmation',
          content: 'Reload All Task ?',
          buttons: {
              cancel: function () {
              },
              confirm: function () {
                $.ajax({
                    url: "{!! url('management/mapping/table/reloadTask') !!}/",
                    data: {},
                    dataType: "json",
                    type: "get",
                  success:function(data)
                    {
                      console.log(data);
                      $.confirm({
                          title: 'Information',
                          content: data,
                          buttons: {
                              ok: function () {
                                location.href="{{url('management/mapping/table')}}";
                              },
                          }
                      });
                      console.log(data);
                    }
                });
              },
          }
      });
  });

});
</script>
@endsection
