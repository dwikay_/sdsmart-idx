@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Task Configuration</h4>
              <p class="card-description">
                Task Configuration and Mapping Table
              </p><br>
              <!-- <form class="forms-sample" id="fpro" action="{{url('management/mapping/table/store')}}" method="post" enctype="multipart/form-data"> -->
              <form>
                {{csrf_field()}}
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Task Name</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" id="taskName" placeholder="" value="{{$task->TASKNAME}}" readonly name="taskName">
                        <input id="inputQuery" type="hidden" value="{{$task->mapTable->SCRIPTQUERY}}"></input>
                        <input id="idTask" name="idTask" type="hidden" value="{{$task->TASKID}}"></input>
                      </div>
                      <label class=" col-sm-2 col-form-label">Process Type</label>
                      <div class="col-sm-4">
                        <select class="form-control form-control-sm " name="proses_type" id="proses_type">
                          <option disabled selected></option>
                          <option value="1" {{ $task->ISINSERT == "1" ? 'selected' : ''}}>Insert</option>
                          <option value="0" {{ $task->ISINSERT == "0" ? 'selected' : ''}}>Insert and Update</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Interval Task</label>
                      <!-- <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" id="taskInterval" onkeypress="return justnumber(event, false)" placeholder="" value="" name="taskInterval">
                      </div>  -->
                      <div class="input-group col-sm-4">
                          <input type="text" title="Recomendation interval is 15 second" value="{{$task->TASKINTERVAL}}" class="form-control" aria-label="Task Interval" name="taskInterval" id="taskInterval" onkeypress="return justnumber(event, false)" placeholder="" value="">
                          <div class="input-group-append bg-secondary border-primary">
                            <span class="input-group-text bg-transparent text-white">Second</span>
                          </div>
                        </div>
                        @if($task->ISINSERT=="0")
                        <label class=" col-sm-2 col-form-label" id="labelDataType" style="display:none">Data Type</label>
                        <div class="col-sm-2" id="divDataType" style="display:none">
                          <select class="form-control form-control-sm" name="data_type" id="data_type">
                            <option disabled selected></option>
                            <option value="0">Timestamp</option>
                            <option value="1">Rownum</option>
                          </select>
                        </div>
                        <div class="col-sm-2" style="margin-left:-2%" id="divTimRow" style="display:none">
                          <input type="text" value="" style="display:none;width:119%" class="form-control form-control-sm" placeholder="" id="timestampVal" name="timestampVal">
                          <input type="text" style="display:none" class="form-control form-control-sm" placeholder="0" onkeypress="return justnumber(event, false)" id="rownumVal" name="rownumVal">
                        </div>
                        @else
                        <label class=" col-sm-2 col-form-label" id="labelDataType">Data Type</label>
                        <div class="col-sm-2" id="divDataType">
                          <select class="form-control form-control-sm" name="data_type" id="data_type">
                            <option disabled selected></option>
                            <!-- <option value="0" {{ $task->ISROWNUM == "0" ? 'selected' : ''}}>Timestamp</option> -->
                            <option value="1" {{ $task->ISROWNUM == "1" ? 'selected' : ''}}>Rownum</option>
                          </select>
                        </div>
                        @if($task->ISROWNUM=="0")
                        <div class="col-sm-2" style="margin-left:-2%" id="divTim">
                          <input type="text" style="display:block;width:114%" class="form-control form-control-sm" placeholder="" id="timestampVal" name="timestampVal" value="{{$type_data->TIMESTART}}">
                          <input type="text" style="display:none" class="form-control form-control-sm" placeholder="0" onkeypress="return justnumber(event, false)" id="rownumVal" name="rownumVal" value="">
                        </div>
                        @else
                        <div class="col-sm-2" style="margin-left:-2%" id="divRow">
                          <input type="text" value="" style="display:none;width:114%" class="form-control form-control-sm" placeholder="" id="timestampVal" name="timestampVal" value="">
                          <input type="text" style="display:block" class="form-control form-control-sm" placeholder="0" onkeypress="return justnumber(event, false)" id="rownumVal" name="rownumVal" value="{{$task->rownum->ROWNUM}}">
                        </div>
                        @endif
                        @endif


                    </div>
                  </div>
                  <div class="col-md-12" style="">
                    <div class="form-group row">
                      <label class=" col-sm-2 col-form-label">Server Name - DB Source</label>
                      <div class="col-sm-4">
                        <select class="form-control dbsource" name="selectSource" id="selectSource">
                          <option disabled selected></option>
                          @foreach($dbSource as $values)
                            <option value="{{$values->DBCONNID}}" {{ $values->DBCONNID == $task->DBSOURCEID ? 'selected' : ''}}>{{$values->DBSERVERNAME}} - {{$values->DBNAME}}</option>
                          @endforeach
                        </select>
                        <input id="valueSource" type="hidden"></input>
`			<input id="selectSourceBefore" type="hidden" value="{{$task->DBSOURCEID}}"></input>
                      </div>
                      <label class=" col-sm-2 col-form-label">Server Name - DB Destination</label>
                      <div class="col-sm-4">
                        <select class="form-control dbtarget" name="selectTarget" id="selectTarget">
                          <option disabled selected></option>
                          @foreach($dbTarget as $values)
                          <option value="{{$values->DBCONNID}}" {{ $values->DBCONNID == $task->DBDESTID ? 'selected' : ''}}>{{$values->DBSERVERNAME}} - {{$values->DBNAME}}</option>
                          @endforeach
                        </select>
                        <input id="valueTarget" type="hidden"></input>
                        <input id="selectTargetBefore" type="hidden" value="{{$task->DBDESTID}}"></input>
                      </div>
                    </div>
                  </div>

                  <div class="col-12 table-responsive">
                    <table id="table-add" class="table table-sm table-striped table-bordered edTable">
                      <thead style="border:1px solid #000">
                        <tr>
                          <th>Source Table</th>
                          <th>Destination Table</th>
                          <th>Stagging Table</th>
                          <th class="text-center">Query</th>
                        </tr>
                      </thead>
                      <tbody>
                      <tr class="tableMap odd" id="row_0" role="row">
                        <td>
                          <select id="stock_0_stockcode" class="stock_0_stockcode form-control stock-code" name="sourceTable[]">
                            <option selected="selected" disabled="disabled" value=""></option>
                            @foreach($getTabSource as $value)
                              <option value="{{$value}}" {{ $value == $task->mapTable->TBLSOURCE ? 'selected' : ''}}>{{$value}}</option>
                            @endforeach
                          </select>
                          <input type="hidden" value="{{$task->mapTable->MAPTBLID}}" id="idTable" name="idTable">
                        </td>
                        <td>
                          <select id="stock_0_quantity" class="stock_0_quantity form-control form-control-sm quantity" name="targetTable[]">
                            <option selected="selected" disabled="disabled" value=""></option>
                            @foreach($getTabDest as $value)
                              <option value="{{$value}}" {{ $value == $task->mapTable->TBLDEST ? 'selected' : ''}}>{{$value}}</option>
                            @endforeach
                          </select>
			                       <input id="stock_0_tableBefore" type="hidden" value="{{$task->mapTable->TBLSOURCE}}">
			                       <input id="stock_0_qty" type="hidden" value="{{$task->mapTable->TBLDEST}}">
                        </td>
                        <td>
                          <select id="table_0_stagging" class="table_0_stagging form-control stock-code" name="staggingTable[]">
                            <option selected="selected" disabled="disabled" value=""></option>
                            @foreach($getTabSource as $value)
                              <option value="{{$value}}" {{ $value == $task->mapTable->TBLTEMP ? 'selected' : ''}}>{{$value}}</option>
                            @endforeach
                          </select>
                        </td>
                        <td class="text-center">
                          <div class="btn-group" role="group" arial-label="Basic Example">
                              <a href="#" class="btn btn-md btn-primary float-right btn-query" data-whatever="btnquery0" title="Query Table" style="color: white; height: 30px;">
                                <span class="fa fa-pencil fa-xs" style="padding-top: 40%;"></span>
                              </a>
                          </div>
                        </td>
                      </tr>
                      </tbody>
                    </table>
                  </div>

                  <div class="col-md-12 text-right">
                    <a class="btn btn-sm btn-success" id="btn-add-field" style="color:white"><i class="fa fa-plus fa-sm"></i> Field</a>
                    <a class="btn btn-sm btn-primary" id="btn-add-field-all" style="color:white"><i class="fa fa-plus fa-sm"></i> All Field</a>
                  </div>

                  <div class="col-12 table-responsive">
                    <table id="table-field" class="table table-sm table-striped table-bordered tableMapField">
                      <thead style="border:1px solid #000">
                        <tr>
                          <!-- <th>No</th> -->
                          <th>PK</th>
                          <th>Field Source</th>
                          <th>Field Type Source</th>
                          <th>PK</th>
                          <th>Field Destination</th>
                          <th>Field Type Destination</th>
                          <th class="text-center">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i = 1; ?>
                      @foreach($field as $fields)
                      <?php $inya = $i++;?>
                        <tr id="row_{{$inya}}" class="tableMapField text-center odd" role="row">
                          <!-- <td>{{$inya}}</td> -->
                          <td>
                            <input id="check_{{$inya}}_source" {{ $fields->ISPRIMARYKEYSOURCE == "1" ? 'checked' : ''}} name="checkSource[]" type="checkbox" class="form-check-input checkbox-source" style="margin-left: -5px; margin-top: -6px;">
                            <input id="input_{{$inya}}_source" name="inputSource[]" type="hidden" class="form-check-input checkboxSource" value="{{$fields->ISPRIMARYKEYSOURCE}}">
 			    <input id="input_{{$inya}}_sourceClone" name="inputSourceClone[]" type="hidden" class="form-check-input checkboxSourceClone" value="{{$fields->ISPRIMARYKEYSOURCE}}">

                          </td>
                          <td>
                            <select id="source_{{$inya}}_sourceField" class="form-control form-control-sm sourceFieldRecord" name="sourceField[]" style="width: 100%;">
                              <option selected="selected" disabled="disabled" value=""></option>
                              @foreach($getFieldSource as $valueSource)
                                <option value="{{$valueSource['id']}}-{{$valueSource['COLUMN_NAME']}}" {{ $valueSource['COLUMN_NAME'] == $fields->FIELDSOURCE ? 'selected' : ''}}>{{$valueSource['COLUMN_NAME']}}</option>
                              @endforeach
                            </select>
                          </td>
                          <td>
                            <input id="source_{{$inya}}_sourceFieldType" name="sourceType[]" value="{{$fields->FIELDSOURCETYPE}}" class="form-control form-control-sm source-field-type" type="text" style="width: 100%;">
                          </td>
                          <td>
                            <input id="check_{{$inya}}_target" name="checkTarget[]" {{ $fields->ISPRIMARYKEYDEST == "1" ? 'checked' : ''}} type="checkbox" class="form-check-input checkbox-target" style="margin-left: -5px; margin-top: -6px;">
                            <input id="input_{{$inya}}_target" name="inputTarget[]" class="form-check-input checkboxTarget" type="hidden" value="{{$fields->ISPRIMARYKEYDEST}}">
			    <input id="input_{{$inya}}_targetClone" name="inputTargetClone[]" class="form-check-input checkboxTargetClone" type="hidden" value="{{$fields->ISPRIMARYKEYDEST}}">

                          </td>
                          <td>
                            <select id="target_{{$inya}}_targetField" class="form-control form-control-sm targetFieldRecord" name="targetField[]" style="width: 100%;">
                              <option selected="selected" disabled="disabled" value=""></option>
                              @foreach($getFieldDest as $valueDest)
                                <option value="{{$valueDest['id']}}-{{$valueDest['COLUMN_NAME']}}" {{ $valueDest['COLUMN_NAME'] == $fields->FIELDDEST ? 'selected' : ''}}>{{$valueDest['COLUMN_NAME']}}</option>
                              @endforeach
                            </select>
                          </td>
                          <td>
                            <input id="target_{{$inya}}_targetFieldType" name="targetType[]" class="form-control form-control-sm target-field-type" value="{{$fields->FIELDDESTTYPE}}" type="text" style="width: 100%;">
                          </td>
                          <td>
                            <btn class="btn btn-danger btn-remove-field"><span class="fa fa-times"></span></btn>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>

                  <div style="" class="col-md-12 mt-10 text-right">
                    <a class="btn btn-sm btn-danger btn-save" id="btn-save" style="color:white"><i class="fa fa-floppy-o fa-sm"></i>&nbsp;Save Task</a>
                    <a href="{{url('management/mapping/table')}}" class="btn btn-sm btn-primary btn-cancel" style="color:white"><i class="fa fa-arrow-left fa-sm"></i>&nbsp;Cancel</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@section('modal')
    @include('inc.modals.modalTextarea')
@endsection
@include('inc.loading')
@section('script')
<script>

var dataFieldTarget = [];
var dataFieldSource = [];
var no = 0;
loadingHide();
function loadingShow(){
  $('#loading').css('opacity','0.7');
  $('#loading').css('background-color','#000');
  $('#loading-image').css('display','block');
  $('#loading-image').css('color','#FFF');
  $('#loading').css('display','block');
}
function loadingHide(){
  $('#loading').css('opacity','0');
  $('#loading').css('background-color','as');
  $('#loading-image').css('display','none');
  $('#loading').css('display','none');
}
$('#selectSource').select2({
 placeholder: 'Select an Server DB'
});
$('#selectTarget').select2({
 placeholder: 'Select an Server DB'
});
$('#data_type').on('change', function(){

  $('#rownumVal').val("");
  $('#timestampVal').val("");

  var types = $('#data_type').val();
  if(types==1){
    $('#rownumVal').css('display','block');
    $('#timestampVal').css('display','none');
  }else if(types==0){
    $('#rownumVal').css('display','none');
    $('#timestampVal').css('display','block');
  }

});

$('#proses_type').on('change', function(){

  $('#queryText').val("");
  if($('#proses_type').val()=="1"){
    $('#table_0_stagging').removeAttr('disabled');
    $('#divDataType').css('display','block');
    $('#divTimRow').css('display','block');
    $('#labelDataType').css('display','block');
    $('#rownumVal').css('display','block');
    // $('#timestampVal').css('display','block');
    $('#rownumVal').val(0);
  }else{
    $('#rownumVal').css('display','none');
    $('#timestampVal').css('display','none');
    $('#table_0_stagging').attr('disabled','disabled');
    $('#divDataType').css('display','none');
    $('#divTimRow').css('display','none');
    $('#labelDataType').css('display','none');
    $('#rownumVal').val(0);
  }

});

$('#taskInterval').on('change', function(){
  if($('#taskInterval').val()>=15){
    $('#taskInterval').removeClass('bg-danger')
    $('#taskInterval').removeClass('text-white')
  }else{
    $('#taskInterval').addClass('bg-danger')
    $('#taskInterval').addClass('text-white')
  }
});

$('.mapping-input').on('change', function(){
  // alert("test")
    submitForm();
});

$('#btn-save').on('click', function(){
    submitForm();
});
function submitForm(){

  var tableMap = $('#table-field').DataTable();
  var countRow = tableMap.rows( '.tableMapField ' ).count();
  var checkedPS = $(".checkbox-source:checkbox:checked").length;
  var checkedPT = $(".checkbox-target:checkbox:checked").length;
  var queryString = $('#queryText').val();
  var checkQuery = queryString.toLowerCase().includes('limit');

  if($('#taskName').val()=="" || $('#taskName').val()==null){

    $.confirm({
        title: 'Information',
        content: "Task Name cant be empty",
        buttons: {
            ok: function () {
            },
        }
    });


  }else if($('#taskInterval').val()=="" || $('#taskInterval').val()==null){

    $.confirm({
        title: 'Information',
        content: "Task Interval cant be empty",
        buttons: {
            ok: function () {
            },
        }
    });


  }
else if($('#taskInterval').val()<=0){

    $.confirm({
        title: 'Information',
        content: "Task Interval must more than 30 second",
        buttons: {
            ok: function () {
            },
        }
    });


  }
else if($('#selectSource').val()=="" || $('#selectSource').val()==null){

    $.confirm({
        title: 'Information',
        content: "DB Source cant be empty",
        buttons: {
            ok: function () {
            },
        }
    });


  }else if($('#selectTarget').val()=="" || $('#selectTarget').val()==null){

    $.confirm({
        title: 'Information',
        content: "DB Target cant be empty",
        buttons: {
            ok: function () {
            },
        }
    });


  }else if($('#stock_0_stockcode').val()=="" || $('#stock_0_stockcode').val()==null){

    $.confirm({
        title: 'Information',
        content: "Source Table cant be empty",
        buttons: {
            ok: function () {
            },
        }
    });


  }else if($('#stock_0_quantity').val()=="" || $('#stock_0_quantity').val()==null){

    $.confirm({
        title: 'Information',
        content: "Target Table cant be empty",
        buttons: {
            ok: function () {
            },
        }
    });


  }else if($('#queryText').val()=="" || $('#queryText').val()==null){

    $.confirm({
        title: 'Information',
        content: "Query cant be empty",
        buttons: {
            ok: function () {
            },
        }
    });


  }
  else if(checkQuery==true){

    $.confirm({
        title: 'Information',
        content: "Check Query! 'LIMIT' is not allowed",
        buttons: {
            ok: function () {
            },
        }
    });
  }
  else if(countRow==0){

    $.confirm({
        title: 'Information',
        content: "Field cant be empty",
        buttons: {
            ok: function () {
            },
        }
    });

  }

   else if($('#proses_type').val()!="1" || $('#proses_type').val()!=1)
   {
     if(checkedPS==0 || checkedPT==0){

    	$.confirm({
        	title: 'Information',
        	content: "Primary Key cant be empty",
        	buttons: {
            	ok: function () {
            	},
           }
    	});

     } else{
       postForm();
     }

   }

else if($('#proses_type').val()=="1"||$('#proses_type').val()==1){

    if(checkedPS==0){

    	$.confirm({
        	title: 'Information',
        	content: "Primary Key cant be empty in Source Field",
        	buttons: {
            	ok: function () {
            	},
           }
    	});

    } else{
      postForm();
    }

   }

  else {
    postForm();
  }
}

function checkName(){

  $.ajax({
      url: "{!! url('management/mapping/table/checkTaskUpdate') !!}/" + $('#taskName').val() + "/" + $('#idTask').val(),
      data: {},
      dataType: "json",
      type: "get",
        success:function(data)
          {
            console.log(data[0]['status']);
            if(data[0]['status']!="kosong"){

              $.confirm({
                  title: 'Information',
                  content: "Task Name already Exist",
                  buttons: {
                      ok: function () {
                      },
                  }
              });

            }else{
              postForm();
            }
          }
    });

}
function postForm(){
  console.log("post form");

  var idTask = $('#idTask').val();
  var idTable = $('#idTable').val();
  var taskName = $('#taskName').val();
  var taskInterval = $('#taskInterval').val();
  var selectSource = $('#selectSource').val();
  var selectTarget = $('#selectTarget').val();
  var sourceTable = $('#stock_0_stockcode').val();
  var targetTable = $('#stock_0_quantity').val();
  var staggingTable = $('#table_0_stagging').val();
  var queryText = $('#queryText').val();
  var prosesType = $('#proses_type').val();
  var dataType = $('#data_type').val();
  var rownumVal = $('#rownumVal').val();
  var timestampVal = $('#timestampVal').val();

    if($('#queryText').val()==null || $('#queryText').val()==""){
        var qtx = $('#inputQuery').val();
    }else{
        var qtx = $('#queryText').val()
    }
    var queryText = qtx;

    var inputSource = [];
    $('.checkboxSource').each(function(){
      inputSource.push($(this).val());
    });

    var inputSourceClone= [];
    $('.checkboxSourceClone').each(function(){
      inputSourceClone.push($(this).val());
    });


    var sourceField = [];
    $('.sourceFieldRecord').each(function(){
      if($(this).val()){
        sourceField.push($(this).val());
      }
    });

    var sourceType = [];
    $('.source-field-type').each(function(){
      sourceType.push($(this).val());
    });

    //Target
    var inputTarget = [];
    $('.checkboxTarget').each(function(){
      inputTarget.push($(this).val());
    });

    var inputTargetClone= [];
    $('.checkboxTargetClone').each(function(){
      inputTargetClone.push($(this).val());
    });


    var targetField = [];
    $('.targetFieldRecord').each(function(){
      if($(this).val()){
        targetField.push($(this).val());
      }
    });

    var targetType = [];
    $('.target-field-type').each(function(){
      targetType.push($(this).val());
    });


    var countSourceData = sourceField.length;
    var countDestData = targetField.length;

    if(countSourceData!=countDestData){
      $.confirm({
          title: 'Information',
          content: "Field doesnt match, plase check again!",
          buttons: {
              ok: function () {
              },
           }
      });
    }

    var dataPost = {
      idTaskPost : idTask,
      idTablePost : idTable,
      taskNamePost : taskName,
      taskIntervalPost : taskInterval,
      prosesTypePost : prosesType,
      dataTypePost : dataType,
      queryTextPost : queryText,
      rownumValPost : rownumVal,
      timestampValPost : timestampVal,
      selectSourcePost : selectSource,
      selectTargetPost : selectTarget,
      sourceTablePost : sourceTable,
      targetTablePost : targetTable,
      staggingTablePost : staggingTable,
      sourceFieldPost : JSON.stringify(sourceField),
      sourceTypePost : JSON.stringify(sourceType),
      targetFieldPost : JSON.stringify(targetField),
      targetTypesPost : JSON.stringify(targetType),
      inputTargetPost : JSON.stringify(inputTarget),
      inputSourcePost : JSON.stringify(inputSource),
      inputTargetClonePost : JSON.stringify(inputTargetClone),
      inputSourceClonePost : JSON.stringify(inputSourceClone)


    }

    $.ajax({
      url: "{!! url('management/mapping/table/update') !!}",
      data: dataPost,
      dataType: "JSON",
      type: "POST",
      success:function(data)
        {
          if(data[0]['result']=="success"){
            location.href="{{url('management/mapping/table/saved')}}";
          }

        },
        error: function (jqXHR, textStatus, errorThrown){
          var errorMsg = 'Ajax error update Table: ' + errorThrown;
          console.log(errorMsg);
        }
      });

}

$(document).ready(function() {

  loadingHide();
  loadingShow();
  $('#timestampVal').datetimepicker({
      language: 'pt-BR'
    });

    $('.stock-code').select2({
      placeholder: 'Select an option'
    });


    $('.quantity').select2({
      placeholder: 'Select an option'
    });

    $('.targetFieldRecord').select2({
      placeholder: 'Select an option'
    });

    $('.sourceFieldRecord').select2({
      placeholder: 'Select an option'
    });



    if($('#taskInterval').val()>10){
      $('#taskInterval').removeClass('bg-danger')
      $('#taskInterval').removeClass('text-white')
    }else{
      $('#taskInterval').addClass('bg-danger')
      $('#taskInterval').addClass('text-white')
    }

    if($('#proses_type').val()=="1"){
      $('#table_0_stagging').removeAttr('disabled');
      $('#divDataType').css('display','block');
      $('#divTimRow').css('display','block');
      $('#labelDataType').css('display','block');
      // $('#rownumVal').val(0);
    }else{
      $('#table_0_stagging').attr('disabled','disabled');
      $('#divDataType').css('display','none');
      $('#divTimRow').css('display','none');
      $('#labelDataType').css('display','none');
      // $('#rownumVal').val(0);
    }
  loadingHide();
});

var tableadd = $('#table-add').DataTable({
      autoWidth: false,
      'bSort': false,
      'bPaginate': false,
      "scrollX": true,
      'scrollY': '100vh',
      "scrollCollapse": true,
      'searching' : false,
       processing: false,
      // 'fnInitComplete': function(){
      //     $('#table-add').parent().css('max-height','calc(100vh - 600px)').css('height','calc(60vh - 500px)');
      // },
      'bInfo': false,
      'drawCallback': function(){
      },
      columns: [
        {data: 'TBLSOURCE', width: "400px"},
        {data: 'TBLDEST', width: "400px"},
        {data: 'TBLSTAGGING', width: "400px"},
        {data: 'SCRIPTQUERY', width: "50px"}
      ],
  });

  var tablefield = $('#table-field').DataTable({
        autoWidth: false,
        'bSort': false,
        'bPaginate': false,
        'searching' : false,
        processing: true,
        language: {
          processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
        },
        columns: [
          // {data: 'NO', width: "5px"},
          {data: 'PRIMARYKEYSOURCE', width: "5px"},
          {data: 'FIELDSOURCE', width: "30%"},
          {data: 'TYPESOURCE'},
          {data: 'PRIMARYKEYDEST', width: "5px"},
          {data: 'FIELDDEST',width: "30%"},
          {data: 'TYPEDEST'},
          {data: 'ACTION', width: "50px"}
        ],
    });

$('#stock_0_stockcode').on('change', function(){
  loadFieldSource();
});
$('#stock_0_quantity').on('change', function(){
  loadFieldTarget();
});

$('#btn-add-field').on('click',function(){

    var tableField = $('#table-field').DataTable();
    var countRow = tableField.rows('.tableField').count();
    if($('#stock_0_stockcode').val()==null){

      $.confirm({
          title: 'Information',
          content: 'Please select Table Source',
          buttons: {
              ok: function () {
              },
          }
      });

    }else if($('#stock_0_quantity').val()==null){

      $.confirm({
          title: 'Information',
          content: 'Please select Table Destination',
          buttons: {
              ok: function () {
              },
          }
      });

    }else{
      loadFieldSource(function(dataSource){
        loadFieldTarget(function(dataTarget){
          addFieldRecord(0,0);
          $('.sourceFieldRecord').select2({
            placeholder: 'Select an option'
          });
          $('.targetFieldRecord').select2({
            placeholder: 'Select an option'
          });
        });
      });

    }

});

$('#table-field').on('change', '.sourceFieldRecord', function() {

        var elem = $(this).parents('tr');

        var tr = $(this).closest('tr');
        var id = tr.attr('id').split('_');
        var index = id[1];

        var valueField = $('#source_'+index+'_sourceField').val();

        $.ajax({
          url: "{!! url('management/mapping/table/changeFieldFromEngine') !!}/" + $('#selectSource').val() + "/" + $('#stock_0_stockcode').val() + "/" + valueField,
          data: {},
          dataType: "json",
          type: "get",
          success:function(data)
            {
              console.log(data['DATA_TYPE']);
              $('#source_'+index+'_sourceFieldType').val(data['DATA_TYPE']);

            }
          });


    });

    $('#table-field').on('change', '.targetFieldRecord', function() {

            var elem = $(this).parents('tr');

            var tr = $(this).closest('tr');
            var id = tr.attr('id').split('_');
            var index = id[1];

            var valueField = $('#target_'+index+'_targetField').val();

            $.ajax({
              url: "{!! url('management/mapping/table/changeFieldFromEngine') !!}/" + $('#selectTarget').val() + "/" + $('#stock_0_quantity').val() + "/" + valueField,
              data: {},
              dataType: "json",
              type: "get",
              success:function(data)
                {
                  console.log(data['DATA_TYPE']);
                  $('#target_'+index+'_targetFieldType').val(data['DATA_TYPE']);

                }
              });
        });

$('#table-field').on('change', '.checkbox-source', function() {

        var elem = $(this).parents('tr');

        var tr = $(this).closest('tr');
        var id = tr.attr('id').split('_');
        var index = id[1];

        var checkBox = document.getElementById("check_"+index+"_source");
        var text = $('#input_'+index+'_source').val();
        if(checkBox.checked == true){
          $('#input_'+index+'_source').val("1");
        }else if(checkBox.checked == false){
          $('#input_'+index+'_source').val("0");
        }

    });

    $('#table-field').on('change', '.checkbox-target', function() {

            var elem = $(this).parents('tr');

            var tr = $(this).closest('tr');
            var id = tr.attr('id').split('_');
            var index = id[1];

            var checkBox = document.getElementById("check_"+index+"_target");
            var text = $('#input_'+index+'_target').val();
            if(checkBox.checked == true){
              $('#input_'+index+'_target').val("1");
            }else if(checkBox.checked == false){
              $('#input_'+index+'_target').val("0");
            }

        });

        $('#btn-add-field-all').on('click', function(){

          var tableField = $('#table-field').DataTable();
          var countRow = tableField.rows('.tableField').count();
          if($('#stock_0_stockcode').val()==null){

            $.confirm({
                title: 'Information',
                content: 'Please select Table Source',
                buttons: {
                    ok: function () {
                    },
                }
            });

          }else if($('#stock_0_quantity').val()==null){

            $.confirm({
                title: 'Information',
                content: 'Please select Table Destination',
                buttons: {
                    ok: function () {
                    },
                }
            });

          }else{

      loadingShow();

      tableField.clear().draw();

	loadFieldSource(function(dataSource){
            loadFieldTarget(function(dataTarget){

              var countSource = dataSource.length;
              var countTarget = dataTarget.length;

              if(countSource>countTarget){
                var counting = dataSource.length;
              }else{
                var counting = dataTarget.length;
              }
              console.log(countSource+" "+countTarget);

              for (var i = 1; i <= counting; i++) {
                addFieldRecordPls(countSource,countTarget);
              }

              initAutoNumeric();
              loadingHide();
              $('.sourceFieldRecord').select2({
                placeholder: 'Select an option'
              });
              $('.targetFieldRecord').select2({
                placeholder: 'Select an option'
              });

            });
          });

          }
        });

    $('#table-field').on('change', '.checkbox-target', function() {

            var elem = $(this).parents('tr');

            var tr = $(this).closest('tr');
            var id = tr.attr('id').split('_');
            var index = id[1];

            var checkBox = document.getElementById("check_"+index+"_target");
            var text = $('#input_'+index+'_target').val();
            if(checkBox.checked == true){
              $('#input_'+index+'_target').val("1");
            }else if(checkBox.checked == false){
              $('#input_'+index+'_target').val("0");
            }

        });
function addFieldRecordPls(countSource, countTarget){
  var stockIdListField = [];
  var stockIdListField1 = [];
  var row = $('<tr>')
    .attr('id','row_'+no)
    .addClass('tableMapField')
    // .append($('<td>').text(no)
    // )
    .append($('<td>')
        .append($('<input>')
            .attr('id','check_'+no+'_source')
            .attr('name',"checkSource[]")
            .attr('type','checkbox')
            .css('margin-left','-5px')
            .css('margin-top','-6px')
            .addClass('form-check-input checkbox-source')
        )
        .append($('<input>')
            .attr('id','input_'+no+'_source')
            .attr('name',"inputSource[]")
            .attr('type',"hidden")
            .addClass('form-check-input checkboxSource')
            .attr('value','0')
        )
        .append($('<input>')
            .attr('id','input_'+no+'_sourceClone')
            .attr('name',"inputSourceClone[]")
            .attr('type',"hidden")
            .addClass('form-check-input checkboxSourceClone')
            .attr('value','0')
        )

    )
    .append($('<td>')
        .append($('<select>')
            .attr('id','source_'+no+'_sourceField')
            .attr('name',"sourceField[]")
            .css('width','100%')
            .addClass('form-control form-control-sm form-control-chosen sourceFieldRecord')
        )
    )
    .append($('<td>')
        .append($('<input>')
            .attr('id','source_'+no+'_sourceFieldType')
            .attr('name',"sourceType[]")
            .css('width','100%')
            .addClass('form-control form-control-sm source-field-type')
            .attr('type','text')
        )
    )
    .append($('<td>')
        .append($('<input>')
            .attr('id','check_'+no+'_target')
            .attr('name',"checkTarget[]")
            .attr('type','checkbox')
            .css('margin-left','-5px')
            .css('margin-top','-6px')
            .addClass('form-check-input checkbox-target')
        )
        .append($('<input>')
            .attr('id','input_'+no+'_target')
            .attr('name',"inputTarget[]")
            .addClass('form-check-input checkboxTarget')
            .attr('type',"hidden")
            .attr('value','0')
        )
        .append($('<input>')
            .attr('id','input_'+no+'_targetClone')
            .attr('name',"inputTargetClone[]")
            .addClass('form-check-input checkboxTargetClone')
            .attr('type',"hidden")
            .attr('value','0')
        )

    )
    .append($('<td>')
        .append($('<select>')
            .attr('id','target_'+no+'_targetField')
            .attr('name',"targetField[]")
            .css('width','100%')
            .addClass('form-control form-control-sm form-control-chosen targetFieldRecord')

        )
    )
    .append($('<td>')
        .append($('<input>')
            .attr('id','target_'+no+'_targetFieldType')
            .attr('name',"targetType[]")
            .css('width','100%')
            .addClass('form-control form-control-sm target-field-type')
            .attr('type','text')
        )
    )
    .append($('<td>')
        .append($('<btn>')
          .addClass('btn btn-danger btn-remove-field')
          .append($('<span>')
            .addClass('fa fa-times')
          )
        )
    ).addClass('text-center');

  tablefield.row.add(row).draw(true);
  initAutoNumeric();

  $.each($(".sourceFieldRecord"), function(){
      stockIdListField.push($(this).val());
  });
  $.each($(".targetFieldRecord"), function(){
      stockIdListField1.push($(this).val());
  });

  if(dataFieldSource.length>0){
    var status = 0;
        dataFieldSource.forEach(function(currentValue,index){
          console.log($.inArray(currentValue.id+'-'+currentValue.COLUMN_NAME, stockIdListField));
          if($.inArray(currentValue.id+'-'+currentValue.COLUMN_NAME, stockIdListField)  === -1 ){
            $('#source_'+no+'_sourceField').append($('<option>', {
              value: currentValue.id+'-'+currentValue.COLUMN_NAME,
              text : currentValue.COLUMN_NAME
            }));

              if(status==0){
                $('#source_'+no+'_sourceFieldType').val(currentValue.DATA_TYPE);
                status++
              }
          }
        });
  } else {
    $.confirm({
        title: 'Information',
        content: 'No table found on DB Source',
        buttons: {
            ok: function () {
            },
        }
    });
  }


  if(dataFieldTarget.length>0){
    var status = 0;
    dataFieldTarget.forEach(function(currentValue,index){
      console.log($.inArray(currentValue.id+'-'+currentValue.COLUMN_NAME, stockIdListField1));
        if($.inArray(currentValue.id+'-'+currentValue.COLUMN_NAME, stockIdListField1)  === -1 ){
          $('#target_'+no+'_targetField').append($('<option>', {
              value: currentValue.id+'-'+currentValue.COLUMN_NAME,
              text : currentValue.COLUMN_NAME
          }));

          if(status==0){
            $('#target_'+no+'_targetFieldType').val(currentValue.DATA_TYPE);
            status++
          }
        }
    })
  }else{
    $.confirm({
        title: 'Information',
        content: 'No table found on DB Target',
        buttons: {
            ok: function () {
            },
        }
    });
  }

  //refreshStockCodeListField(no);


  no++;
}

function addFieldRecord(countSource, countTarget){
  var stockIdListField = [];
  var stockIdListField1 = [];

  var tableMap = $('#table-field').DataTable();
  var countRows = tableMap.rows( '.tableMapField' ).count();
  var countRow = countRows+1;
  if(countRows==0){
  var resultLastRowAF = countRows+1;
  }else{
  var last_row = tableMap.rows(':last').data();
  var result_last_rowBF = last_row[0]['DT_RowId'].replace('row_','');
  var resultLastRowAF = result_last_rowBF+1;
  }

  //console.log("Jumlah row : "+countRow);
  //console.log(result_last_rowBF);


  var row = $('<tr>')
    .attr('id','row_'+resultLastRowAF )
    .addClass('tableMapField')
    .append($('<td>')
        .append($('<input>')
            .attr('id','check_'+resultLastRowAF +'_source')
            .attr('name',"checkSource[]")
            .attr('type','checkbox')
            .css('margin-left','-5px')
            .css('margin-top','-6px')
            .addClass('form-check-input checkbox-source')
        )
        .append($('<input>')
            .attr('id','input_'+resultLastRowAF +'_source')
            .attr('name',"inputSource[]")
            .attr('type',"hidden")
            .addClass('form-check-input checkboxSource')
            .attr('value','0')
        )
    )
    .append($('<td>')
        .append($('<select>')
            .attr('id','source_'+resultLastRowAF +'_sourceField')
            .attr('name',"sourceField[]")
            .css('width','100%')
            .addClass('form-control form-control-sm form-control-chosen sourceFieldRecord')
        )
    )
    .append($('<td>')
        .append($('<input>')
            .attr('id','source_'+resultLastRowAF +'_sourceFieldType')
            .attr('name',"sourceType[]")
            .css('width','100%')
            .addClass('form-control form-control-sm source-field-type')
            .attr('type','text')
        )
    )
    .append($('<td>')
        .append($('<input>')
            .attr('id','check_'+resultLastRowAF +'_target')
            .attr('name',"checkTarget[]")
            .attr('type','checkbox')
            .css('margin-left','-5px')
            .css('margin-top','-6px')
            .addClass('form-check-input checkbox-target')
        )
        .append($('<input>')
            .attr('id','input_'+resultLastRowAF +'_target')
            .attr('name',"inputTarget[]")
            .addClass('form-check-input checkboxTarget')
            .attr('type',"hidden")
            .attr('value','0')
        )
    )
    .append($('<td>')
        .append($('<select>')
            .attr('id','target_'+resultLastRowAF +'_targetField')
            .attr('name',"targetField[]")
            .css('width','100%')
            .addClass('form-control form-control-sm targetFieldRecord')

        )
    )
    .append($('<td>')
        .append($('<input>')
            .attr('id','target_'+resultLastRowAF +'_targetFieldType')
            .attr('name',"targetType[]")
            .css('width','100%')
            .addClass('form-control form-control-sm target-field-type')
            .attr('type','text')
        )
    )
    .append($('<td>')
        .append($('<btn>')
          .addClass('btn btn-danger btn-remove-field')
          .append($('<span>')
            .addClass('fa fa-times')
          )
        )
    ).addClass('text-center');

  tablefield.row.add(row).draw(true);
  initAutoNumeric();

  $.each($(".sourceFieldRecord"), function(){
      stockIdListField.push($(this).val());
  });
  $.each($(".targetFieldRecord"), function(){
      stockIdListField1.push($(this).val());
  });

  if(dataFieldSource.length>0){
    var status = 0;
        dataFieldSource.forEach(function(currentValue,index){
          if($.inArray(currentValue.id+'-'+currentValue.COLUMN_NAME, stockIdListField)  === -1 ){
            $('#source_'+resultLastRowAF +'_sourceField').append($('<option>', {
              value: currentValue.id+'-'+currentValue.COLUMN_NAME,
              text : currentValue.COLUMN_NAME
            }));

              if(status==0){
                $('#source_'+resultLastRowAF +'_sourceFieldType').val(currentValue.DATA_TYPE);
                status++
              }
          }
        });
  } else {
    $.confirm({
        title: 'Information',
        content: 'No table found on DB Source',
        buttons: {
            ok: function () {
            },
        }
    });
  }


  if(dataFieldTarget.length>0){
    var status = 0;
    dataFieldTarget.forEach(function(currentValue,index){
        if($.inArray(currentValue.id+'-'+currentValue.COLUMN_NAME, stockIdListField1)  === -1 ){
          $('#target_'+resultLastRowAF +'_targetField').append($('<option>', {
              value: currentValue.id+'-'+currentValue.COLUMN_NAME,
              text : currentValue.COLUMN_NAME
          }));

          if(status==0){
            $('#target_'+resultLastRowAF +'_targetFieldType').val(currentValue.DATA_TYPE);
            status++
          }
        }
    })
  }else{
    $.confirm({
        title: 'Information',
        content: 'No table found on DB Target',
        buttons: {
            ok: function () {
            },
        }
    });
  }


  refreshStockCodeListField(resultLastRowAF );


  resultLastRowAF ++;
}


function refreshStockCodeListField(no){
    var stockIdListField = [];
    var stockIdListField1 = [];

    $.each($(".sourceFieldRecord"), function(){
        stockIdListField.push($(this).val());
    });
    $.each($(".targetFieldRecord"), function(){
        stockIdListField1.push($(this).val());
    });

    $.each($(".sourceFieldRecord"), function(){
        var stockidSource = $(this).val()
        $(this)
            .find('option')
            .remove()

        var id = $(this).attr('id');
        dataFieldSource.forEach(function(currentValue,index){
          if($.inArray(currentValue.id+'-'+currentValue.COLUMN_NAME, stockIdListField)  === -1 || currentValue.id+'-'+currentValue.COLUMN_NAME == stockidSource){
            $('#'+id).append($('<option>', {
                value: currentValue.id+'-'+currentValue.COLUMN_NAME,
                text : currentValue.COLUMN_NAME
            })
          );
        }
        });

        $(this).val(stockidSource);

        stockIdListField.forEach(function(currentValue,index){
            if(stockidSource != currentValue){
                $("#"+id+" option[value='"+currentValue+"']").remove();
            }
        });

    });

    $.each($(".targetFieldRecord"), function(){
        var stockidTarget = $(this).val()

        $(this)
            .find('option')
            .remove()

        var ids = $(this).attr('id');

        dataFieldTarget.forEach(function(currentValue,index){
          if($.inArray(currentValue.id+'-'+currentValue.COLUMN_NAME, stockIdListField1)  === -1 || currentValue.id+'-'+currentValue.COLUMN_NAME == stockidTarget){
            $('#'+ids).append($('<option>', {
                value: currentValue.id+'-'+currentValue.COLUMN_NAME,
                text : currentValue.COLUMN_NAME
            })
          );
        }
        });

        $(this).val(stockidTarget);

        stockIdListField1.forEach(function(currentValue,index){
            if(stockidTarget != currentValue){
                $("#"+ids+" option[value='"+currentValue+"']").remove();
            }
        });
    });
}

$('#table-field').on('click', '.btn-remove-field', function() {
        var elem = $(this).parents('tr');
        $.confirm({
            title: 'Confirmation',
            content: 'Delete this row ?',
            buttons: {
                cancel: function () {
                },
                confirm: function () {
                    tablefield.row(elem).remove().draw( true );
                    // refreshStockCodeListField();
                },
            }
        });
    });

    $('#table-field').on('click', '.checkbox-source', function() {
            var elem = $(this).parents('tr');

            var tr = $(this).closest('tr');

            var id = tr.attr('id').split('_');
            var index = id[1];

            if($('#check_'+index+'_source:checked').length == $('#check_'+index+'_source').length){
		if($('#proses_type').val()!="1" || $('#proses_type').val()!=1){
              $('#check_'+index+'_target').prop('checked',true);
              $('#input_'+index+'_target').val("1");
		}
            }else{
		if($('#proses_type').val()!="1" || $('#proses_type').val()!=1){
              $('#check_'+index+'_target').prop('checked',false);
              $('#input_'+index+'_target').val("0");
		}
            }
        });

    $('#table-field').on('change', '.source-field-type', function() {
      var elem = $(this).parents('tr');

      var tr = $(this).closest('tr');

      var id = tr.attr('id').split('_');
      var index = id[1];

      var typeSource = $('#source_'+index+'_sourceFieldType').val();
      var typeTarget = $('#target_'+index+'_targetFieldType').val();

      if(typeSource=="undefined") {
      } else if(typeTarget=="undefined"){
      } else {
        if(typeSource!=typeTarget){
          $('#source_'+index+'_sourceFieldType').css('background','#eb4d4b');
          $('#target_'+index+'_targetFieldType').css('background','#eb4d4b');
        }else{
          $('#source_'+index+'_sourceFieldType').css('background','#2ecc71');
          $('#target_'+index+'_targetFieldType').css('background','#2ecc71');
        }
      }

  });
  $('#table-field').on('change', '.target-field-type', function() {
          var elem = $(this).parents('tr');

          var tr = $(this).closest('tr');

          var id = tr.attr('id').split('_');
          var index = id[1];

          var typeSource = $('#source_'+index+'_sourceFieldType').val();
          var typeTarget = $('#target_'+index+'_targetFieldType').val();

          if(typeSource=="undefined") {
          } else if(typeTarget=="undefined"){
          } else {
            if(typeSource!=typeTarget){
              $('#source_'+index+'_sourceFieldType').css('background','#eb4d4b');
              $('#target_'+index+'_targetFieldType').css('background','#eb4d4b');
            }else{
              $('#source_'+index+'_sourceFieldType').css('background','#2ecc71');
              $('#target_'+index+'_targetFieldType').css('background','#2ecc71');
            }
          }

});

$('#selectSource').on('change', function(){

  var tableMap = $('#table-add').DataTable();
  var tableMapField = $('#table-field').DataTable();
  var countRow = tableMap.rows( '.tableMap' ).count();
  var valueSelect = $('#selectSource option:selected').html();
  $('#valueSource').val(valueSelect);
  var valueBox = $('#selectSource').val();

  var sourceTable = $('#selectSource').val();
  var sourceTables = document.getElementById("selectSource");
  var targetTable  = document.getElementById("selectTarget");



  if($('#selectSource').val()==null){
    $('#selectSourceBefore').val(valueBox);
  }else if($('#selectTarget').val()==null){
    $('#selectSourceBefore').val(valueBox);
  }
  else{
    if(countRow==0){
      $('#selectSourceBefore').val(valueBox);
      loadingShow();
      $.ajax({
        url: "{!! url('management/config/checkConnection') !!}/" + $('#selectSource').val() + "/" + $('#selectTarget').val(),
        data: {},
        dataType: "json",
        type: "get",
        success:function(data)
          {
            loadingHide();
            console.log(data[0]["result"]+"1")
            if(data[0]["result"]=="success"){
              loadSource2(function(dataSource){
                loadTarget2(function(dataTarget){
                  addRecord();
                });
              });
            }else if(data[0]["result"]=="destSuccess"){
                        $.confirm({
                            title: 'Information',
                            content: "Server Name - DB Source cant connect, please check Connection",
                            buttons: {
                                ok: function () {
                                  location.href="{{url('management/mapping/table/edit')}}" + "/" + $('#idTask').val();
                                },
                            }
                        });
                      }
			       else if(data[0]["result"]=="sourceSuccess") {
                        $.confirm({
                            title: 'Information',
                            content: "Server Name - DB Destination cant connect, please check Connection",
                            buttons: {
                                ok: function () {
                                  location.href="{{url('management/mapping/table/edit')}}" + "/" + $('#idTask').val();
                                },
                            }
                        });
                      } else{
                        $.confirm({
                            title: 'Information',
                            content: "Server Name - DB Source & Server Name - DB Destination cant connect, please check onnection",
                            buttons: {
                                ok: function () {
                                  location.href="{{url('management/mapping/table/edit')}}" + "/" + $('#idTask').val();
                                },
                            }
                        });
                      }

          }
        });


    }
    else{

      var oldVal =  $('#selectSourceBefore').val();
      console.log('default'+valueBox);
      console.log('old'+oldVal);
      if(valueBox!=oldVal){
      $.confirm({
          title: 'Confirmation',
          content: 'Do you want to change Database ?',
          buttons: {
              cancel: function () {
                $("#selectSource").val(oldVal).change();
              },
              confirm: function () {
                loadingShow();
                $.ajax({
                  url: "{!! url('management/config/checkConnection') !!}/" + $('#selectSource').val() + "/" + $('#selectTarget').val(),
                  data: {},
                  dataType: "json",
                  type: "get",
                  success:function(data)
                    {
                      loadingHide();
		      console.log(data[0]["result"]+"2")
                      if(data[0]["result"]=="success"){
						$('#selectSourceBefore').val(valueBox);
                        tableMap.clear().draw();
                        tableMapField.clear().draw();
                        loadSource2(function(dataSource){
                          loadTarget2(function(dataTarget){
                            addRecord();
                          });
                        });
                      }else if(data[0]["result"]=="destSuccess"){
                        $.confirm({
                            title: 'Information',
                            content: "Server Name - DB Source cant connect, please check Connection",
                            buttons: {
                                ok: function () {
                                  location.href="{{url('management/mapping/table/edit')}}" + "/" + $('#idTask').val();
                                },
                            }
                        });
                      }
			               else if(data[0]["result"]=="sourceSuccess") {
                        $.confirm({
                            title: 'Information',
                            content: "Server Name - DB Destination cant connect, please check Connection",
                            buttons: {
                                ok: function () {
                                  location.href="{{url('management/mapping/table/edit')}}" + "/" + $('#idTask').val();
                                },
                            }
                        });
                      } else{
                        $.confirm({
                            title: 'Information',
                            content: "Server Name - DB Source & Server Name - DB Destination cant connect, please check onnection",
                            buttons: {
                                ok: function () {
                                  location.href="{{url('management/mapping/table/edit')}}" + "/" + $('#idTask').val();
                                },
                            }
                        });
                      }
                    }
                  });
              },
          }
      });
	}
    }
  }
});

$('#selectTarget').on('change', function(){

  var tableMap = $('#table-add').DataTable();
  var tableMapField = $('#table-field').DataTable();
  var countRow = tableMap.rows( '.tableMap' ).count();
  var valueBox = $('#selectTarget').val();

  var valueTarget = $('#selectTarget option:selected').html();
  $('#valueTarget').val(valueTarget);

  if($('#selectSource').val()==null){
    $('#selectTargetBefore').val(valueBox);
  }else if($('#selectTarget').val()==null){
    $('#selectTargetBefore').val(valueBox);
  }else{

    if(countRow==0){

      $('#selectTargetBefore').val(valueBox);
      loadingShow();
      $.ajax({
        url: "{!! url('management/config/checkConnection') !!}/" + $('#selectSource').val() + "/" + $('#selectTarget').val(),
        data: {},
        dataType: "json",
        type: "get",
        success:function(data)
          {
            loadingHide();
            console.log(data[0]["result"]+"3")
            if(data[0]["result"]=="success"){
              loadSource2(function(dataSource){
                loadTarget2(function(dataTarget){
                  addRecord();
                });
              });
            }else if(data[0]["result"]=="destSuccess"){
                        $.confirm({
                            title: 'Information',
                            content: "Server Name - DB Source cant connect, please check Connection",
                            buttons: {
                                ok: function () {
                                  location.href="{{url('management/mapping/table/edit')}}" + "/" + $('#idTask').val();
                                },
                            }
                        });
                      }
			        else if(data[0]["result"]=="sourceSuccess") {
                        $.confirm({
                            title: 'Information',
                            content: "Server Name - DB Destination cant connect, please check Connection",
                            buttons: {
                                ok: function () {
                                  location.href="{{url('management/mapping/table/edit')}}" + "/" + $('#idTask').val();
                                },
                            }
                        });
                      } else{
                        $.confirm({
                            title: 'Information',
                            content: "Server Name - DB Source & Server Name - DB Destination cant connect, please check onnection",
                            buttons: {
                                ok: function () {
                                  location.href="{{url('management/mapping/table/edit')}}" + "/" + $('#idTask').val();
                                },
                            }
                        });
                      }
          }
        });
    }
    else{

      var oldVal =  $('#selectTargetBefore').val();
      if(valueBox!=oldVal){
      $.confirm({
          title: 'Confirmation',
          content: 'Do you want to change Database ?',
          buttons: {
              cancel: function () {
                $("#selectTarget").val(oldVal).change();
              },
              confirm: function () {
                loadingShow();
                $.ajax({
                  url: "{!! url('management/config/checkConnection') !!}/" + $('#selectSource').val() + "/" + $('#selectTarget').val(),
                  data: {},
                  dataType: "json",
                  type: "get",
                  success:function(data)
                    {
                      loadingHide();
                      console.log(data[0]["result"]+"4")
                      if(data[0]["result"]=="success"){
			$('#selectTargetBefore').val(valueBox);
                        tableMap.clear().draw();
                        tableMapField.clear().draw();
                        loadSource2(function(dataSource){
                          loadTarget2(function(dataTarget){
                            addRecord();
                          });
                        });
                      }else if(data[0]["result"]=="destSuccess"){
                        $.confirm({
                            title: 'Information',
                            content: "Server Name - DB Source cant connect, please check Connection",
                            buttons: {
                                ok: function () {
                                  location.href="{{url('management/mapping/table/edit')}}" + "/" + $('#idTask').val();
                                },
                            }
                        });
                      }
			                   else if(data[0]["result"]=="sourceSuccess") {
                        $.confirm({
                            title: 'Information',
                            content: "Server Name - DB Destination cant connect, please check Connection",
                            buttons: {
                                ok: function () {
                                  location.href="{{url('management/mapping/table/edit')}}" + "/" + $('#idTask').val();
                                },
                            }
                        });
                      } else{
                        $.confirm({
                            title: 'Information',
                            content: "Server Name - DB Source & Server Name - DB Destination cant connect, please check onnection",
                            buttons: {
                                ok: function () {
                                  location.href="{{url('management/mapping/table/edit')}}" + "/" + $('#idTask').val();
                                },
                            }
                        });
                      }
                    }
                  });
              },
          }
      });
	}
    }

  }

});

//CHANGE FIELD
$('.edTable').on('change','.stock_0_stockcode', function(){

  var tableMap = $('#table-field').DataTable();
  var countRow = tableMap.rows('.tableMapField').count();
  var valueSource = $('#stock_0_stockcode').val();

  if($('#stock_0_stockcode').val()==null){
    $('#stock_0_tableBefore').val(valueSource);
  }else if($('#stock_0_quantity').val()==null){
    $('#stock_0_tableBefore').val(valueSource);
  }else{
    if(countRow==0){
      console.log(valueSource);
      $('#stock_0_tableBefore').val(valueSource);
    }
    else{
      var oldVal =  $('#stock_0_tableBefore').val();
      if(valueSource!=oldVal){
        $.confirm({
            title: 'Confirmation',
            content: 'Do you want to change table ?',
            buttons: {
                cancel: function () {
                  $("#stock_0_stockcode").val(oldVal).change();
                },
                confirm: function () {
                  $('#stock_0_tableBefore').val(valueSource);
                  tableMap.clear().draw();
                },
            }
        });
      }
    }
  }
});



$('.edTable').on('change','.stock_0_quantity', function(){

  var tableMap = $('#table-field').DataTable();
  var countRow = tableMap.rows('.tableMapField').count()
  var valueSource = $('#stock_0_quantity').val();

  if($('#stock_0_stockcode').val()==null){
    $('#stock_0_qty').val(valueSource);
  }else if($('#stock_0_quantity').val()==null){
    $('#stock_0_qty').val(valueSource);
  }else{
    if(countRow==0){
      console.log(valueSource);
      $('#stock_0_qty').val(valueSource);
    }
    else{
      var oldVal =  $('#stock_0_qty').val();
      if(valueSource!=oldVal){
      $.confirm({
          title: 'Confirmation',
          content: 'Do you want to change table ?',
          buttons: {
              cancel: function () {
                  $("#stock_0_quantity").val(oldVal).change();
              },
              confirm: function () {
                $('#stock_0_qty').val(valueSource);
                tableMap.clear().draw();
              },
          }
      });
    }
    }

  }

});


$('#table-add').on('click', '.btn-query', function() {

    var textQuery = $('#queryText').val();
    var elem = $(this).parents('tr');
    var tr = $(this).closest('tr');

    var id = tr.attr('id').split('_');
    var index = id[1];

    var sourceTable = $('#stock_'+index+'_stockcode').val();
    var targetTable = $('#stock_'+index+'_quantity').val();
    var tableId = $('#stock_'+index+'_tableId').val();

    if(sourceTable==null){

      $.confirm({
          title: 'Information',
          content: 'Please select Table Source',
          buttons: {
              ok: function () {
              },
          }
      });

    }else if(targetTable==null){
      $.confirm({
          title: 'Information',
          content: 'Please select Table Destination',
          buttons: {
              ok: function () {
              },
          }
      });
    }else{

      var queryRownum = "SELECT %s FROM %s WHERE DATE = :DATE AND  InsertId >= :FIRSTROWNUM AND InsertId < :ENDROWNUM";
      var queryTimestamp = "SELECT %s FROM %s  WHERE DATE = :DATE and TIME >= :TIMESTART and TIME <= :TIMEEND";
      var queryInUp = "SELECT %s FROM %s  WHERE DATE = :DATE";

      if($('#proses_type').val()==1||$('#proses_type').val()=="1"){

        if($('#data_type').val()=="0" || $('#data_type').val()==0){
          if(textQuery=="" || textQuery==null){
              var querys = queryTimestamp;
          } else{
            if(textQuery==queryRownum){
              var querys = queryTimestamp;
            }else{
              var querys = textQuery;
            }
          }

        }else{
          if(textQuery=="" || textQuery==null){
              var querys = queryRownum;
          } else{
            if(textQuery==queryRownum){
              var querys = queryRownum;
            }else{
              var querys = textQuery;
            }
          }
        }

      }else{
        if(textQuery=="" || textQuery==null){
            var querys = queryInUp;
        }else {
          if(textQuery==queryRownum){
            var querys = queryInUp;
          }else{
            var querys = textQuery;
          }
        }
      }

      $('#queryText').val(querys);
      $('#idTableTxt').val(tableId);
      $('#modalTextarea').modal();
    }


    });

    // $('#table-add').on('change', '.stock-code', function() {
    //   $(this).closest(".quantity").val(0);
    //
    //   refreshStockCodeList();
    // });

    function addRecord(){
      var row = $('<tr>')
        .addClass('tableMap')
        .attr('id','row_'+no)
        .append($('<td>')
            .append($('<select>')
                .attr('id','stock_'+no+'_stockcode')
                .addClass('stock_'+no+'_stockcode')
                .attr('name',"sourceTable[]")
                .addClass('form-control form-control-sm form-control-chosen stock-code')
                 .append($('<option>'))
                 .val("")
            )
        )
        .append($('<td>')
            .append($('<select>')
                .attr('id','stock_'+no+'_quantity')
                .addClass('stock_'+no+'_quantity')
                .attr('name',"targetTable[]")
                .addClass('form-control form-control-sm quantity')
                  .append($('<option>'))
                  .val("")
            )
            .append($('<input>')
              .attr('id','stock_'+no+'_tableId')
              .attr('name',"idTable[]")
              .attr('type','hidden')
              .attr('value','nan')
            )
	   .append($('<input>')
              .attr('id','stock_'+no+'_tableBefore')
              .attr('type','hidden')
            )
	   .append($('<input>')
              .attr('id','stock_'+no+'_qty')
              .attr('type','hidden')
            )
        )

        .append($('<td>')
            .append($('<select>')
                .attr('id','table_'+no+'_stagging')
                .addClass('table_'+no+'_stagging')
                .attr('name',"staggingTable[]")
                .attr(attrs,attrs)
                .addClass('form-control stock-code ')
                  .append($('<option>'))
                  .val("")
            )
        )

        .append($('<td>')
          .append($('<div>')
          .addClass('btn-group')
          .attr('role','group')
          .attr('arial-label','Basic Example')
              .append($('<a>')
              .attr('href','#')
              .addClass('btn btn-md btn-primary float-right btn-query')
              .attr('data-whatever','btnquery'+no)
              .css('color','white')
              .css('height','30px')
              .attr('title','Query Table')
              .append($('<span>')
                  .addClass('fa fa-pencil fa-xs')
                  .css('padding-top','40%')
              )
            )
      ).addClass('text-center'))

      tableadd.row.add(row).draw( true );
      initAutoNumeric();

      var stockIdList = [];
      var stockIdList1 = [];

      $.each($(".stock-code option:selected"), function(){
          stockIdList.push(parseInt($(this).val()));
      });

      $.each($(".quantity option:selected"), function(){
          stockIdList1.push(parseInt($(this).val()));
      });

      if(dataSource.length>0){
        dataSource.forEach(function(currentValue,index){
            if($.inArray(currentValue, stockIdList)  === -1 ){
              $('#stock_'+no+'_stockcode').append($('<option>', {
                  value: currentValue,
                  text : currentValue
              }));
            }
        });
      }else{
        $.confirm({
            title: 'Information',
            content: 'No table found on DB Source',
            buttons: {
                ok: function () {
                },
            }
        });
      }


      if(dataTarget.length>0){
        dataTarget.forEach(function(currentValue,index){
            if($.inArray(currentValue, stockIdList1)  === -1 ){
              $('#stock_'+no+'_quantity').append($('<option>', {
                  value: currentValue,
                  text : currentValue
              }));
            }
        })
      }else{
        $.confirm({
            title: 'Information',
            content: 'No table found on DB Destination',
            buttons: {
                ok: function () {
                },
            }
        });
      }

      refreshStockCodeList();

      no++;
    }

    function refreshStockCodeList(){
        var stockIdList = [];
        var stockIdList1 = [];

        $.each($(".stock-code option:selected"), function(){
            stockIdList.push($(this).val());
        });
        $.each($(".quantity option:selected"), function(){
            stockIdList1.push($(this).val());
        });

        $.each($(".stock-code"), function(){

          $('.stock-code').select2({
            placeholder: 'Select an option'
          });

            var stockid = $(this).val()

            $(this)
                .find('option')
                .remove()

            var ids = $(this).attr('id');
            $('#'+ids).append($('<option>')
              .attr('selected','selected')
              .attr('disabled','disabled')
              .val("")
            );

            dataSource.forEach(function(currentValue,index){
                $('#'+ids).append($('<option>', {
                    value: currentValue,
                    text : currentValue
                }));
            });

            $(this).val(stockid);
        });

        $.each($(".quantity"), function(){

            var qtyId = $(this).val()

            $('.quantity').select2({
              placeholder: 'Select an option'
            });

            $(this)
                .find('option')
                .remove()

            var ids = $(this).attr('id');
            $('#'+ids).append($('<option>')
              .attr('selected','selected')
              .attr('disabled','disabled')
              .val("")
            );
            dataTarget.forEach(function(currentValue,index){

                $('#'+ids).append($('<option>', {
                    value: currentValue,
                    text : currentValue
                }));
            });

            $(this).val(qtyId);
        });
    }

    $('#table-add').on('click', '.btn-remove-record', function() {
            var elem = $(this).parents('tr');
            $.confirm({
                title: 'Confirmation',
                content: 'Delete this row ?',
                buttons: {
                    cancel: function () {
                    },
                    confirm: function () {
                        tableadd.row(elem).remove().draw( true );
                        refreshStockCodeList();

                    },
                }
            });
        });

        $('#table-add').on('change', '.quantity', function() {

                var elem = $(this).parents('tr');

                var tr = $(this).closest('tr');
                var id = tr.attr('id').split('_');
                var index = id[1];

                var sourceTable = $('#stock_'+index+'_stockcode').val();
                var targetTable = $('#stock_'+index+'_quantity').val();
                var tableId = $('#stock_'+index+'_tableId').val();
            });

            $('#table-add').on('change', '.stock-code', function() {

                    var elem = $(this).parents('tr');

                    var tr = $(this).closest('tr');
                    var id = tr.attr('id').split('_');
                    var index = id[1];

                    var sourceTable = $('#stock_'+index+'_stockcode').val();
                    var targetTable = $('#stock_'+index+'_quantity').val();
                    var tableId = $('#stock_'+index+'_tableId').val();

                });



    $('#table-add').on('click', '.btn-map-record', function() {

        var elem = $(this).parents('tr');

        var tr = $(this).closest('tr');
        var id = tr.attr('id').split('_');
        var index = id[1];

        var sourceTable = $('#stock_'+index+'_stockcode').val();
        var targetTable = $('#stock_'+index+'_quantity').val();
        var taskId = $('#taskid').val();

        if(sourceTable==null){

          $.confirm({
              title: 'Information',
              content: 'Please select Table Source',
              buttons: {
                  ok: function () {
                  },
              }
          });

        }else if(targetTable==null){
          $.confirm({
              title: 'Information',
              content: 'Please select Table Destination',
              buttons: {
                  ok: function () {
                  },
              }
          });
        }else{



        $.ajax({
          url: "{!! url('management/mapping/table/getColumn') !!}/" + $('#selectSource').val() + "/" + sourceTable,
          data: {},
          dataType: "json",
          type: "get",
          success:function(data)
            {
              var dbscname = $('#valueSource').val();
              $('#dbSourceTable').val(sourceTable)
              $('#taskIdModal').val(taskId)
              loadFieldSource(sourceTable);

            }
          });

          $.ajax({
            url: "{!! url('management/mapping/table/getColumn') !!}/" + $('#selectTarget').val() + "/" + targetTable,
            data: {},
            dataType: "json",
            type: "get",
            success:function(data)
              {
                var dbtgname = $('#valueTarget').val();
                $('#dbTargetTable').val(targetTable)

                loadFieldTarget(targetTable);
              }
            });

        var sourceSelect = $('#selectSource option:selected').html();
        var targetSelect = $('#selectSource option:selected').html();

        $('#dbSourceName').val(sourceSelect);
        $('#dbTargetName').val(targetSelect);
        $('#modal-mapping').modal();
      }

    });

    function callingTable(){

      loadSource(function(data){
        console.log(data);
        if(data.length<=0){
          loadTarget(function(data){
            if(data.length<=0){
              addRecord();
            }
          })
        }
      });

    }

    function loadSource(callback){
      $.ajax({
          url : '{{ url('management/mapping/table/getTableFromEngine') }}/' + $('#selectSource').val(),

          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataSource = data;
             if(dataSource.length<0){
                callingTable();
             }
             callingTable();



          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
          }
      });
    }

    function loadSource2(callback){
      console.log()
      $.ajax({
          url : '{{ url('management/mapping/table/getTableFromEngine') }}/' + $('#selectSource').val(),
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataSource = data;
             return callback(data);


          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
          }
      });
    }

    function loadTarget(callback){


      $.ajax({
          url : '{{ url('management/mapping/table/getTableFromEngine') }}/' + $('#selectTarget').val(),
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataTarget = data;
             if(dataTarget.lengt>0){
             }
             callingTable();


          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
          }
      });
    }
    function loadTarget2(callback){


      $.ajax({
          url : '{{ url('management/mapping/table/getTableFromEngine') }}/' + $('#selectTarget').val(),
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataTarget = data;
             return callback(data);


          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
          }
      });
    }

    function loadFieldSource(callback){
      $.ajax({
          url : '{{ url('management/mapping/table/getTableDefinitionFromEngine') }}/' + $('#selectSource').val() + "/" + $('#stock_0_stockcode').val(),

          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataFieldSource = data;
             return callback(data);
          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
              console.log(errorMsg);
          }
      });

    }

    function loadFieldTarget(callback){
      $.ajax({
          url : '{{ url('management/mapping/table/getTableDefinitionFromEngine') }}/' + $('#selectTarget').val() + "/" + $('#stock_0_quantity').val(),
          type: "GET",
          dataType: "JSON",
          success: function(data)
          {
             dataFieldTarget = data;
             return callback(data);
          },
          error: function (jqXHR, textStatus, errorThrown){
              var errorMsg = 'Ajax request failed table with: ' + errorThrown;
              console.log(errorMsg);
          }
      });
    }
</script>
@endsection
