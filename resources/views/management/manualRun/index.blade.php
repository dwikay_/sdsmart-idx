@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Manual Job</h4>
              <p class="card-description">
                Running Manual Job
              </p><br>
              <form id="form" class="forms-sample" action="{{url('management/config/store')}}" method="POST">
                {{csrf_field()}}
                <div class="row ">

                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">Task Name</label>
                      <div class="col-sm-4">
                        <select class="form-control form-control-sm js-example-basic-singlee" id="jobsId" required name="jobsId">
                          <option value="" disabled selected></option>
                          @foreach($task as $value)
                          <option value="{{$value->TASKID}}">{{$value->TASKNAME}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">Date Job</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" id="dateJob" name="dateJob">
                        <input type="hidden" class="form-control form-control-sm" id="datesJob" name="datesJob">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">Process Type</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" id="PROSES_TYPE" name="PROSES_TYPE" value="" placeholder="" readonly="readonly">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group row">
                      <label class="col-sm-3 col-form-label text-right">Interval</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm " id="INTERVAL" name="INTERVAL" value="" placeholder="" readonly="readonly">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group row">
                      <label class="col-sm-3 col-form-label text-right">DB Source</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm " id="DBSOURCE" name="DBSOURCE" value="" placeholder="" readonly="readonly">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group row">
                      <label class="col-sm-3 col-form-label text-right">Table Source</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm " id="TBLSOURCE" name="TBLSOURCE" value="" placeholder="" readonly="readonly">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                     <div class="form-group row">
                      <label class="col-sm-3 col-form-label text-right">DB Destination</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm " id="DBDEST" name="DBDEST" value="" placeholder="" readonly="readonly">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">Table Destination</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" id="TBLDEST" name="TBLDEST" value="" placeholder="" readonly="readonly">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">Created Date</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" id="DATE" name="DATE" value="" placeholder="" readonly="readonly">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">Count Record</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" id="COUNTRECORD" name="COUNTRECORD" value="" placeholder="" readonly="readonly">
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="form-group row">
                      <label class=" col-sm-3 col-form-label text-right">Status</label>
                      <div class="col-sm-4">
                        <input type="text" class="form-control form-control-sm" id="STATUS" name="STATUS" value="" placeholder="" readonly="readonly">
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 mt-10 text-right" style="margin-top:5%">
                    <a class="btn btn-sm btn-info btn-initial float-left" id="btnInitial" style="color:white;display:none"><i class="fas fa-redo"></i>&nbsp;Initial Data</a>
                    <a class="btn btn-sm btn-success btn-start" id="btnsave" style="color:white"><i class="fa fa-toggle-right fa-sm"></i>&nbsp;Start Job</a>
                    <a class="btn btn-sm btn-danger btn-stop" style="color:white;"><span class="fa fa-ban fa-sm"></span>&nbsp;&nbsp;Stop Job</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
@include('inc.loading')
@section('script')
<script>
loadingHide();
function loadingShow(){
  $('#loading').css('opacity','0.7');
  $('#loading').css('background-color','#000');
  $('#loading-image').css('display','block');
  $('#loading-image').css('color','#FFF');
  $('#loading').css('display','block');
}
function loadingHide(){
  $('#loading').css('opacity','0');
  $('#loading').css('background-color','#FFF');
  $('#loading-image').css('display','none');
  $('#loading').css('display','none');
}

function getDate(){
    var yest = new Date(new Date());
    var month = yest.getMonth();
    var year = yest.getFullYear();
    var date = yest.getDate();
    var startDate = new Date(year, month, date);
    $('#dateJob').datepicker({
        locale: 'id',
        format: 'dd-M-yy',
        autoclose: true
    });
    $('#datesJob').datepicker({
        locale: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true
    });

    $('#dateJob').datepicker('setDate', startDate);
    $('#datesJob').datepicker('setDate', startDate);
}

  $(document).ready(function() {
      getDate();
      $('.js-example-basic-single').select2();
  });

  $('#jobsId').on('change', function(){

    $.ajax({
        url: "{!! url('management/job/manual-run/getOneRecord') !!}/" + $('#jobsId').val(),
        data: {},
        dataType: "json",
        type: "get",
      success:function(data)
        {
          loadData(data[0]);
          console.log(data[0]);
        }
    });

  });


  function convertMoment(date){
    var dates = moment(date).locale('id').format('YYYY-MM-DD');
    $('#datesJob').val(dates);
    // return dates;
   }

   $('#dateJob').on('change', function(){
       convertMoment($('#dateJob').val());
   });

   $('#jobsId').select2({
      placeholder: 'Select an option'
   });


   function loadData(data){

     $('#modal-monitoring').find('.modal-footer').remove();
     $('#PROSES_TYPE').val(data.PROSES_TYPE);
     $('#INTERVAL').val(data.INTERVAL+"s");
     $('#DBSOURCE').val(data.DBSOURCE);
     $('#dateJob').val(data.DATEJOB);
     $('#datesJob').val(data.DATESJOB);
     $('#TBLSOURCE').val(data.TBLSOURCE);
     $('#DBDEST').val(data.DBDEST);
     $('#TBLDEST').val(data.TBLDEST);
     $('#COUNTRECORD').val(data.COUNTRECORD);
     //var dates = moment(data.DATE).locale('en').format('d-MMM-YY');
     $('#DATE').val(data.DATE);

     if(data.STATUS=="Stop"){
       $('#STATUS').removeClass("bg-success");
       $('#STATUS').removeClass("bg-warning");
       $('#STATUS').val(data.STATUS);
       $('#STATUS').addClass("bg-danger");
       $('#STATUS').addClass("text-white");
       $('#btnInitial').css('display','block');
       $('#btnInitial').addClass("text-white");
     }
     else if(data.STATUS=="Complete"){
       $('#STATUS').val(data.STATUS);
       $('#STATUS').removeClass("bg-warning");
       $('#STATUS').removeClass("bg-danger");
       $('#STATUS').addClass("bg-success");
       $('#STATUS').addClass("text-white");
       $('#btnInitial').css('display','none');
     }
	else if(data.STATUS=="Pending" || data.STATUS=="Exception"){
       $('#STATUS').removeClass("bg-success");
       $('#STATUS').removeClass("bg-warning");
       $('#STATUS').val(data.STATUS);
       $('#STATUS').addClass("bg-danger");
       $('#STATUS').addClass("text-white");
       $('#btnInitial').css('display','none');
     }
     else if(data.STATUS=="Finish"){
       $('#STATUS').val(data.STATUS);
       $('#STATUS').removeClass("bg-warning");
       $('#STATUS').removeClass("bg-danger");
       $('#STATUS').removeClass("bg-info");
       $('#STATUS').addClass("bg-success");
       $('#STATUS').addClass("text-white");
       $('#btnInitial').css('display','none');
     }

     else if(data.STATUS=="On Progres"){
       $('#STATUS').val(data.STATUS);
       $('#STATUS').removeClass("bg-success");
       $('#STATUS').removeClass("bg-danger");
       $('#STATUS').removeClass("text-white");
       $('#STATUS').addClass("bg-info text-white");
       $('#STATUS').addClass("text-black");
       $('#btnInitial').css('display','none');
     }
   }

  $('.btn-start').on('click', function(){

    if($('#jobsId').val()=="" || $('#jobsId').val()==null){
      $.confirm({
          title: 'Information',
          content: "Please choose Job ID",
          buttons: {
              ok: function () {
              },
          }
      });
    } else{

      $.confirm({
          title: 'Confirmation',
          content: 'Start this job ?',
          buttons: {
              cancel: function () {
              },
              confirm: function () {
                $.ajax({
                    url: "{!! url('management/job/manual-run/start-job') !!}/" + $('#jobsId').val() + "/" + $('#datesJob').val(),
                    data: {},
                    dataType: "json",
                    type: "get",
                  success:function(data)
                    {
                      $.confirm({
                          title: 'Information',
                          content: data,
                          buttons: {
                              ok: function () {
                                location.href="{{url('monitoring/monJobs')}}";
                              },
                          }
                      });
                      console.log(data);
                    }
                });
              },
          }
      });

    }
  });

  $('#dateJob').on('change', function(){

    $.ajax({
        url: "{!! url('management/job/manual-run/check-record') !!}/" + $('#jobsId').val() + "/" + $('#datesJob').val(),
        data: {},
        dataType: "json",
        type: "get",
      success:function(data)
        {
          $('#COUNTRECORD').val(data);
        }
    });

  });
  $('.btn-stop').on('click', function(){

    if($('#jobsId').val()=="" || $('#jobsId').val()==null){
      $.confirm({
          title: 'Information',
          content: "Please choose Job ID",
          buttons: {
              ok: function () {
              },
          }
      });
    } else{
      $.confirm({
          title: 'Confirmation',
          content: 'Stop this job ?',
          buttons: {
              cancel: function () {
              },
              confirm: function () {
                $.ajax({
                    url: "{!! url('management/job/manual-run/cancel-job') !!}/"  + $('#jobsId').val(),
                    data: {},
                    dataType: "json",
                    type: "get",
                  success:function(data)
                    {
                      $.confirm({
                          title: 'Information',
                          content: data,
                          buttons: {
                              ok: function () {
                                //location.href="{{url('monitoring/monJobs')}}";
				     $.ajax({
        				url: "{!! url('management/job/manual-run/getOneRecord') !!}/" + $('#jobsId').val(),
        				data: {},
        				dataType: "json",
        				type: "get",
      					  success:function(data)
        				{
          					loadData(data[0]);
          					console.log(data[0]);
        				}
    				    });
				     $.ajax({
        				url: "{!! url('management/job/manual-run/check-record') !!}/" + $('#jobsId').val() + "/" + $('#datesJob').val(),
        				data: {},
        				dataType: "json",
        				type: "get",
      					success:function(data)
        				{
          					$('#COUNTRECORD').val(data);
        				}
    				});
                              },
                          }
                      });
                    }
                });
              },
          }
      });
    }
  });

  $('.btn-initial').on('click', function(){

    if($('#jobsId').val()=="" || $('#jobsId').val()==null){
      $.confirm({
          title: 'Information',
          content: "Please choose Job ID",
          buttons: {
              ok: function () {
              },
          }
      });
    }
    else{
      $.confirm({
          title: 'Confirmation',
          content: 'Initial data this record ?',
          buttons: {
              cancel: function () {
              },
              confirm: function () {

                var sessionInterval = setInterval(function(){
                  clearTimeout(setSessionTimeout);
                  sessionTimeout();
                }, 15000);

                loadingShow();
                stayAlive();
                $.ajax({
                    url: "{!! url('management/job/manual-run/initialData') !!}/" + $('#jobsId').val() + "/" + $('#datesJob').val(),
                    data: {},
                    dataType: "json",
                    type: "get",
                  success:function(data)
                    {
                      $.confirm({
                          title: 'Information',
                          content: data,
                          buttons: {
                              ok: function () {
					$.ajax({
        				url: "{!! url('management/job/manual-run/getOneRecord') !!}/" + $('#jobsId').val(),
        				data: {},
        				dataType: "json",
        				type: "get",
      					  success:function(data)
        				{
          					loadData(data[0]);
          					console.log(data[0]);
        				}
    				    });
					$.ajax({
        				url: "{!! url('management/job/manual-run/check-record') !!}/" + $('#jobsId').val() + "/" + $('#datesJob').val(),
        				data: {},
        				dataType: "json",
        				type: "get",
      					success:function(data)
        				{
          					$('#COUNTRECORD').val(data);
        				}
    				});

                              },
                          }
                      });
                      loadingHide();
                      setRollback();
                      clearInterval(sessionInterval)
                    }
                });
              },
          }
      });
    }
  });

</script>
@endsection
