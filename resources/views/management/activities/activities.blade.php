@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
              <div class="form-group mt-10" role="group" aria-label="Basic example">
              </div>
                <h4 class="card-title">Management Job</h4>
                <p class="card-description">Jobs Activities Configuration</p>
                <a href="{{url('management/job/create')}}" class="btn btn-sm btn-danger float-right" style="color:white;"><span class="fa fa-plus fa-sm"></span>&nbsp;&nbsp;Add Job</a>

            </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped">
                    <thead style="">
                      <tr>
                        <th>No.</th>
                        <th>Activities ID</th>
                        <th>Activities Name</th>
                        <th>Description</th>
                        <th>Server Source</th>
                        <th>Server Target</th>
                        <th>Date</th>
                        <th>Progress</th>
                        <th>Count Record</th>
                        <th>Status</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

$(document).ready(function() {

  var table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    searching: true,
    ajax:{
      url: "{{ url('monitoring/monJobs/getIndex') }}",
      dataType: "json",
      type: "GET",
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="11">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    columns: [
      {data: 'noKontrak'},
      {data: 'kodeNasabah'},
      {data: 'namaNasabah'},
      {data: 'nilaiKewajiban'},
      {data: 'totalCollateral'},
      {data: 'ratioPinjaman'},
      {data: 'status'},
      {data: 'topUpCash'},
      {data: 'topUpCollateral'},
      {data: 'noKontrak'}
    ],
    columnDefs: [

      {
          "targets": [3],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
          "targets": [4],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
          "targets": [5],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('percent')
                        .text(cellData*100)
                    )
          },
      },
      {
          "targets": [6],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            var status;
            var classnya;
            switch(cellData){
              case 'N' : status = 'Normal'; classnya = "success"; break;
              case 'MC' : status = 'Topup'; classnya = "warning"; break;
              case 'E' : status = 'Eksekusi'; classnya = "danger"; break;
            }
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('badge badge-'+classnya)
                        .text(status)
                    )
          },
      },
      {
          "targets": [7],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
          "targets": [8],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('currencyNoComma')
                        .text(cellData)
                    )
          },
      },
      {
        "targets": [9],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
        },
        orderable: false
      },
    ],
    drawCallback: function(settings) {
           initAutoNumeric();
        },
        createdRow: function ( row, data, index ) {
            $(row).attr('id','table_'+index);
        }
  });

  $('.table').on('click','.btn-detail', function(){

      var tr = $(this).closest('tr');
      var row = table.api().row( tr );
      var id = tr.attr('id').split('_');
      var index = id[1];
      var data = table.fnGetData();

      $.ajax({
        url: "{!! url('monitoring/transaksi/getDetail') !!}/" + data[index].noKontrak,
        data: {},
        dataType: "json",
        type: "get",
        success:function(data)
        {
          // $('#idrActual').html(data[0]["actualPrice"]);
          loadData(data);
        }
        });
        $('#modal-monitoring').modal();

      // loadData(data[index]);
  });

  $('.btn-eksekusi').on('click', function(){

    $('#modal-eksekusi').modal();

  });

});
</script>
@endsection
