@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="card">
            <div class="card-body">
              <h4 class="card-title">Management jOB</h4>
              <p class="card-description">
              Job Configuration
              </p><br>
              <form class="forms-sample" action="#" method="post">
                {{csrf_field()}}
                <div class="row">

                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label text-right">Job Activities ID</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control form-control-sm" placeholder="" value="" name="kodeEfek">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label text-right">Job Activities Name</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control form-control-sm" placeholder="" value="" name="kodeEfek">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label text-right">Server Source</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control form-control-sm" placeholder="" value="" name="kodeEfek">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label text-right">Server Target</label>
                        <div class="col-sm-4">
                          <input type="text" class="form-control form-control-sm" placeholder="" value="" name="kodeEfek">
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label text-right">Description Activities</label>
                        <div class="col-lg-6">
                          <textarea class="form-control form-control-lg" style="font-size: 12px;" placeholder="" value="" name="kodeEfek"></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group row">
                        <label class=" col-sm-3 col-form-label text-right">Query</label>
                        <div class="col-sm-6">
                          <textarea class="form-control form-control-lg" style="font-size: 12px; margin-top: 0px; margin-bottom: 0px; height: 150px;" placeholder="" value="" name="kodeEfek"></textarea>
                        </div>
                      </div>
                    </div>

                  <div class="col-md-12 mt-10 text-right">
                    <a class="btn btn-sm btn-danger btn-save" style="color:white">&nbsp;&nbsp;Save Config</a>
                    <a class="btn btn-sm btn-primary btn-save" style="color:white">&nbsp;&nbsp;Back</a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
@endsection
