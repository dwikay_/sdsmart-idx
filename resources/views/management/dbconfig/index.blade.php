@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
              <div class="form-group mt-10" role="group" aria-label="Basic example">
              </div>
                <a href="{{url('management/config/create')}}" class="btn btn-sm btn-danger float-right" style="color:white;"><span class="fa fa-plus fa-sm"></span>&nbsp;&nbsp;Add Database</a>
                <h4 class="card-title">DB Config</h4>
                <p class="card-description">Database Configuration</p>
            </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" style="width:100%">
                    <thead style="">
                      <tr>
                        <th>No.</th>
                        <th>Server Name</th>
                        <th>Provider Name</th>
                        <th>Database Type</th>
                        <th>Database Name</th>
                        <th>IP Address</th>
                        <th>IP Port</th>
                        <th>User ID</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

$(document).ready(function() {

  var table = $("#lookup").dataTable({
    processing: true,
    serverSide: true,
    searching: true,
    "lengthMenu": [[15,25,50,100], [15,25,50,100]],
    "pageLength": 15,
    ajax:{
      url: "{{ url('management/config/getIndex') }}",
      dataType: "json",
      type: "GET",
      error: function(){  // error handling
        $(".lookup-error").html("");
        $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="11">No data found in the server</th></tr></tbody>');
        $("#lookup_processing").css("display","none");

      }
    },
    columns: [
      {data: 'DBSERVERNAME'},
      {data: 'DBSERVERNAME'},
      {data: 'DBPROVIDER'},
      {data: 'TYPEDB'},
      {data: 'DBNAME'},
      {data: 'DBHOSTNAME'},
      {data: 'DBPORT'},
      {data: 'DBUSERID'},
      {data: 'DBSERVERNAME'},
    ],
    columnDefs: [
      {
        "targets": [0],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).text(row+1);
        },
        orderable: false
      },
      {
        "targets": [1],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
        },
        orderable: false
      },
      {
        "targets": [2],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
        },
        orderable: false
      },
      {
          "targets": [3],
          "data": null,
          "createdCell": function (td, cellData, rowData, row, col) {
            var status;
            var classnya;
            switch(cellData){
              case '0' : status = 'Source'; classnya = "success"; break;
              case '1' : status = 'Destination'; classnya = "primary"; break;
            }
            $(td).empty();
            $(td).append($('<span>')
                        .addClass('badge badge-'+classnya)
                        .text(status)
                    )

          },
      },
      {
        "targets": [4,5],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
        },
        orderable: false
      },

      {
        "targets": [6,7],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).text(cellData);
        },
        orderable: false
      },
      {
        "targets": [8],
        "createdCell": function (td, cellData, rowData, row, col) {
          $(td).empty();
          $(td).append($('@include('inc.button.groupED')'));

        },
        orderable: false
      }
    ],
    drawCallback: function(settings) {
           initAutoNumeric();
        },
        createdRow: function ( row, data, index ) {
            $(row).attr('id','table_'+index);
        }
  });

  $('.table').on('click','.btn-edit-record', function(){
    var tr = $(this).closest('tr');

    var id = tr.attr('id').split('_');
    var index = id[1];
    var data = table.fnGetData()
    console.log(data[index]);

   checkUpdateAkses(data[index].DBCONNID,data[index].TYPEDB);
    
});

function checkUpdateAkses(id,type){

  $.ajax({
      url : "{{ url('/management/config/checkingUpdate') }}/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        console.log(data['info']);
        if(data['info']=="Error"){

          $.confirm({
            title: data['info'],
            content: data['alert'],
            type: data['colors'],
            icon: data['icons'],
            typeAnimated: true,
            buttons: {
                close: function () {
                }
              }
            });

        }else{
          updates(id,type);
        }
      },
      error: function (jqXHR, textStatus, errorThrown){
          var errorMsg = 'Ajax request failed stock with: ' + errorThrown;
          console.log(errorMsg);
      }
  });

}

function updates(id,type){
$.ajax({
                url: "{!! url('management/config/Checking-update') !!}/" + id + "/" + type,
                data: {},
                dataType: "json",
                type: "get",
                success:function(datas)
                {
                  console.log(datas[0]['notif']);
                  if(datas[0]['notif']=="already"){
                    $.confirm({
                        title: 'Information',
                        content: "Can't edit DB Config. DB Config is active!",
                        buttons: {
                            ok: function () {
                            },
                        }
                    });
                  }else{
                    
                    location.href="{{url('management/config/edit')}}"
    		      + "/" + id
                  }

                }
            });
}
$('.table').on('click','.btn-remove-record', function(){
  var tr = $(this).closest('tr');

  var id = tr.attr('id').split('_');
  var index = id[1];
  var data = table.fnGetData();

  checkDeleteAkses(data[index].DBCONNID,data[index].TYPEDB);
});

function checkDeleteAkses(id,type){

  $.ajax({
      url : "{{ url('/management/config/checkingDelete') }}/" + id,
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        console.log(data['info']);
        if(data['info']=="Error"){

          $.confirm({
            title: data['info'],
            content: data['alert'],
            type: data['colors'],
            icon: data['icons'],
            typeAnimated: true,
            buttons: {
                close: function () {
                }
              }
            });

        }else{
          deletes(id,type);
        }
      },
      error: function (jqXHR, textStatus, errorThrown){
          var errorMsg = 'Ajax request failed stock with: ' + errorThrown;
          console.log(errorMsg);
      }
  });

}

function deletes(id,type){
$.ajax({
                url: "{!! url('management/config/Checking-delete')!!}/" + id + "/" + type,
                data: {},
                dataType: "json",
                type: "get",
              success:function(datas)
                {
                  console.log(datas[0]['notif']+"s");
                  if(datas[0]['notif']=="already"){
                    $.confirm({
                        title: 'Information',
                        content: "Can't delete DB Config. DB Config is active!",
                        buttons: {
                            ok: function () {
                            },
                        }
                    });
                  }else{
		$.confirm({
      			title: 'Confirmation',
      			content: 'Delete config ?',
      			buttons: {
          		cancel: function () {
          		},
          		confirm: function () {
				
                    		location.href="{{url('management/config/delete')}}"
                    		+ "/" + id;
          		},
      		}
  });
                    
                  }

                }
            });



}


  $('.btn-eksekusi').on('click', function(){

    $('#modal-eksekusi').modal();

  });

});
</script>
@endsection
