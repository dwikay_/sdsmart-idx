@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
          <div class="card">
            <div class="card-header">
              <h4 class="card-title">Log Manager</h4>
              <p class="card-description">View Log Jobs</p>
                <div class="form-group row">
                  <div class="col-md-2">
                    <label class="control-label">Task Name</label>
                    <select class="form-control col-md-12 form-choosen" name="taskId" id="taskId" style="">
                      <option value="0" selected disabled></option>
                      <option value="all">All</option>
                      @foreach($task as $tasks)
                      <option value="{{$tasks->TASKID}}">{{$tasks->TASKNAME}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-md-2">
                    <label class="control-label">Start Date</label>
                    <input class="form-control form-control-sm col-md-12" style="" name="startdate" placeholder="Start Date" title="Start Date" id="startdate">
                    <input class="form-control form-control-sm col-md-2" name="startdates" type="hidden" id="startdates">
                  </div>
                  <div class="col-md-2">
                    <label class="control-label">End Date</label>
                    <input class="form-control form-control-sm col-md-12" style="" name="enddate" id="enddate" placeholder="End Date" title="End Date">
                    <input class="form-control form-control-sm col-md-2" name="enddates" type="hidden" id="enddates">
                  </div>
                  <div class="col-md-2">
                    <a class="btn btn-sm btn-info text-white btn-search" id="btnsearch" style="margin-top:20px"><span class="fa fa-search"></span></a>
                  </div>

                </div>
            </div>
            <div class="card-body">
              <div class="row" style="margin-top:20px">
                <div class="col-12 table-responsive">
                  <table id="lookup" class="table table-sm table-striped" style="width:100%">
                    <thead style="">
                      <tr>
                        <th>Task Name</th>
			<th>Task ID</th>
                        <th>Date</th>
			<th>Execution Date</th>
                        <th>IP Database Source</th>
                        <th>IP Database Destination</th>
                        <th>Database Source</th>
                        <th>Table Source</th>
                        <th>Database Destination</th>
                        <th>Table Destination</th>
                        <th>Process Type</th>
                        <th>PK Source</th>
                        <th>PK Destination</th>
                        <th>Flag Staging</th>
                        <th>Process Job</th>
                        <th>Start Time Get Data from Staging</th>
                        <th>Finish Time Get Data from Staging</th>
                        <th>Duration (s) Time Get Data from Staging</th>
                        <th>Start Time Get Data from View</th>
                        <th>Finish Time Get Data from View</th>
                        <th>Duration (s) Time Get Data from View</th>
                        <th>Start Time Insert Data to DWH</th>
                        <th>Finish Time Insert Data to DWH</th>
                        <th>Duration (s) Time Insert Data to DWH</th>
                        <th>Record Start Sequence Rownum from SD-Smarts</th>
                        <th>Record Get Data from SD-Smarts</th>
                        <th>Record Finish Sequence Rownum from SD-Smarts</th>
                        <th>Record Finish insert data to DWH</th>
                        <th>Total Duration (s)</th>
                      </tr>
                    </thead>
                    <tbody>

                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection
@section('script')
<script>

var table = $("#lookup").dataTable({
  dom: 'lrtip',
  columns:
    [
      {data: 'TASKNAME'},
      {data: 'TASKID'},
      {data: 'DATEPROC'},
      {data: 'DATELOG'},
      {data: 'DBSOURCEHOST'},
      {data: 'DBDESTHOST'},
      {data: 'DBSOURCENAME'},
      {data: 'TBLSOURCE'},
      {data: 'DBDESTNAME'},
      {data: 'TBLDEST'},
      {data: 'SCRIPTQUERY'},
      {data: 'PKSOURCE'},
      {data: 'PKDESTINATION'},
      {data: 'ISSTAGING'},
      {data: 'ISMANUALJOB'},
      {data: 'STARTTIMEREFRESH'},
      {data: 'ENDTIMEREFRESH'},
      {data: 'DURATIONTIMEREFRESH'},
      {data: 'STARTTIMEGETDATA'},
      {data: 'FINISHTIMEGETDATA'},
      {data: 'DURATIONTIMEGETDATA'},
      {data: 'STARTTIMEINSERT'},
      {data: 'ENDTIMEINSERT'},
      {data: 'DURATIONTIMEINSERT'},
      {data: 'STARTROWNUM'},
      {data: 'RECORDGETDATA'},
      {data: 'ENDROWNUM'},
      {data: 'RECORDINSERTDATA'},
      {data: 'TOTALDURATION'},
    ],
  });

function getDate(){
    var yest = new Date(new Date());
    var month = yest.getMonth();
    var year = yest.getFullYear();
    var date = yest.getDate();
    var startDate = new Date(year, month, date);
    $('#startdate').datepicker({
        locale: 'id',
        format: 'dd-M-yy',
        autoclose: true
    });
    $('#startdates').datepicker({
        locale: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true
    });

    $('#enddate').datepicker({
        locale: 'id',
        format: 'dd-M-yy',
        autoclose: true
    });
    $('#enddates').datepicker({
        locale: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true
    });

    $('#startdate').datepicker('setDate', startDate);
    $('#startdates').datepicker('setDate', startDate);
    $('#enddate').datepicker('setDate', startDate);
    $('#enddates').datepicker('setDate', startDate);
}

  $('#btnsearch').on('click', function(){

      var taskid = $('#taskId').val();
      var startdate = $('#startdates').val();
      var enddate = $('#enddates').val();

      if(taskid==null||taskid==""){
        $.confirm({
            title: 'Information',
            content: 'Task ID Cant empty',
            buttons: {
                ok: function () {
                },
            }
        });
      }else if(startdate==null||startdate==""){
        $.confirm({
            title: 'Information',
            content: 'Start Date Cant empty',
            buttons: {
                ok: function () {
                },
            }
        });
      }else if(enddate==null||enddate==""){
        $.confirm({
            title: 'Information',
            content: 'End Date Cant empty',
            buttons: {
                ok: function () {
                },
            }
        });
      }else{
        loadData();
      }
  });
  function convertMoment(date){
    var dates = moment(date).locale('id').format('YYYY-MM-DD');
    $('#startdates').val(dates);
    // return dates;
   }
   function convertMoment1(date){
     var dates = moment(date).locale('id').format('YYYY-MM-DD');
     $('#enddates').val(dates);
     // return dates;
    }

  $('.btn-get').on('click', function(){
      $('#modal-closeprice').modal();
      $('.btn-ok').attr('id','btngetsaham')
  });
$('#taskId').select2({
      placeholder: 'Select an option'
   });
  $(document).ready(function() {
    getDate();
    var tanggal = $('#startdates').val();
    var tanggal1 = $('#enddates').val();
    var tipeEfek = $('#tipeEfek').val();
    loadData(tanggal,tipeEfek);
  });

  $('#startdate').on('change', function(){
      convertMoment($('#startdate').val());
  });

  $('#enddate').on('change', function(){
      convertMoment1($('#enddate').val());
  });

  $('#tipeEfek').on('change', function(){
      loadData();
  });

  function loadData(){

    $('#lookup').dataTable().fnDestroy();

    var tanggal = $('#startdates').val();
    var tanggal1 = $('#enddates').val();
    var taskId = $('#taskId').val();

    var table = $("#lookup").dataTable({
      processing: true,
      serverSide: true,
      dom: 'lrtip',
      ajax:{
        url: "{{ url('log/logJob/getIndex') }}",
        dataType: "json",
        type: "POST",
        data: {
          startDate: tanggal,
          endDate: tanggal1,
          taskId: taskId,
        },
        error: function(){  // error handling
          $(".lookup-error").html("");
          $("#lookup").append('<tbody class="employee-grid-error"><tr><th class="text-center" colspan="23">No data found in the server</th></tr></tbody>');
          $("#lookup_processing").css("display","none");
        }
      },
      columns: [
        {data: 'TASKNAME'},
        {data: 'TASKID'},
        {data: 'DATEPROC'},
	{data: 'DATELOG'},
        {data: 'DBSOURCEHOST'},
        {data: 'DBDESTHOST'},
        {data: 'DBSOURCENAME'},
        {data: 'TBLSOURCE'},
        {data: 'DBDESTNAME'},
        {data: 'TBLDEST'},
        {data: 'SCRIPTQUERY'},
        {data: 'PKSOURCE'},
        {data: 'PKDESTINATION'},
        {data: 'ISSTAGING'},
        {data: 'ISMANUALJOB'},
        {data: 'STARTTIMEREFRESH'},
        {data: 'ENDTIMEREFRESH'},
        {data: 'DURATIONTIMEREFRESH'},
        {data: 'STARTTIMEGETDATA','searchable':true,'orderable':true},
        {data: 'FINISHTIMEGETDATA'},
        {data: 'DURATIONTIMEGETDATA'},
        {data: 'STARTTIMEINSERT'},
        {data: 'ENDTIMEINSERT'},
        {data: 'DURATIONTIMEINSERT'},
        {data: 'STARTROWNUM'},
        {data: 'RECORDGETDATA'},
        {data: 'ENDROWNUM'},
        {data: 'RECORDINSERTDATA'},
        {data: 'TOTALDURATION'},
      ],
      "lengthMenu": [[15,25,50,100], [15,25,50,100]],
      "pageLength": 15,
      "scrollX": true,
      'scrollY': '100vh',
      "scrollCollapse": true,
      'autoWidth': true,
      'bSort': true,
      'bPaginate': true,
      'searching' : true,
      columnDefs: [
              {
                  "targets": [0,1],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                  },
              },
              {
                  "targets": [2],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    var dates = moment(cellData).locale('en').format("D-MMM-YY");
                    $(td).text(dates);
                  },
              },
	      {
                  "targets": [3],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    var dates = moment(cellData).locale('en').format("D-MMM-YY");
                    $(td).text(dates);
              },
              },
              {
                  "targets": [4,5,6,7,8,9],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                  },
              },
		          {
                  "targets": [10],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    if(rowData.ISSTAGING=="1" || rowData.ISSTAGING==1){
                        $(td).text("Insert");
                    }else{
                        $(td).text("Insert & Update");
                    }
                  },
              },
		          {
                  "targets": [11,12],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                  },
              },
              {
                  "targets": [13],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    if(cellData=="1" || cellData==1){
                        $(td).text("Staging");
                    }else{
                        $(td).text("Non Staging");
                    }
                  },
              },
              {
                  "targets": [14],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
			console.log(rowData.ISMANUALJOB)
                    if(rowData.ISMANUALJOB=="1" || rowData.ISMANUALJOB==1){
                        $(td).text("Manual Job");
                    }else if(rowData.ISMANUALJOB=="2" || rowData.ISMANUALJOB==2){
                        $(td).text("Initial Data");
                    }else{
                        $(td).text("Normal Job");
                    }
                  },
              },

              {
                  "targets": [15,16],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                    $(td).addClass("text-center");
                  },
              },
              {
                  "targets": [17],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                  },
              },
              {
                  "targets": [18,19],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                    $(td).addClass("text-center");
                  },
              },
              {
                  "targets": [20],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                  },
              },
              {
                  "targets": [21,22],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                    $(td).addClass("text-center");
                  },
              },
              {
                  "targets": [23],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                  },
              },
              {
                  "targets": [24,25,26,27],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
		    var rdc;
		    if(cellData==""){rdc = "0";} else {rdc = cellData;}
                    $(td).text(rdc);
                    $(td).addClass("text-center");
                  },
              },
              {
                  "targets": [28],
                  "createdCell": function (td, cellData, rowData, row, col) {
                    $(td).empty();
                    $(td).text(cellData);
                  },
              }
          ],
          createdRow: function ( row, data, index ) {
              $(row).attr('id','table_'+index);
          },

      drawCallback: function(settings) {
             initAutoNumeric();
          },
	order: [[18,'desc']]

      });
  }
</script>
@endsection
