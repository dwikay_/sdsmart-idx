@extends('layouts.apps')
@section('content')
<div class="content-wrapper">
  <div class="row">
    <div class="col-md-4 d-flex align-items-stretch grid-margin">
      <div class="row flex-grow">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <h4 class="card-title">Log Manager</h4>
              <p class="card-description">
                Backup Log Job
              </p>
              <form class="forms-sample">
                <div class="form-group">
                  <label for="exampleInputEmail1">Task Name</label>
                  <select class="form-control form-choosen" name="taskId" id="taskId">
                    <option value="0" selected disabled></option>
			                 <option value="all">All</option>
                    @foreach($task as $tasks)
                    <option value="{{$tasks->TASKID}}">{{$tasks->TASKNAME}}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group" style="margin-top:13px">
                  <label for="exampleInputEmail1">Start Date</label>
                  <input class="form-control form-control-sm" name="startdate" placeholder="Start Date" title="Start Date" id="startdate">
                  <input class="form-control form-control-sm" name="startdates" type="hidden" id="startdates">
                </div>
                <div class="form-group" style="margin-top:13px">
                  <label for="exampleInputEmail1">End Date</label>
                  <input class="form-control form-control-sm" name="enddate" id="enddate" placeholder="End Date" title="End Date">
                  <input class="form-control form-control-sm" name="enddates" type="hidden" id="enddates">
                </div>
                <div class="row mt-10 text-right">
                  <div class="col-md-12">
                    <a class="btn btn-sm btn-danger btn-download" id="btndownload" style="color:white">
                     <span class="fa fa-download"></span>&nbsp;&nbsp;Download Log
                    </a>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
@endsection
@include('inc.loading')
@section('script')
<script>

loadingHide();
function loadingShow(){
  $('#loading').css('opacity','0.7');
  $('#loading').css('background-color','#000');
  $('#loading-image').css('display','block');
  $('#loading-image').css('color','#FFF');
  $('#loading').css('display','block');
}
function loadingHide(){
  $('#loading').css('opacity','0');
  $('#loading').css('background-color','#FFF');
  $('#loading-image').css('display','none');
  $('#loading').css('display','none');
}

function getDate(){
    var yest = new Date(new Date());
    var month = yest.getMonth();
    var year = yest.getFullYear();
    var date = yest.getDate();
    var startDate = new Date(year, month, date);
    $('#startdate').datepicker({
        locale: 'id',
        format: 'dd-M-yy',
        autoclose: true
    });
    $('#startdates').datepicker({
        locale: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true
    });

    $('#enddate').datepicker({
        locale: 'id',
        format: 'dd-M-yy',
        autoclose: true
    });
    $('#enddates').datepicker({
        locale: 'id',
        format: 'yyyy-mm-dd',
        autoclose: true
    });

    $('#startdate').datepicker('setDate', startDate);
    $('#startdates').datepicker('setDate', startDate);
    $('#enddate').datepicker('setDate', startDate);
    $('#enddates').datepicker('setDate', startDate);
}

$(document).ready(function() {
  getDate();
});

$('#taskId').select2({
      placeholder: 'Select an option'
   });
function convertMoment(date){
  var dates = moment(date).locale('id').format('YYYY-MM-DD');
  $('#startdates').val(dates);
  // return dates;
 }

 function convertMoment1(date){
   var dates = moment(date).locale('id').format('YYYY-MM-DD');
   $('#enddates').val(dates);
   // return dates;
  }

  $('#startdate').on('change', function(){
      convertMoment($('#startdate').val());
  });

  $('#enddate').on('change', function(){
      convertMoment1($('#enddate').val());
  });

  $('#btndownload').on('click', function(){

      var taskid = $('#taskId').val();
      var startdate = $('#startdates').val();
      var enddate = $('#enddates').val();

      if(taskid==null||taskid==""){
        $.confirm({
            title: 'Information',
            content: 'Task ID Cant empty',
            buttons: {
                ok: function () {
                },
            }
        });
      }else if(startdate==null||startdate==""){
        $.confirm({
            title: 'Information',
            content: 'Start Date Cant empty',
            buttons: {
                ok: function () {
                },
            }
        });
      }else if(enddate==null||enddate==""){
        $.confirm({
            title: 'Information',
            content: 'End Date Cant empty',
            buttons: {
                ok: function () {
                },
            }
        });
      }else{
        var sessionInterval = setInterval(function(){
          clearTimeout(setSessionTimeout);
          sessionTimeout();
        }, 15000);
        loadingShow();
        stayAlive();
        $.ajax({
          url: "{!! url('log/logBackup/download') !!}/" + taskid + "/" + startdate + "/" + enddate,
          data: {},
          dataType: "json",
          type: "get",
          success:function(data)
            {
              console.log(data);
              var $a = $("<a>");
              $a.attr("href","{{url('log/logBackup/downloadExcel')}}/"+data);
              $("body").append($a);
              $a.attr("download",data);
              $a[0].click();
              $a.remove();
              loadingHide();
              setTimeout(function(){
                $.ajax({
                  url: "{!! url('log/logBackup/removeExcel') !!}/" + data,
                  data: {},
                  dataType: "json",
                  type: "get",
                  success:function(data)
                    {
                      setRollback();
                      clearInterval(sessionInterval)
                    }
                  });
              },1000)
            }
          });


      }
  });

</script>
@endsection
