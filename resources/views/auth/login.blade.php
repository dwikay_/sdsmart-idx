@extends('layouts.app')

@section('content')
<div class="container-fluid" id="div-login" style="height: calc(100vh)">
    <div class="row">
        <div class="offset-md-7 col-md-4" style="margin-top: calc(5%)">
            <div class="card float-right" style="width: 400px;border-radius: 10px">
                <div class="card-body" style="padding: 60px">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row col-offset-1">
                            <div class="col-md-6">
                                <img src="{{asset('logins/images/idx.png')}}" width="100">
                            </div>
                            <div class="col-md-6 text-center">
                                <img src="{{asset('logins/images/sdm.png')}}" width="100" style="margin-top: 20%">
                            </div>
                        </div>
                        <div class="form-group row">
                          <h2 class="line_horizontal"></h2>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                                <h4 class="font-weight-bold text-muted">Login</h4>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-login" name="email" value="{{ old('email') }}" required autofocus autocomplete="off" placeholder="Username">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-login" name="password" required autocomplete="off" placeholder="Password">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row pt-4">
                            <div class="offset-md-2 col-md-8">
                                <button type="submit" class="btn btn-md btn-login btn-block">
                                    Sign In
                                </button>
                            </div>
                        </div>
                        @if (Route::has('password.request'))
                            <div class="form-group row">
                                <div class="col-md-12 text-center">
                                    <a class="btn btn-link" style="font-size: smaller;" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                </div>
                            </div>
                        @endif
                    </form>
                </div>
		 <div class="col-md-12 text-center">
			<label class="label label-control" style="color:#A33333"><b>SD-SMART Engine {{$version}}</b></label><br>
			<label class="label label-control" style="color:#A33333"><b>Built with {{$built}}</b></label>
		</div>
		<div class="col-md-12 text-center">
			<label class="label label-control pt-3" style="color:#A33333"><b>2019 Indonesia Stock Exchange v1.0</b></label>
		</div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    @parent
    <script>
    @if(count($errors) > 0 || Session::has('success') || Session::has('info') || Session::has('warning'))
      $.confirm({
        title: '{{Session::get('info')}}',
        content: '{{Session::get('alert')}}',
        type: '{{Session::get('colors')}}',
        icon: '{{Session::get('icons')}}',
        typeAnimated: true,
        buttons: {
            close: function () {
            }
          }
        });
    @elseif(count($errors) == 0)
    @endif
    </script>
@endsection
