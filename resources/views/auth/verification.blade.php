@extends('layouts.app')

@section('content')
<div class="container-fluid" id="div-login" style="height: calc(100vh)">
    <div class="row">
        <div class="col-md-4" style="margin-top: calc(5%);margin-left:calc(30%)">
            <div class="card float-right" style="width: 400px;border-radius: 10px">
                <div class="card-body" style="padding: 60px">
                    <form method="POST" id="fpro" action="{{url('verification', $username)}}">
                        @csrf

                        <div class="form-group row col-offset-1">
                            <div class="col-md-6">
                                <img src="{{asset('logins/images/idx.png')}}" width="100">
                            </div>
                            <div class="col-md-6 text-center">
                                <img src="{{asset('logins/images/sdm.png')}}" width="100" style="margin-top: 20%">
                            </div>
                        </div>
                        <div class="form-group row">
                          <h2 class="line_horizontal"></h2>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 text-center">
                                <h4 class="font-weight-bold text-muted">Change Password</h4>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control form-login" name="password" required autofocus autocomplete="off" placeholder="New Password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="confirm_password" type="password" class="form-control form-login" name="confirm_password" required autocomplete="off" placeholder="Confirm Password">
                            </div>
                        </div>
                        <div class="form-group row mb-1">
                            <div class="offset-md-2 col-md-8">
                                <a href="javascript:void(0)" id="btn-simpan" class="btn btn-md btn-login btn-block">
                                    Sign In
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>

$('.btn-login').on('click', function(){

    var password = $('#password').val();
    var confirm = $('#confirm_password').val();

     if(password==""){
      $.alert({
        title: 'Error',
        content: 'Password cant be empty!',
        icon: 'fa fa-times'
      });
    }
    else if(confirm==""){
      $.alert({
        title: 'Error',
        content: 'Password confirmation cant be empty',
        icon: 'fa fa-times'
      });
    }
    else{
      if(password!=confirm){
        $.alert({
          title: 'Error',
          content: 'Password doesnt match!',
          icon: 'fa fa-times'
        });
      }
      else{

        if(password.length >= 6){

          var uppercase = /[a-z]/;
          var lowercase = /[A-Z]/;
          var number = /[0-9]/;
          var spesChar = /[!@#\$%\^&]/;
          var valid = number.test(password) && uppercase.test(password) && lowercase.test(password) && spesChar.test(password); //match a letter _and_ a number

          if(valid==true){
            $('#fpro').submit();
          }else{
            $.alert({
              title: 'Error',
              content: 'Password must using alphanumeric ,uppercase, lowercase and special simbols !',
              icon: 'fa fa-times'
            });
          }

        }else{
          $.alert({
            title: 'Error',
            content: 'Password must more than 6 character !',
            icon: 'fa fa-times'
          });

        }
      }
    }
});

</script>
@endsection
@section('js')
    @parent
    <script>
    @if(count($errors) > 0 || Session::has('success') || Session::has('info') || Session::has('warning'))
      $.confirm({
        title: '{{Session::get('info')}}',
        content: '{{Session::get('alert')}}',
        type: '{{Session::get('colors')}}',
        icon: '{{Session::get('icons')}}',
        typeAnimated: true,
        buttons: {
            close: function () {
            }
          }
        });
    @elseif(count($errors) == 0)
    @endif
        $('#refreshCaptcha').on('click',function(){
            $('#imgCaptcha').attr('src','{{url('/captcha/image?589528204?_=')}}'+Math.random());
            var captcha= document.getElementById('captcha');
            if(captcha){
              captcha.focus()
            }
        });
    </script>
@endsection
