<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(url('login'));
});

// Auth::routes();
Route::get('login', 'Auth\LoginController@getLogin')->name('login');
// Route::post('login', 'Auth\LoginController@postLogin')->name('login');
Route::post('login', 'Auth\LoginController@postLoginLdap')->name('login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('verification/{username}', 'Users\VerificationController@getVerification')->name('verification');
Route::post('verification/{username}', 'Users\VerificationController@postVerification')->name('verification');
Route::get('getLdap', 'Users\UserManagementController@getLdap')->name('ldap');

Route::group(['middleware' => ['web']], function () {

   // Route::group(['middleware' => ['auth'] ], function () {

      Route::group(['prefix' => 'monitoring/monJobs'], function () {

        Route::group(['middleware' => 'roler:monitoring/monJobs'], function () {

          Route::get('', 'Monitoring\MonitoringController@index')->name('monitoring');
          Route::get('getIndex', 'Monitoring\MonitoringController@getIndex')->name('monitoring');
          Route::get('getOneRecord/{id}', 'Monitoring\MonitoringController@getOneRecord')->name('monitoring');
          Route::get('start-job/{id}', 'Monitoring\MonitoringController@startJob')->name('monitoring');
          Route::get('cancel-job/{id}', 'Monitoring\MonitoringController@cancelJob')->name('monitoring');
          Route::get('initialData/{id}', 'Monitoring\MonitoringController@initialData')->name('monitoring');

          });

      });

      Route::group(['prefix' => 'management/config'], function () {

        Route::group(['middleware' => 'roler:management/config'], function () {
	  
          Route::get('', 'Management\ConfigController@indexConfig')->name('management');
          Route::get('/getIndex', 'Management\ConfigController@getIndex')->name('management');
          });

        Route::group(['middleware' => 'rolec:management/config'], function () {
          Route::get('/create', 'Management\ConfigController@createConfig')->name('management');
          Route::post('/store', 'Management\ConfigController@store')->name('management');
          Route::get('/testConnection', 'Management\ConfigController@testConnection')->name('management');
          Route::get('checkTask/{taskName}', 'Management\ConfigController@checkTask')->name('management');
          });

        Route::group(['middleware' => 'roleu:management/config'], function () {
          Route::get('/edit/{id}', 'Management\ConfigController@edit')->name('management');
          Route::post('/update/{id}', 'Management\ConfigController@update')->name('management');
          Route::get('checkTaskUpdate/{taskName}/{id}', 'Management\ConfigController@checkTaskUpdate')->name('management');
          Route::get('Checking-update/{id}/{typeDB}', 'Management\ConfigController@Checkupdate')->name('management');
          Route::get('Checking-updateConfirm/{id}/{typeDB}', 'Management\ConfigController@CheckupdateConfirm')->name('management');
          });

        Route::group(['middleware' => 'roled:management/config'], function () {
            Route::get('Checking-delete/{id}/{typeDB}', 'Management\ConfigController@Checkdelete')->name('management');
            Route::get('Checking-deleteConfirm/{id}/{typeDB}', 'Management\ConfigController@CheckdeleteConfirm')->name('management');
            Route::get('/delete/{id}', 'Management\ConfigController@delete')->name('management');
          });

	       Route::group(['middleware' => 'rolecd:management/config'], function () {
                Route::get('checkingDelete/{id}', 'Management\ConfigController@checkingDelete')->name('management');
                
          });
       Route::group(['middleware' => 'rolecu:management/config'], function () {
                Route::get('checkingUpdate/{id}', 'Management\ConfigController@checkingUpdate')->name('management');
                
          });


      });

      Route::group(['prefix' => 'management/mapping/table'], function () {

        Route::group(['middleware' => 'roler:management/mapping/table'], function () {
          Route::get('', 'MappingController@index')->name('mapping');
          Route::get('/getIndex', 'MappingController@getIndex')->name('mapping');
	  Route::get('/saved', 'MappingController@saved')->name('mapping');
          Route::get('/reloadTask', 'MappingController@reloadTask')->name('mapping');
          });

        Route::group(['middleware' => 'rolec:management/mapping/table'], function () {
          Route::get('create', 'MappingController@create')->name('mapping');
          Route::get('checkTask/{taskName}', 'MappingController@checkTask')->name('mapping');
          Route::post('store', 'MappingController@store')->name('mapping');
          });

        Route::group(['middleware' => 'roleu:management/mapping/table'], function () {
          Route::get('Checking-editConfirm/{id}', 'MappingController@checkingEditConfirm')->name('mapping');
          Route::get('edit/{id}', 'MappingController@edit')->name('mapping');
          Route::get('admTaskUpdate', 'MappingController@admTaskUpdate')->name('mapping');
          Route::get('admTableUpdate', 'MappingController@admTableUpdate')->name('mapping');
          Route::get('updateTable/{tipe}/{table}/{tableid}/{taskid}', 'MappingController@updateTable')->name('mapping');
          Route::post('update', 'MappingController@update')->name('mapping');
          Route::get('checkTaskUpdate/{taskName}/{id}', 'MappingController@checkTaskUpdate')->name('mapping');

          });

        Route::group(['middleware' => 'roled:management/mapping/table'], function () {
          Route::get('deleteTable/{tableId}', 'MappingController@deleteTable')->name('mapping');
          Route::get('delete/{taskid}', 'MappingController@deleteTask')->name('mapping');
	  Route::get('checkDelete/{taskid}', 'MappingController@checkDelete')->name('mapping');
          Route::get('Checking-deleteConfirm/{id}', 'MappingController@checkingDeleteConfirm')->name('mapping');
          });

	 Route::group(['middleware' => 'rolecd:management/mapping/table'], function () {
           Route::get('checkingDelete/{taskid}', 'MappingController@checkingDelete')->name('mapping');
          });
         Route::group(['middleware' => 'rolecu:management/mapping/table'], function () {
           Route::get('checkingUpdate/{taskid}', 'MappingController@checkingUpdate')->name('mapping');
          });
      });

Route::group(['prefix' => 'management/job/manual-run'], function () {

  Route::group(['middleware' => 'roleu:management/job/manual-run'], function () {
    Route::get('', 'Management\JobController@indexJob')->name('management');
    Route::get('/create', 'Management\JobController@create')->name('management');
    Route::get('/getIndex', 'Management\JobController@getIndexJob')->name('management');
    Route::get('start-job/{id}/{tanggal}', 'Management\JobController@startJob')->name('monitoring');
    Route::get('cancel-job/{id}', 'Management\JobController@cancelJob')->name('monitoring');
    Route::get('initialData/{id}/{tanggal}', 'Management\JobController@initialData')->name('monitoring');
    Route::get('check-record/{id}/{tanggal}', 'Management\JobController@checkRecord')->name('monitoring');
    Route::get('getOneRecord/{id}', 'Management\JobController@getOneRecord')->name('monitoring');

    });

});

Route::group(['prefix' => 'log/logJob'], function () {
  Route::group(['middleware' => 'roler:log/logJob'], function () {
    Route::get('', 'Log\LogJobController@logJobIndex')->name('log');
    Route::post('/getIndex', 'Log\LogJobController@getIndex')->name('log');
    });
});

Route::group(['prefix' => 'log/logBackup'], function () {
  Route::group(['middleware' => 'roler:log/logBackup'], function () {
    Route::get('', 'Log\LogBackupController@indexLogBackup')->name('log');
    // Route::get('download/{taskid}/{startdate}/{enddate}', 'Log\LogBackupController@download')->name('log');
    Route::get('download/{taskid}/{startdate}/{enddate}', 'Log\LogBackupController@downloadNew')->name('log');
    Route::get('downloadExcel/{filename}', 'Log\LogBackupController@retriveFile')->name('log');
    Route::get('removeExcel/{filename}', 'Log\LogBackupController@removeFile')->name('log');
    });
});

Route::group(['prefix' => 'users/activitiesLog'], function () {
  Route::group(['middleware' => 'roler:users/activitiesLog'], function () {
    Route::get('', 'Users\UserActLogController@index')->name('users');
    Route::post('/getIndex', 'Users\UserActLogController@getIndex')->name('log');
    });
});


// Route::group(['prefix' => 'users/logBackup'], function () {
//   Route::group(['middleware' => 'roler:users/logBackup'], function () {
//     Route::get('', 'Users\UserBckLogController@index')->name('users');
//     Route::get('download/{startdate}/{enddate}', 'Users\UserBckLogController@downloadNew')->name('log');
//     });
// });


Route::group(['prefix' => 'users/logBackup'], function () {

  Route::group(['middleware' => 'roler:users/logBackup'], function () {
    Route::get('', 'Users\UserBckLogController@index')->name('users');
    Route::get('download/{startdate}/{enddate}', 'Users\UserBckLogController@downloadNew')->name('log');
    Route::get('downloadExcel/{filename}', 'Users\UserBckLogController@retriveFile')->name('log');
    Route::get('removeExcel/{filename}', 'Users\UserBckLogController@removeFile')->name('log');
    });

});

Route::group(['prefix' => 'users/management'], function () {

  Route::group(['middleware' => 'roler:users/management'], function () {
    Route::get('', 'Users\UserManagementController@index')->name('users');
    Route::get('/getUser', 'Users\UserManagementController@getUsers')->name('users');
  });

  Route::group(['middleware' => 'rolec:users/management'], function () {
    Route::get('create', 'Users\UserManagementController@create')->name('users');
    Route::post('store', 'Users\UserManagementController@store')->name('users');
  });

  Route::group(['middleware' => 'roleu:users/management'], function () {
    Route::get('edit/{id}', 'Users\UserManagementController@edit')->name('users');
    Route::get('enabled/{id}', 'Users\UserManagementController@enabled')->name('users');
    Route::get('disabled/{id}', 'Users\UserManagementController@disabled')->name('users');
    Route::post('update/{id}', 'Users\UserManagementController@update')->name('users');
  });

  Route::group(['middleware' => 'roled:users/management'], function () {
    Route::get('delete/{id}', 'Users\UserManagementController@delete')->name('users');
  });

  Route::group(['middleware' => 'rolecd:users/management'], function () {
    Route::get('checkingDelete/{id}', 'Users\UserManagementController@checkingDelete')->name('users');
  });

});


Route::group(['prefix' => 'users/group'], function () {
  Route::group(['middleware' => 'roler:users/group'], function () {
    Route::get('', 'Users\UserGroupController@index')->name('group');
    Route::get('/getUsers', 'Users\UserGroupController@getUsers')->name('group');
    Route::get('countUsers/{role_id}', 'Users\UserGroupController@countUsers')->name('users');
  });
  Route::group(['middleware' => 'rolec:users/group'], function () {
    Route::get('/create', 'Users\UserGroupController@create')->name('group');
    Route::post('/store', 'Users\UserGroupController@store')->name('group');
  });
  Route::group(['middleware' => 'roleu:users/group'], function () {
    Route::post('/update/{id}', 'Users\UserGroupController@update')->name('group');
    Route::get('/edit/{id}', 'Users\UserGroupController@edit')->name('group');
  });
  Route::group(['middleware' => 'roled:users/group'], function () {
    Route::get('/delete/{id}', 'Users\UserGroupController@delete')->name('group');
  });
  Route::group(['middleware' => 'rolecd:users/group'], function () {
    Route::get('checkingDelete/{id}', 'Users\UserGroupController@checkingDelete')->name('group');
  });
});

// check Connection
Route::get('management/config/checkConnection/{conIdSource}/{conIdDest}', 'MappingController@checkConnection')->name('mapping');
//API GET and Check FIELD

Route::get('management/mapping/table/checkTable/{taskId}', 'MappingController@checkTable')->name('mapping');
Route::get('management/mapping/table/getTable/{dbconnid}/{type}', 'MappingController@getDatabaseTable')->name('mapping');
Route::get('management/mapping/table/getTableFromEngine/{dbconnid}', 'MappingController@getTableFromEngine')->name('mapping');

Route::get('management/mapping/table/getColumn/{dbconnid}/{tablename}', 'MappingController@getTableDefinition')->name('mapping');
Route::get('management/mapping/table/getTableDefinitionFromEngine/{dbconnid}/{tablename}', 'MappingController@getTableDefinitionFromEngine')->name('mapping');

Route::get('management/mapping/table/getField', 'MappingController@getField')->name('mapping');

Route::get('management/mapping/table/changeFieldSource/{dbconnid}/{tablename}/{field}/{type}', 'MappingController@changeFieldSource')->name('mapping');
Route::get('management/mapping/table/changeFieldFromEngine/{dbconnid}/{tablename}/{field}', 'MappingController@changeFieldFromEngine')->name('mapping');

Route::get('stayAlive', 'GlobalController@sessionSetStayAlive')->name('mapping');
Route::get('setRollback', 'GlobalController@sessionSetRollback')->name('mapping');

//OTHER ROUTE




      // });
  });
