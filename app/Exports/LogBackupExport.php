<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\FromArray;

class LogBackupExport implements FromArray, WithHeadings, WithCustomStartCell, ShouldAutoSize
{
  /**
  * @return \Illuminate\Support\Collection
  */
  public function __construct(array $invoices)
  {
      $this->invoices = $invoices;
  }

  public function array(): array
  {
      return $this->invoices;
  }

  public function headings(): array
  {
      return [
        'Task Name',
        'Task ID',
        'Date',
        'Execution Date',
        'IP Database Source',
        'IP Database Destination',
        'Database Source',
        'Table Source',
        'Database Destination',
        'Table Destination',
        'Process Type',
        'PK Source',
        'PK Destination',
        'Flag Staging',
        'Process Job',
        'Start Time Get Data from Staging',
        'Finish Time Get Data from Staging',
        'Duration (s) Time Get Data from Staging', //13
        'Start Time Get Data from View',
        'Finish Time Get Data from View',
        'Duration (s) Time Get Data from View', //16
        'Start Time Insert Data to DWH',
        'Finish Time Insert Data to DWH',
        'Duration (s) Time Insert Data to DWH  (s)', //19
        'Record Start Sequence Rownum from SD-Smarts',
        'Record Get Data from SD-Smarts',
        'Record Finish Sequence Rownum from SD-Smarts',
        'Record Finish insert data to DWH',
        'Total Duration (s)'
      ];
  }

  public function startCell(): string
  {
      return 'A1';
  }
}
