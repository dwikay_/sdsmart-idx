<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;
use Closure;
use Session;

class CheckingUpdate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$role)
    {
      //if (Auth::check()) {
          $mod = \App\Model\Module::where('menu_path', $role)->first();
          if ($mod != null) {
              $role = \App\Model\Roleacl::where('module_id', $mod->kdModule)->where('role_id', Session::get('role_id'))->first();
              if ($role != null) {
                  if ($role->update_acl == $mod->kdModule) {

                    return response([
                      'info' => "Success",
                      'colors' => "green",
                      'icons' => "fas fa-cirle-check",
                      'alert' => "Done!"
                    ]);
                  }

                  return response([
                    'info' => "Error",
                    'colors' => "red",
                    'icons' => "fas fa-times",
                    'alert' => "Access Denied!"
                  ]);

              } else {
                return response([
                  'info' => "Error",
                  'colors' => "red",
                  'icons' => "fas fa-times",
                  'alert' => "Access Denied!"
                ]);
              }
          } else {
            return response([
              'info' => "Error",
              'colors' => "red",
              'icons' => "fas fa-times",
              'alert' => "Access Denied!"
            ]);
          }
      } 
	//else {
        //Session::flash('info', 'Error');
        //Session::flash('colors', 'red');
        //Session::flash('icons', 'fas fa-times');
        //Session::flash('alert', 'Username / Password Salah!');
        //return redirect(url('/login'));
      //}
    //}
}
