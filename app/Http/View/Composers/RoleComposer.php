<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use DB;
use Auth;
use Session;
class RoleComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $users;

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
	  //dd(Session::get('role_id'));
	  
      $module_parent = DB::table('role_acl')
      ->select('module_parent','module_name','pathParent','menu_icon')
      ->join('modules','role_acl.module_parent','=','modules.id')
      ->where('role_id', Session::get('role_id'))
      ->where(function ($query){
        $query->where('create_acl','<>',0)
              ->orWhere('read_acl','<>',0)
              ->orWhere('update_acl','<>',0)
              ->orWhere('delete_acl','<>',0);
      })
      ->groupBy('module_parent')
      ->groupBy('pathParent')
      ->groupBy('menu_icon')
      ->groupBy('module_name')
      ->orderBy('menu_order','asc')
      ->get();
	  
	  
		
      $module_childs = DB::table('role_acl')
                ->join('modules','role_acl.module_id','=','modules.kdModule')
                ->where('role_id', Session::get('role_id'))
                ->where(function ($query) {
                    $query->where('create_acl','<>',0)
                            ->orWhere('read_acl','<>',0)
                            ->orWhere('update_acl','<>',0)
                            ->orWhere('delete_acl','<>',0);
                })
                ->orderBy('menu_order','asc')
                ->get();

		
      $view->with('module_parent',$module_parent)->with('module_childs',$module_childs);
    }
}
