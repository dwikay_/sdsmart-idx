<?php

namespace App\Http\Controllers\Log;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AdmMapTable;
use App\Model\AdmMapField;
use App\Model\AdmDBCon;
use App\Model\AdmTask;
use App\Model\Log;
use DB;
use App\Model\AdmRowNum;
use App\Model\AdmTimeStamp;
use App\Library\DBConnection;
use App\Library\CurlGenerator;
use Config;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;
use App\Library\activityLog;

class LogJobController extends Controller
{
    public function logJobIndex(activityLog $activityLog){

      $task = DB::table('adm_task')->orderByRaw('LENGTH(TASKNAME)','asc')->orderby('TASKNAME','asc')->get();

      $act = "Log Manager";
      $desc = "Opening Log Manager";
      $activityLog->logUser($act, $desc);

      return view('logManager.logJob')
      ->with('task', $task);
    }

    public function searchLog(CurlGenerator $curlGen, Request $request){

    }
    public function getIndex(CurlGenerator $curlGen, Request $request) {

      if($request->taskId=="all"){
        $log = Log::whereBetween('INSDA', [$request->startDate.' 00:00:00', $request->endDate.' 23:59:59'])->orderby('INSDA','desc')->get();
      }else{
        $log = Log::where('TASKID', $request->taskId)
        ->whereBetween('INSDA', [$request->startDate.' 00:00:00', $request->endDate.' 23:59:59'])->orderby('INSDA','desc')->get();
      }


       //return $log;
      return Datatables::of($log)->escapeColumns([])->make(true);
    }
}
