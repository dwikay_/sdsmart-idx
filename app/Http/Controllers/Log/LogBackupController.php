<?php

namespace App\Http\Controllers\Log;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AdmMapTable;
use App\Model\AdmMapField;
use App\Exports\LogBackupExport;
use App\Model\AdmDBCon;
use App\Model\AdmTask;
use App\Model\Log;
use App\Model\AdmRowNum;
use App\Model\AdmTimeStamp;
use App\Library\DBConnection;
use App\Library\CurlGenerator;
use Config;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use DB;
use App\Library\activityLog;

class LogBackupController extends Controller
{
  public function indexLogBackup(activityLog $activityLog){

    $task = DB::table('adm_task')->orderByRaw('LENGTH(TASKNAME)','asc')->orderby('TASKNAME','asc')->get();
    //return $task;

    $act = "Backup Log Manager";
    $desc = "Opening Backup Log Manager";
    $activityLog->logUser($act, $desc);

    return view('logManager.backupLog')
    ->with('task', $task);
  }

  public function downloadNew(Request $request, activityLog $activityLog, $taskid, $startdate, $enddate){

    ini_set('max_execution_time',7200);
    $push = [];
    if($request->taskid=="all"){
        $log = Log::whereBetween('INSDA', [$startdate.' 00:00:00', $enddate.' 23:59:59'])->orderby('INSDA','desc')->get();
      }else{
        $log = Log::where('TASKID', $request->taskid)
        ->whereBetween('INSDA', [$startdate.' 00:00:00', $enddate.' 23:59:59'])->orderby('INSDA','desc')->get();
      }

    $task = AdmTask::where('TASKID', $taskid)->first();
    if($task==null){
      $taskName = "All Task";
    }else{
      $taskName = $task->TASKNAME;
    }
    $act = "Backup Log Manager";
    $desc = "Download Backup Log Manager on date ".$startdate." until ".$enddate." with TASKNAME ".$taskName." ID : ".$taskid;
    $activityLog->logUser($act, $desc);

     $export = new LogBackupExport([
       $push,
     ]);

     $i = 1;
     foreach ($log as $key => $item) {


       if($item->ISSTAGING=="1" || $item->ISSTAGING==1){
         $staging = "Staging";
         $squery = "Insert";
       }else{
         $staging = "Non Staging";
         $squery = "Insert & Update";
       }

       if($item->ISMANUALJOB=="1" || $item->ISMANUALJOB==1){
         $ismanualjob = "Manual Job";
       }else if($item->ISMANUALJOB=="2" || $item->ISMANUALJOB==2){
         $ismanualjob = "Initial Data";
       }else{
         $ismanualjob = "Normal Job";
       }
      if($item->STARTROWNUM==""){$STARTROWNUM = "0";}else{$STARTROWNUM = $item->STARTROWNUM;}
      if($item->RECORDGETDATA==""){$RECORDGETDATA = "0";}else{$RECORDGETDATA = $item->RECORDGETDATA;}
      if($item->ENDROWNUM==""){$ENDROWNUM = "0";}else{$ENDROWNUM = $item->ENDROWNUM;}
      if($item->RECORDINSERTDATA==""){$RECORDINSERTDATA = "0";}else{$RECORDINSERTDATA = $item->RECORDINSERTDATA;}

       array_push($push, array(
          $i++,
          $item->TASKNAME,
          $item->TASKID,
          $item->DATEPROC,
          $item->DATELOG,
          $item->DBSOURCEHOST,
          $item->DBDESTHOST,
          $item->DBSOURCENAME,
          $item->TBLSOURCE,
          $item->DBDESTNAME,
          $item->TBLDEST,
          $squery,
          $item->PKSOURCE,
          $item->PKDESTINATION,
          $staging,
          $ismanualjob,
          $item->STARTTIMEREFRESH,
          $item->ENDTIMEREFRESH,
          $item->DURATIONTIMEREFRESH,
          $item->STARTTIMEGETDATA,
          $item->FINISHTIMEGETDATA,
          $item->DURATIONTIMEGETDATA,
          $item->STARTTIMEINSERT,
          $item->ENDTIMEINSERT,
          $item->DURATIONTIMEINSERT,
          $STARTROWNUM, //1
          $RECORDGETDATA, //2
          $ENDROWNUM, //3
          $RECORDINSERTDATA, //4
          $item->TOTALDURATION
       ));


     }

     $export = new LogBackupExport([
       $push,
     ]);
     // dd($export);
    Excel::store($export, "file/excelLogManager/".$taskName." Log Jobs-SDSMARTS ".$taskid." ".$startdate." sd ".$enddate.".csv");

    return json_encode($taskName." Log Jobs-SDSMARTS ".$taskid." ".$startdate." sd ".$enddate.".csv");

  }
  public function download(Request $request, activityLog $activityLog, $taskid, $startdate, $enddate){

	 ini_set('max_execution_time',7200);
    if($request->taskid=="all"){
        $log = Log::whereBetween('INSDA', [$startdate.' 00:00:00', $enddate.' 23:59:59'])->orderby('INSDA','desc')->get();
      }else{
        $log = Log::where('TASKID', $request->taskid)
        ->whereBetween('INSDA', [$startdate.' 00:00:00', $enddate.' 23:59:59'])->orderby('INSDA','desc')->get();
      }

    $task = AdmTask::where('TASKID', $taskid)->first();
    if($task==null){
      $taskName = "All Task";
    }else{
      $taskName = $task->TASKNAME;
    }
    $act = "Backup Log Manager";
    $desc = "Download Backup Log Manager on date ".$startdate." until ".$enddate." with TASKNAME ".$taskName." ID : ".$taskid;
    $activityLog->logUser($act, $desc);

    Excel::create($taskName." Log Jobs-SDSMARTS ".$taskid." ".$startdate." sd ".$enddate, function ($result) use ($log) {
  				$result->sheet('SheetName', function ($sheet) use ($log) {
            $i = 1;
  						foreach ($log as $key => $item) {

                if($item->ISSTAGING=="1" || $item->ISSTAGING==1){
                  $staging = "Staging";
		              $squery = "Insert";
                }else{
                  $staging = "Non Staging";
		              $squery = "Insert & Update";
                }

                if($item->ISMANUALJOB=="1" || $item->ISMANUALJOB==1){
                  $ismanualjob = "Manual Job";
                }else if($item->ISMANUALJOB=="2" || $item->ISMANUALJOB==2){
                  $ismanualjob = "Initial Data";
                }else{
                  $ismanualjob = "Normal Job";
                }
		if($item->STARTROWNUM==""){$STARTROWNUM = "0";}else{$STARTROWNUM = $item->STARTROWNUM;}
		if($item->RECORDGETDATA==""){$RECORDGETDATA = "0";}else{$RECORDGETDATA = $item->RECORDGETDATA;}
		if($item->ENDROWNUM==""){$ENDROWNUM = "0";}else{$ENDROWNUM = $item->ENDROWNUM;}
		if($item->RECORDINSERTDATA==""){$RECORDINSERTDATA = "0";}else{$RECORDINSERTDATA = $item->RECORDINSERTDATA;}

  		$data=[];
  		array_push($data, array(
            		$item->TASKNAME,
          		  $item->TASKID,
            		$item->DATEPROC,
          		  $item->DATELOG,
            		$item->DBSOURCEHOST,
            		$item->DBDESTHOST,
            		$item->DBSOURCENAME,
            		$item->TBLSOURCE,
            		$item->DBDESTNAME,
            		$item->TBLDEST,
            		$squery,
                $item->PKSOURCE,
                $item->PKDESTINATION,
                $staging,
                $ismanualjob,
                $item->STARTTIMEREFRESH,
                $item->ENDTIMEREFRESH,
                $item->DURATIONTIMEREFRESH,
                $item->STARTTIMEGETDATA,
                $item->FINISHTIMEGETDATA,
                $item->DURATIONTIMEGETDATA,
                $item->STARTTIMEINSERT,
                $item->ENDTIMEINSERT,
                $item->DURATIONTIMEINSERT,
                $STARTROWNUM, //1
                $RECORDGETDATA, //2
                $ENDROWNUM, //3
                $RECORDINSERTDATA, //4
		            $item->TOTALDURATION
  		));
  		$sheet->fromArray($data, null, 'A2', false, false);
  	}
  		$sheet->row(1, array(
                'Task Name',
                'Task ID',
                'Date',
		            'Execution Date',
                'IP Database Source',
                'IP Database Destination',
                'Database Source',
                'Table Source',
                'Database Destination',
                'Table Destination',
                'Process Type',
                'PK Source',
                'PK Destination',
                'Flag Staging',
                'Process Job',
                'Start Time Get Data from Staging',
                'Finish Time Get Data from Staging',
                'Duration (s) Time Get Data from Staging', //13
                'Start Time Get Data from View',
                'Finish Time Get Data from View',
                'Duration (s) Time Get Data from View', //16
                'Start Time Insert Data to DWH',
                'Finish Time Insert Data to DWH',
                'Duration (s) Time Insert Data to DWH  (s)', //19
                'Record Start Sequence Rownum from SD-Smarts',
                'Record Get Data from SD-Smarts',
                'Record Finish Sequence Rownum from SD-Smarts',
                'Record Finish insert data to DWH',
		            'Total Duration (s)'
              ));

  						$sheet->setBorder('A1:AC1', 'thin');
  						$sheet->cells('A1:AB1', function ($cells) {
  								$cells->setBackground('#0070c0');
  								$cells->setFontColor('#ffffff');
  								$cells->setValignment('center');
  								$cells->setFontSize('11');
  						});
  						$sheet->setHeight(array(
  					'1' => '20'
  			));
  						$sheet->setWidth('A', '10');
  						$sheet->setWidth('B', '25');
  						$sheet->setWidth('C', '25');
  						$sheet->setWidth('D', '25');
  						$sheet->setWidth('E', '25');
  						$sheet->setWidth('F', '25');
  						$sheet->setWidth('G', '100');
  						$sheet->setWidth('H', '25');
  						$sheet->setWidth('I', '25');
  						$sheet->setWidth('J', '20');
  						$sheet->setWidth('K', '20');
  						$sheet->setWidth('L', '20');
  						$sheet->setWidth('M', '20');
  						$sheet->setWidth('N', '20');
  						$sheet->setWidth('O', '20');
  						$sheet->setWidth('P', '20');
  						$sheet->setWidth('Q', '20');
  						$sheet->setWidth('R', '20');
  						$sheet->setWidth('S', '20');
  						$sheet->setWidth('T', '20');
  						$sheet->setWidth('U', '20');
  						$sheet->setWidth('V', '20');
  						$sheet->setWidth('W', '20');
  						$sheet->setWidth('X', '20');
  						$sheet->setWidth('Z', '20');
  						$sheet->setWidth('AA', '20');
  						$sheet->setWidth('AB', '20');
  						$sheet->setWidth('AC', '20');
  				});
  				ob_end_clean();
  		})->store('csv', public_path('file/excelLogManager/'));

      return json_encode($taskName." Log Jobs-SDSMARTS ".$taskid." ".$startdate." sd ".$enddate.".csv");

  }

  function retriveFile($filename){
    $responses = response()->download(public_path("file/excelLogManager/".$filename));
    return $responses;
  }
  function removeFile($filename){
    unlink(public_path("file/excelLogManager/".$filename));
    return json_encode(true);
  }

}
