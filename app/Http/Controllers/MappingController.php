<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\AdmMapTable;
use App\Model\AdmMapField;
use App\Model\AdmDBCon;
use App\Model\AdmTask;
use App\Model\AdmRowNum;
use App\Model\RecordLog;
use App\Model\AdmTimeStamp;
use App\Library\DBConnection;
use App\Library\CurlGenerator;
use Config;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;
use Session;
use DB;
use App\Library\activityLog;

class MappingController extends Controller
{
  public function checkConnection(CurlGenerator $curlGen, $conIdSource, $conIdDest){

    $cek_koneksiSource = AdmDBCon::where('DBCONNID', $conIdSource)->first();
    $urlsSource = "/datasnap/rest/TRESTMethods/TestConnection"."/".$cek_koneksiSource->DBPROVIDER.";".$cek_koneksiSource->DBHOSTNAME.";".$cek_koneksiSource->DBPORT.";".$cek_koneksiSource->DBNAME.";".$cek_koneksiSource->DBUSERID.";".base64_decode($cek_koneksiSource->DBPASSWORD).";";
    $resultParamSource = $curlGen->testConnection($urlsSource);

    $cek_koneksiDest = AdmDBCon::where('DBCONNID', $conIdDest)->first();
    $urlsDest = "/datasnap/rest/TRESTMethods/TestConnection"."/".$cek_koneksiDest->DBPROVIDER.";".$cek_koneksiDest->DBHOSTNAME.";".$cek_koneksiDest->DBPORT.";".$cek_koneksiDest->DBNAME.";".$cek_koneksiDest->DBUSERID.";".base64_decode($cek_koneksiDest->DBPASSWORD).";";
    $resultParamDest = $curlGen->testConnection($urlsDest);

    // return $cek_koneksiSource->DBPASSWORD;
    // return base64_decode("'.$cek_koneksiSource->DBPASSWORD.'");
    // return $resultParamDest;
    if($resultParamSource=="Database is connected successfully" && $resultParamDest=="Database is connected successfully"){
      $result = "success";
    }else if($resultParamSource!="Database is connected successfully"){
      $result = "destSuccess";
    }else if($resultParamDest!="Database is connected successfully"){
      $result = "sourceSuccess";
    }
    else{
      $result = "failAll";
    }
    $data[] = array(
      'result' => $result
    );

    return json_encode($data);
  }
  public function changeFieldSource($dbconnid,$tablename, $columns,$type, DBConnection $DBConnection){

    $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->first();
    $column1 = explode('-', $columns);
    // return $column1[0];
    $column = $column1;
    // return $tbl;
    $connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);

    $columns = array();

    // $TABLE_SCHEMA = explode('.', $tablename)[0];
    // $TABLE_NAME = explode('.', $tablename)[1];
    $TABLE_NAME = explode('.', $tablename);

    if($type=="target"){
      if($AdmDBCon->DBPROVIDER == 'SQL Server'){
        $result = $connection->table('INFORMATION_SCHEMA.COLUMNS')->select('COLUMN_NAME','DATA_TYPE')->where('TABLE_NAME',$TABLE_NAME)->where('COLUMN_NAME',$column)->where('TABLE_SCHEMA','dbo')->orderBy('COLUMN_NAME','asc')->get();
        $i = 1;
        foreach($result as $column){
          $columns[] = array(
            'id' => $i++,
            'COLUMN_NAME'=>$column->COLUMN_NAME,
            'DATA_TYPE'=>$column->DATA_TYPE,
          );
        }
      }
    }

    else{
      if($AdmDBCon->DBPROVIDER == 'PostgreSQL'){
        $result = $connection->table('information_schema.columns')->select('column_name','udt_name')->where('table_name',$TABLE_NAME)->where('column_name',$column)->where('table_schema','public')->get();
        // return $result;
        $i = 1;
        foreach($result as $column){
          $columns[] = array(
            'id' => $i++,
            'COLUMN_NAME'=>$column->column_name,
            'DATA_TYPE'=>$column->udt_name,
          );
        }
      }
    }


    $DBConnection->disconnect($AdmDBCon->DBPROVIDER);
    // return count($columns);
    echo json_encode($columns[0]);
  }

   public function saved(activityLog $activityLog){

      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Task Configuration is succesfully save!');
      return redirect(url('management/mapping/table'));
    }

    public function index(activityLog $activityLog){

      $act = "Task Configuration";
      $desc = "Opening Task Configuration";
      $activityLog->logUser($act, $desc);

      	return view('management.mapping.index');
    }

    public function checkTaskUpdate($taskName, $id){

      $cek_task_name = AdmTask::where('TASKID', $id)->first();

      if ($taskName != $cek_task_name->TASKNAME){

        $cek_task = AdmTask::where(DB::raw('BINARY `TASKNAME`'), $taskName)->first();

        if($cek_task==null){
          $status = "kosong";
        }else{
          $status = "ada";
        }
      }else{
        $status = "kosong";
      }

      $data[] = array(
        "status" => $status
      );

      return json_encode($data);

    }
    public function checkTask($taskName){

      //$cek_task = AdmTask::where('TASKNAME', $taskName)->first();
      $cek_task = AdmTask::where(DB::raw('BINARY `TASKNAME`'), $taskName)->first();

      if($cek_task==null){
        $status = "kosong";
      }else{
        $status = "ada";
      }
      $data[] = array(
        "status" => $status
      );

      return json_encode($data);

    }
    public function checkingEditConfirm(CurlGenerator $curlGen, $taskId)
    {
      $urlGetField = "/datasnap/rest/TRESTMethods/GetDataTask/".$taskId;
      $paramGetField = $curlGen->getField($urlGetField);
      $expd2 = explode(',', $paramGetField);
     
    if(substr($paramGetField, -9) != "not found"){
     if($expd2[10] == "Stop"){
        $result = "allow";
     } else{
        $result = "not_allow";
     }
    }else {
        $result = "delete";
     }

     $data[] = array(
        "result" => $result
      );

      return json_encode($data);
    }
    public function checkingDeleteConfirm(CurlGenerator $curlGen, $taskId)
    {
      $urlGetField = "/datasnap/rest/TRESTMethods/GetDataTask/".$taskId;
      $paramGetField = $curlGen->getField($urlGetField);
      $expd2 = explode(',', $paramGetField);
      //return substr($paramGetField, -9);
      if(substr($paramGetField, -9) != "not found"){
        if($expd2[10] == "Stop"){
        $result = "allow";
     } else{
        $result = "not_allow";
     }
     
     $data[] = array(
        "result" => $result
      );

      return json_encode($data);
      }else{
        $data[] = array(
        "result" => 'delete'
      );

      return json_encode($data);
      }
     
    }
    public function checkDelete(CurlGenerator $curlGen, $taskid){
      
      $urlGetField = "/datasnap/rest/TRESTMethods/GetDataTask/".$taskid;
      $paramGetField = $curlGen->getField($urlGetField);
      $expd2 = explode(',', $paramGetField);
      
     if(substr($paramGetField, -9) != "not found"){
      if($expd2[10] == "Stop"){
      
      $url = "/datasnap/rest/TRESTMethods/Deletetask/".$taskid;
      $param = $curlGen->getIndex($url);
      $expd = explode(',', $param);

      if($expd[0]==00 || $expd[0]=="00"){
        AdmTask::where('TASKID', $taskid)->delete();
        $code = $expd[0];
        $result = $expd[1];
      }else{$result = $expd[1];$code = $expd[0];}

     if($expd[1]=="Task already deleted"){

        $result = "Deleted";
        $code = "33";
     }
    }else{
      $result = "not_allow";
      $code = "401";
    }
   } else{
      $result = "Deleted";
      $code = "33";
    }
      $data[] = array(
        "code" => $code,
        "result" => $result
      );
    
      return json_encode($data);
    }
    public function deleteTask(activityLog $activityLog, $taskid){
       
        $cek_task = AdmTask::where('TASKID',$taskid)->first();
	$desc = "Delete Task Configuration with Task ID ".$taskid;
        AdmTask::where('TASKID', $taskid)->delete();
        AdmRowNum::where('TASKID', $taskid)->delete();
        AdmTimeStamp::where('TASKID', $taskid)->delete();
        $cek_table = AdmMapTable::where('TASKID', $taskid)->first();
        AdmMapField::where('MAPTBLID', $cek_table->MAPTBLID)->delete();
        AdmMapTable::where('TASKID', $taskid)->delete();
        RecordLog::where('TASKID', $taskid)->delete();

        $act = "Task Configuration";
        $activityLog->logUser($act, $desc);

        return redirect(url('management/mapping/table'));
	
        

    }

    public function checkingDelete(activityLog $activityLog, $taskid){
	 
        $cek_task = AdmTask::where('TASKID', $taskid)->first();
        $act = "Task Configuration";
        $desc = "Delete Task Configuration with Task ID ".$taskid." TASKNAME :".$cek_task->TASKNAME;
        AdmTask::where('TASKID', $taskid)->delete();
        AdmRowNum::where('TASKID', $taskid)->delete();
        AdmTimeStamp::where('TASKID', $taskid)->delete();
        $cek_table = AdmMapTable::where('TASKID', $taskid)->first();
        AdmMapField::where('MAPTBLID', $cek_table->MAPTBLID)->delete();
        AdmMapTable::where('TASKID', $taskid)->delete();
        RecordLog::where('TASKID', $taskid)->delete();       
        $activityLog->logUser($act, $desc);

        return redirect(url('management/mapping/table'));

    }
    public function checkingUpdate(activityLog $activityLog, $taskid){

        return redirect(url('management/mapping/table'));

    }
      public function getIndex(){

      $task = AdmTask::with('dbSource','dbTarget','mapTable')->orderby('TASKNAME','asc')->get();

      $data = array(
        "listMapping" => array()
      );

      foreach ($task as $key => $values) {
        // return $values->dbSource->DBSERVERNAME;
        if($values->dbSource==null){
          $dbsourceName = "####";
          $tblsource = "####";
        }else{
          $dbsourceName = $values->dbsource->DBSERVERNAME;
          $tblsource = $values->mapTable->TBLSOURCE;
        }

        if($values->dbtarget==null){
          $dbtargetName = "####";
          $tbltarget = "####";
        }else{
          $dbtargetName = $values->dbtarget->DBSERVERNAME;
          $tbltarget = $values->mapTable->TBLDEST;
        }

        if($values->ISINSERT=="1"){
          $proses_type = "Insert";
        }
        else{
          $proses_type = "Insert & Update";
        }

        $timestamp = strtotime($values->INSDA);
        $date = date('d-M-y', $timestamp);

                $data["listMapping"][$key] = array(
                    "TASKID" => $values->TASKID,
                    "TASKNAME" => $values->TASKNAME,
                    "PROSES_TYPE" => $proses_type,
                    "TASKINTERVAL" => $values->TASKINTERVAL,
                    "DBSOURCEID" => $dbsourceName.' - '.$values->dbSource->DBNAME,
                    "TBLSOURCE" => $tblsource,
                    "DBDESTID" => $dbtargetName.' - '.$values->dbTarget->DBNAME,
                    "TBLDEST" => $tbltarget,
                    "DATE" => $date,
                    "CREATED_BY" => $values->INSUI
                  );
              }

      // $data['listMapping'] = array_values($data['listMapping']);

      // return $data['listMapping'];
      return Datatables::of($data['listMapping'])->escapeColumns([])->make(true);
    }

    public function reloadTask(activityLog $activityLog,CurlGenerator $curlGen){
      
      $urlGetTask = "/datasnap/rest/TRESTMethods/GetDataTasks";
      $paramGetTask = $curlGen->getField($urlGetTask);
      $expdTask = explode(';', $paramGetTask);
      foreach($expdTask as $expdTasks){
        
        $expd2 = explode(',', $expdTasks);
        if($expd2[10] != "Stop"){
         $notif = "All TASK must be Stop!";
         return json_encode($notif);
        }
      }
      $url = "/datasnap/rest/TRESTMethods/Reloadtasks";
      $param = $curlGen->getIndex($url);
      //return $param;
      $act = "Task Configuration";
      $desc = "Reload All Task Configuration";
      $activityLog->logUser($act, $desc);

      return json_encode($param);
    }

    public function getField(CurlGenerator $curlGen) {

      $url = "/datasnap/rest/TRESTMethods/GetFieldsList/2;idxindexstats;3;idxindexstats";
      $param = $curlGen->getField($url);

      // return $param;

      $expd1 = explode('/', $param);
      foreach($expd1 as $key => $value){
        $expd2 = explode(';', $value);
          foreach($expd2 as $keys => $values){
            $expd3 = explode(',', $values);
            $data[] = array(
              'field' => $expd3[0],
              'tipeData' => $expd3[1],
              'lenghtChar' => $expd3[2],
              'numericPres' => $expd3[3]
            );
        }
      }
      return $data;
    }
    public function store(Request $request,activityLog $activityLog){

      date_default_timezone_set('asia/jakarta');

      $task = new AdmTask();

      if($request->prosesTypePost=="1"){
        $isinsert = 1;
          if($request->dataTypePost=="0"){
            $isrownum = 0;
          }else{
            $isrownum = 1;
          }
      }else{
        $isinsert = 0;
        $isrownum = 0;
      }

      // return $request->all();
      $task->TASKNAME = $request->taskNamePost;
      $task->TASKINTERVAL = $request->taskIntervalPost;
      $task->ISINSERT = $isinsert;
      $task->ISROWNUM = $isrownum;
      $task->DBSOURCEID = $request->selectSourcePost;
      $task->DBDESTID = $request->selectTargetPost;
      $task->INSUI = Session::get('users')->email;
      $task->INSDA = date('Y-m-d H:i:s');
      $task->save();

      if($isinsert==1){$resInsert = "Y";}else{$resInsert = "N";}
      if($isrownum==1){$resRow = "Y";}else{$resRow = "N";}
      $cekConDBSOURCE = AdmDBCon::where('DBCONNID', $request->selectSourcePost)->first();
      $cekConDBDEST = AdmDBCon::where('DBCONNID', $request->selectTargetPost)->first();

      $dataTask[] = array(
        'TASKNAME' => $request->taskNamePost,
        'TASKINTERVAL' => $request->taskIntervalPost,
        'ISINSERT' => $resInsert,
        'ISROWNUM' => $resRow,
        'DB_SOURCE' => $cekConDBSOURCE->DBNAME,
        'DB_DEST' => $cekConDBDEST->DBNAME,
        'userInsert' => Session::get('users')->email,
        'insertDate' => date('Y-m-d H:i:s')
      );

      $act = "Task Configuration";
      $desc = "Save data TASK with value ".json_encode($dataTask);
      $activityLog->logUser($act, $desc);

      $cek_task = AdmTask::orderby('TASKID','desc')->first();

      $expdTblSource = explode('.',$request->sourceTablePost);
      $expdTblTarget = explode('.',$request->targetTablePost);
      $expdTblStagging = explode('.',$request->staggingTablePost);

      $table = new AdmMapTable();
      $table->TASKID = $cek_task->TASKID;
      $table->TBLSOURCE = $expdTblSource[0];
      $table->TBLDEST = $expdTblTarget[0];
      $table->TBLTEMP = $expdTblStagging[0];
      $table->SCRIPTQUERY = $request->queryTextPost;
      $table->INSUI = Session::get('users')->email;
      $table->INSDA = date('Y-m-d H:i:s');
      $table->save();

      $dataAdmTable[] = array(
        'TASKNAME' => $cek_task->TASKNAME,
        'TABEL_SOURCE' => $expdTblSource[0],
        'TABLE_DEST' => $expdTblTarget[0],
        'TABLE_TEMP' => $expdTblStagging[0],
        'SCRIPT_QUERY' => $request->queryTextPost,
        'userInsert' => Session::get('users')->email,
        'insertDate' => date('Y-m-d H:i:s')
      );

      $act = "Task Configuration";
      $desc = "Save data TASK Table with value ".json_encode($dataAdmTable);
      $activityLog->logUser($act, $desc);

      $cek_table = AdmMapTable::orderby('MAPTBLID','desc')->first();

      $dataField[] = array(
        "fieldList" => [],
        "TableSource" => $expdTblSource[0],
        "TableDest" => $expdTblTarget[0],
        'userInsert' => Session::get('users')->email,
        'insertDate' => date('Y-m-d H:i:s')
      );

      $sourceFieldPost = json_decode($request->sourceFieldPost);
      $sourceTypePost  = json_decode($request->sourceTypePost);
      $targetFieldPost = json_decode($request->targetFieldPost);
      $targetTypesPost = json_decode($request->targetTypesPost);
      $inputSourcePost = json_decode($request->inputSourcePost);
      $inputTargetPost = json_decode($request->inputTargetPost);

      foreach ($sourceFieldPost as $key => $value) {

        $explodeSource = explode('-', $sourceFieldPost[$key]);
        $explodeTarget = explode('-', $targetFieldPost[$key]);

        $field = new AdmMapField();
        $field->MAPTBLID = $cek_table->MAPTBLID;
        $field->FIELDSOURCE = $explodeSource[1];
        $field->FIELDDEST = $explodeTarget[1];
        $field->FIELDSOURCETYPE = $sourceTypePost[$key];
        $field->FIELDDESTTYPE = $targetTypesPost[$key];
        $field->ISPRIMARYKEYSOURCE = $inputSourcePost[$key];
        $field->ISPRIMARYKEYDEST = $inputTargetPost[$key];
        $field->INSUI = Session::get('users')->email;
        $field->INSDA = date('Y-m-d H:i:s');
        $field->save();

        $dataField["fieldList"][$key] = array(
            "fieldSource"=> $explodeSource[1],
            "fieldDest"=> $explodeTarget[1],
            "typeFieldSource"=> $sourceTypePost[$key],
            "typeFieldDest"=> $targetTypesPost[$key],
            "primaryKeySource"=> $inputSourcePost[$key],
            "primaryKeyDest"=> $inputTargetPost[$key]
          );
      }

      $act = "Task Configuration";
      $desc = "Save data TASK Field with value ".json_encode($dataField);
      $activityLog->logUser($act, $desc);

      if($request->prosesTypePost=="1"){

          if($request->dataTypePost=="0"){
            $timestamp = new AdmTimeStamp();
            $timestamp->TIMESTART = $request->timestampValPost;
            $timestamp->TASKID = $cek_task->TASKID;
            $timestamp->INSUI = Session::get('users')->email;
            $timestamp->INSDA = date('Y-m-d H:i:s');
            $timestamp->save();

            $dataTimestamp[] = array(
              'TASKNAME' => $cek_task->TASKNAME,
              'TIMESTART' => $request->timestampValPost,
              'userInsert' => Session::get('users')->email,
              'insertDate' => date('Y-m-d H:i:s')
            );

            $act = "Task Configuration";
            $desc = "Save data TASK Timetamps with value ".json_encode($dataTimestamp);
            $activityLog->logUser($act, $desc);

          }else{
            $rownum = new AdmRowNum();
            $rownum->ROWNUM = $request->rownumValPost;
            $rownum->ROWNUMDATE = date('Y-m-d');
            $rownum->TASKID = $cek_task->TASKID;
            $rownum->INSUI = Session::get('users')->email;
            $rownum->INSDA = date('Y-m-d H:i:s');
            $rownum->save();

            $dataRownum[] = array(
              'TASKNAME' => $cek_task->TASKNAME,
              'ROWNUM' => $request->rownumValPost,
              'ROWNUMDATE' => date('Y-m-d'),
              'userInsert' => Session::get('users')->email,
              'insertDate' => date('Y-m-d H:i:s')
            );

            $act = "Task Configuration";
            $desc = "Save data TASK Rownum with value ".json_encode($dataRownum);
            $activityLog->logUser($act, $desc);
          }

      } else {
        // dd("sini");
        // $rownum = new AdmRowNum();
        // $rownum->ROWNUM = 0;
        // $rownum->ROWNUMDATE = date('Y-m-d');
        // $rownum->TASKID = $cek_task->TASKID;
        // $rownum->INSUI = Auth::user()->name;
        // $rownum->INSDA = date('Y-m-d H:i:s');
        // $rownum->save();


      }

      $data[] = array(
        'result' => 'success'
      );

      return json_encode($data);
    }

    public function update(Request $request,activityLog $activityLog){

      date_default_timezone_set('asia/jakarta');

      $task = AdmTask::where('TASKID', $request->idTaskPost)->first();

      if($request->prosesTypePost=="1"){
        $isinsert = 1;
          if($request->dataTypePost=="0"){
            $isrownum = 0;
          }else{
            $isrownum = 1;
          }
      }else{
        $isinsert = 0;
        $isrownum = 0;
      }
      // return json_encode($task);
      $task->TASKNAME = $request->taskNamePost;
      $task->TASKINTERVAL = $request->taskIntervalPost;
      $task->ISINSERT = $isinsert;
      $task->ISROWNUM = $isrownum;
      $task->DBSOURCEID = $request->selectSourcePost;
      $task->DBDESTID = $request->selectTargetPost;
      $task->INSUI = Session::get('users')->email;
      $task->INSDA = date('Y-m-d H:i:s');
      $task->save();

      if($isinsert==1){$resInsert = "Y";}else{$resInsert = "N";}
      if($isrownum==1){$resRow = "Y";}else{$resRow = "N";}
      $cekConDBSOURCE = AdmDBCon::where('DBCONNID', $request->selectSourcePost)->first();
      $cekConDBDEST = AdmDBCon::where('DBCONNID', $request->selectTargetPost)->first();

      $dataTask[] = array(
        'TASKNAME' => $request->taskNamePost,
        'TASKINTERVAL' => $request->taskIntervalPost,
        'ISINSERT' => $resInsert,
        'ISROWNUM' => $resRow,
        'DB_SOURCE' => $cekConDBSOURCE->DBNAME,
        'DB_DEST' => $cekConDBDEST->DBNAME,
        'userInsert' => Session::get('users')->email,
        'insertDate' => date('Y-m-d H:i:s')
      );

      $act = "Task Configuration";
      $desc = "Update data TASK with value ".json_encode($dataTask);
      $activityLog->logUser($act, $desc);

      $cek_task = AdmTask::where('TASKID', $request->idTaskPost)->first();

      $expdTblSource = explode('.',$request->sourceTablePost);
      $expdTblTarget = explode('.',$request->targetTablePost);
      $expdTblStagging = explode('.',$request->staggingTablePost);

      $table = AdmMapTable::where('MAPTBLID', $request->idTablePost)->first();
      $table->TASKID = $cek_task->TASKID;
      $table->TBLSOURCE = $expdTblSource[0];
      $table->TBLDEST = $expdTblTarget[0];
      $table->TBLTEMP = $expdTblStagging[0];
      $table->SCRIPTQUERY = $request->queryTextPost;
      $table->INSUI = Session::get('users')->email;
      $table->INSDA = date('Y-m-d H:i:s');
      $table->save();

      $dataAdmTable[] = array(
        'TASKNAME' => $cek_task->TASKNAME,
        'TABEL_SOURCE' => $expdTblSource[0],
        'TABLE_DEST' => $expdTblTarget[0],
        'TABLE_TEMP' => $expdTblStagging[0],
        'SCRIPT_QUERY' => $request->queryTextPost,
        'userInsert' => Session::get('users')->email,
        'insertDate' => date('Y-m-d H:i:s')
      );

      $act = "Task Configuration";
      $desc = "Update data TASK Table with value ".json_encode($dataAdmTable);
      $activityLog->logUser($act, $desc);

      $cek_table = AdmMapTable::where('MAPTBLID', $request->idTablePost)->first();

      $sourceFieldPost = json_decode($request->sourceFieldPost);
      $sourceTypePost= json_decode($request->sourceTypePost);
      $targetFieldPost = json_decode($request->targetFieldPost);
      $targetTypesPost = json_decode($request->targetTypesPost);
      $inputSourcePost= json_decode($request->inputSourcePost);
      $inputTargetPost= json_decode($request->inputTargetPost);

      $dataField[] = array(
        "fieldList" => [],
        "TableSource" => $expdTblSource[0],
        "TableDest" => $expdTblTarget[0],
        'userInsert' => Session::get('users')->email,
        'insertDate' => date('Y-m-d H:i:s')
      );

      $removeField = AdmMapField::where('MAPTBLID', $request->idTablePost)->delete();
      foreach ($sourceFieldPost as $key => $value) {

        $explodeSource = explode('-', $sourceFieldPost[$key]);
        $explodeTarget = explode('-', $targetFieldPost[$key]);

        $field = new AdmMapField();
        $field->MAPTBLID = $cek_table->MAPTBLID;
        $field->FIELDSOURCE = $explodeSource[1];
        $field->FIELDDEST = $explodeTarget[1];
        $field->FIELDSOURCETYPE = $sourceTypePost[$key];
        $field->FIELDDESTTYPE = $targetTypesPost[$key];
        $field->ISPRIMARYKEYSOURCE = $inputSourcePost[$key];
        $field->ISPRIMARYKEYDEST = $inputTargetPost[$key];
        $field->INSUI = Session::get('users')->email;
        $field->INSDA = date('Y-m-d H:i:s');
        $field->save();

        $dataField["fieldList"][$key] = array(
            "fieldSource"=> $explodeSource[1],
            "fieldDest"=> $explodeTarget[1],
            "typeFieldSource"=> $sourceTypePost[$key],
            "typeFieldDest"=> $targetTypesPost[$key],
            "primaryKeySource"=> $inputSourcePost[$key],
            "primaryKeyDest"=> $inputTargetPost[$key]
          );

      }

      $act = "Task Configuration";
      $desc = "Update data TASK Field with value ".json_encode($dataField);
      $activityLog->logUser($act, $desc);

      if($request->prosesTypePost=="1" || $request->prosesTypePost==1){

        if($request->dataTypePost=="0"){
          AdmRowNum::where('TASKID', $request->idTaskPost)->delete();

          $timestampz = AdmTimeStamp::where('TASKID',$request->idTaskPost)->first();
          // return $timestampz;
          if($timestampz==null){
            $timestamp = new AdmTimeStamp();
          }else{
            $timestamp = AdmTimeStamp::where('TASKID',$request->idTaskPost)->first();
          }
          $timestamp->TIMESTART = $request->timestampValPost;
          $timestamp->TASKID = $cek_task->TASKID;
          $timestamp->MODUI = Session::get('users')->email;
          $timestamp->MODDA = date('Y-m-d H:i:s');
          $timestamp->save();

          $dataTimestamp[] = array(
            'TASKNAME' => $cek_task->TASKNAME,
            'TIMESTART' => $request->timestampValPost,
            'userInsert' => Session::get('users')->email,
            'insertDate' => date('Y-m-d H:i:s')
          );

          $act = "Task Configuration";
          $desc = "Update data TASK Timetamps with value ".json_encode($dataTimestamp);
          $activityLog->logUser($act, $desc);

        }else{
          AdmTimeStamp::where('TASKID', $request->idTaskPost)->delete();

          $rownumz = AdmRowNum::where('TASKID',$request->idTaskPost)->first();
          // return $rownumz;
          if($rownumz==null){
            $rownum = new AdmRowNum();
          }else{
            $rownum = AdmRowNum::where('TASKID',$request->idTaskPost)->first();
          }
          $rownum->ROWNUM = $request->rownumValPost;
          $rownum->ROWNUMDATE = date('Y-m-d');
          $rownum->TASKID = $cek_task->TASKID;
          $rownum->MODUI = Session::get('users')->email;
          $rownum->MODDA = date('Y-m-d H:i:s');
          $rownum->save();

          $dataRownum[] = array(
            'TASKNAME' => $cek_task->TASKNAME,
            'ROWNUM' => $request->rownumValPost,
            'ROWNUMDATE' => date('Y-m-d'),
            'userInsert' => Session::get('users')->email,
            'insertDate' => date('Y-m-d H:i:s')
          );

          $act = "Task Configuration";
          $desc = "Update data TASK Rownum with value ".json_encode($dataRownum);
          $activityLog->logUser($act, $desc);
        }

      }else{
        // dd("sini");
        // $rownum = AdmRowNum::where('TASKID',$request->idTaskPost)->first();
        // $rownum->ROWNUM = 0;
        // $rownum->ROWNUMDATE = date('Y-m-d');
        // $rownum->TASKID = $cek_task->TASKID;
        // $rownum->MODUI = Auth::user()->name;
        // $rownum->MODDA = date('Y-m-d H:i:s');
        // $rownum->save();
      }

      $data[] = array(
        'result' => 'success'
      );

      return json_encode($data);

    }

    public function deleteTable($id){

      AdmMapTable::where('MAPTBLID', $id)->destroy();

    }

    public function create(activityLog $activityLog){

      $act = "Task Configuration";
      $desc = "Opening form create task configuration";
      $activityLog->logUser($act, $desc);

      $dbSource   = AdmDBCon::where('DBPROVIDER','!=','mysql')->where('TYPEDB',0)->get();
      $dbDest   = AdmDBCon::where('DBPROVIDER','!=','mysql')->where('TYPEDB',1)->get();
     
      $task = null;
        // return view('management.mapping.mapping')
        return view('management.mapping.mappingReboot')
        ->with('dbSource', $dbSource)
        ->with('dbTarget', $dbDest)
        ->with('task', $task);
    }

    public function checkTable($taskId){

      $cek_table = AdmMapTable::where('TASKID', $taskId)->count();

      if($cek_table!=0){
        $cek_table = AdmMapTable::where('TASKID', $taskId)->get();
      }else{
        $cek_table = "empty";
      }

      $data[] = array(
        'data' => $cek_table
      );

      return json_encode($data);
    }
    public function updateTable($jenis, $table, $idtable, $taskId){

      if($idtable=="nan"){

        if($jenis=="target"){

          $exp1 = explode('.', $table);
          // dd($exp1);
          $addTable = new AdmMapTable();
          $addTable->TBLDEST = $exp1[1];
          $addTable->TASKID = $taskId;
          $addTable->INSUI = Session::get('users')->email;
          $addTable->INSDA = date('Y-m-d H:i:s');
          $addTable->save();

        }else if($jenis=="source"){

          $exp1 = explode('.', $table);

          $addTable = new AdmMapTable();
          $addTable->TBLSOURCE = $exp1[1];
          $addTable->TASKID = $taskId;
          $addTable->INSUI = Session::get('users')->email;
          $addTable->INSDA = date('Y-m-d H:i:s');
          $addTable->save();

        }

        $cekTable = AdmMapTable::where('TASKID', $taskId)->orderby('MAPTBLID','desc')->first();
        // dd($cekTable);
        $data[] = array(
          'tableId' => $cekTable->MAPTBLID
        );
        return json_encode($data);
      }else{

        $cekTable = AdmMapTable::where('MAPTBLID', $idtable)->first();

        if($jenis=="target"){

          $exp1 = explode('.', $table);
          // return $exp1[1];
          $cekTable->TBLDEST = $exp1[1];
          $cekTable->TASKID = $taskId;
          $cekTable->MODUI = Session::get('users')->email;
          $cekTable->MODDA = date('Y-m-d H:i:s');
          $cekTable->save();

        }else if($jenis=="source"){

          $exp1 = explode('.', $table);

          $cekTable->TBLSOURCE = $exp1[1];
          $cekTable->TASKID = $taskId;
          $cekTable->MODUI = Session::get('users')->email;
          $cekTable->MODDA = date('Y-m-d H:i:s');
          $cekTable->save();

        }

        $idTableUp = $idtable;

      }

      $data[] = array(
        'tableId' => $idtable
      );
      return json_encode($data);

    }

    public function admTableUpdate(Request $request){

      $cekTable = AdmMapTable::where('MAPTBLID', $request->idTableTxt)->first();
      $cekTable->SCRIPTQUERY = $request->queryText;
      $cekTable->MODUI = Session::get('users')->email;
      $cekTable->MODDA = date('Y-m-d H:i:s');
      $cekTable->save();

    }
    public function admTaskUpdate(Request $request){
      $task = AdmTask::where('TASKID', $request->taskid)->first();
      // dd($task);
      $task->TASKNAME = $request->taskName;
      $task->TASKINTERVAL = $request->taskInterval;
      $task->ISINSERT = $request->proses_type;
      $task->ISROWNUM = $request->data_type;
      $task->PRIMARYKEY1 = $request->primaryKey1;
      $task->PRIMARYKEY2 = $request->primaryKey2;
      $task->DBSOURCEID = $request->selectSource;
      $task->DBDESTID = $request->selectTarget;
      $task->MODUI = Session::get('users')->email;
      $task->MODDA = date('Y-m-d H:i:s');
      $task->save();

      if($request->data_type=="0"){
          $timestamp = AdmTimeStamp::where('TASKID', $request->taskid)->first();
          // dd($timestamp);
          $timestamp->TIMESTART = $request->timestampVal;
          $timestamp->INSDA = date('Y-m-d');
          $timestamp->TASKID = $task->TASKID;
          $timestamp->MODUI = Session::get('users')->email;
          $timestamp->MODDA = date('Y-m-d H:i:s');
          $timestamp->save();
      }else{
        // dd("sini");
        $rownum = AdmRowNum::where('TASKID', $request->taskid)->first();
        $rownum->ROWNUM = $request->rownumVal;
        $rownum->ROWNUMDATE = date('Y-m-d');
        $rownum->TASKID = $task->TASKID;
        $rownum->MODUI = Session::get('users')->email;
        $rownum->MODDA = date('Y-m-d H:i:s');
        $rownum->save();
      }

      return json_encode("done");
    }

    public function edit(CurlGenerator $curlGen, activityLog $activityLog, $id){

      $task = AdmTask::with('dbsource','dbtarget','mapTable','rownum','timestamp')->where('TASKID', $id)->first();
      //return $task;
      $cekKoneksiSmarts = "/datasnap/rest/TRESTMethods/TestConnection/".$task->dbsource->DBPROVIDER.";".$task->dbsource->DBHOSTNAME.";".$task->dbsource->DBPORT.";".$task->dbsource->DBNAME.";".$task->dbsource->DBUSERID.";".base64_decode($task->dbsource->DBPASSWORD).";";
      $cekKoneksiDwh = "/datasnap/rest/TRESTMethods/TestConnection/".$task->dbtarget->DBPROVIDER.";".$task->dbtarget->DBHOSTNAME.";".$task->dbtarget->DBPORT.";".$task->dbtarget->DBNAME.";".$task->dbtarget->DBUSERID.";".base64_decode($task->dbtarget->DBPASSWORD).";";
      $paramSmarts = $curlGen->testConnection($cekKoneksiSmarts);
      $paramDwh = $curlGen->testConnection($cekKoneksiDwh);


      if($paramSmarts!="Database is connected successfully"){
        return redirect(url('management/mapping/table'))
        ->with('info', 'Information')
        ->with('alert', 'red')
        ->with('icons', '')
        ->with('alert', 'DB Source : '.$task->dbsource->DBSERVERNAME.' cannot connect');
      }
      if($paramDwh!="Database is connected successfully"){
        return redirect(url('management/mapping/table'))
        ->with('info', 'Information')
        ->with('alert', 'red')
        ->with('icons', '')
        ->with('alert', 'DB Destination : '.$task->dbtarget->DBSERVERNAME.' cannot connect');
      }
      $dbSource   = AdmDBCon::where('DBPROVIDER','!=','mysql')->where('TYPEDB',0)->get();
      $dbDest   = AdmDBCon::where('DBPROVIDER','!=','mysql')->where('TYPEDB',1)->get();
      $dbTemp   = AdmDBCon::where('DBPROVIDER','!=','mysql')->where('TYPEDB',0)->get();

      $field = AdmMapField::where('MAPTBLID', $task->mapTable->MAPTBLID)->get();

      $getTabSource = $this->getTableFromEngineView($task->dbSource->DBCONNID);
      $getTabDest = $this->getTableFromEngineView($task->dbtarget->DBCONNID);

      $getFieldSource = $this->getTableDefinitionFromEngineView($task->dbSource->DBCONNID,$task->mapTable->TBLSOURCE);
      $getFieldDest = $this->getTableDefinitionFromEngineView($task->dbtarget->DBCONNID,$task->mapTable->TBLDEST);

       //return $getTabSource;

         if($task->ISROWNUM=="1" || $task->ISROWNUM==1){
          $type_data = AdmRowNum::where('TASKID', $task->TASKID)->first();
        }else{
          $type_data = AdmTimeStamp::where('TASKID', $task->TASKID)->first();
        }

        // return $field;

        $act = "Task Configuration";
        $desc = "Opening form edit task configuration TASKNAME :".$task->TASKNAME;
        $activityLog->logUser($act, $desc);
        // return $getTabDest;
        return view('management.mapping.edit')
        ->with('dbSource', $dbSource)
        ->with('dbTarget', $dbDest)
        ->with('dbTemp', $dbTemp)
        ->with('task', $task)
        ->with('type_data', $type_data)
        ->with('getTabSource', $getTabSource)
        ->with('getTabDest', $getTabDest)
        ->with('getFieldSource', $getFieldSource)
        ->with('getFieldDest', $getFieldDest)
        ->with('field', $field);
    }

    public function getFieldSource($dbconnid,$TABLE_NAME){

      $DBConnection = new DBConnection;
      $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->first();
      $connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);
      $columns = array();

      // $TABLE_SCHEMA = explode('.', $tablename)[0];
      // $TABLE_NAME = explode('.', $tablename)[1];

    		$result = $connection->table('information_schema.columns')->where('table_name',$TABLE_NAME)->where('table_schema','public')->orderBy('column_name','asc')->get();
        $i = 1;
	    	foreach($result as $column){
	    		$columns[] = array(
            'id' => $i++,
	    			'COLUMN_NAME'=>$column->column_name,
	    			'DATA_TYPE'=>$column->udt_name,
	    		);
	    	}

      $DBConnection->disconnect($AdmDBCon->DBPROVIDER);

      return $columns;

    }
    public function getFieldDestination($dbconnid,$TABLE_NAME){

      $DBConnection = new DBConnection;
      $AdmDBCon   = AdmDBCon::where('DBCONNID',$dbconnid)->first();
      $connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);
      $columns    = array();

    		$result = $connection->table('INFORMATION_SCHEMA.COLUMNS')->where('TABLE_NAME',$TABLE_NAME)->where('TABLE_SCHEMA','dbo')->orderBy('COLUMN_NAME','asc')->get();
        $i = 1;
	    	foreach($result as $column){
	    		$columns[] = array(
            'id' => $i++,
	    			'COLUMN_NAME'=>$column->COLUMN_NAME,
	    			'DATA_TYPE'=>$column->DATA_TYPE,
	    		);
	    	}

        $DBConnection->disconnect($AdmDBCon->DBPROVIDER);
      	return $columns;

    }
    public function getTableSource($dbconnid){

      $DBConnection = new DBConnection;

      $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->where('TYPEDB',0)->first();
      $connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);
      // dd($connection);
      $tables = array();

      $result = $connection->table('information_schema.tables')->orderBy('table_schema','asc')->orderBy('table_name','asc')->get();

	    	foreach($result as $table){
	    		// $tables[] = $table->table_schema.'.'.$table->table_name;
	    		$tables[] = $table->table_name;
	    	}

    	$DBConnection->disconnect($AdmDBCon->DBPROVIDER);

      return $tables;
    }

    public function getTableDestination($dbconnid){

      $DBConnection = new DBConnection;
      $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->where('TYPEDB',1)->first();

      $connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);
      $tables = array();

    		$result = $connection->table('INFORMATION_SCHEMA.TABLES')->orderBy('TABLE_SCHEMA','asc')->orderBy('TABLE_NAME','asc')->get();

	    	foreach($result as $table){
	    		// $tables[] = $table->TABLE_SCHEMA.'.'.$table->TABLE_NAME;
	    		$tables[] = $table->TABLE_NAME;
	    	}

        $DBConnection->disconnect($AdmDBCon->DBPROVIDER);

        return $tables;

    }

    public function getDatabaseTable(DBConnection $DBConnection, $dbconnid, $type){
      ini_set('memory_limit', '-1');

      if($type=="pgsql"){
        $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->where('TYPEDB',0)->first();
      }else{
        $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->where('TYPEDB',1)->first();
      }

      // dd($AdmDBCon);
      // dd($AdmDBCon);
    	$connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);
      // return;
    	$tables = array();

    	if($AdmDBCon->DBPROVIDER == 'SQL Server'){
    		$result = $connection->table('INFORMATION_SCHEMA.TABLES')->orderBy('TABLE_SCHEMA','asc')->orderBy('TABLE_NAME','asc')->get();

	    	foreach($result as $table){
	    		// $tables[] = $table->TABLE_SCHEMA.'.'.$table->TABLE_NAME;
	    		$tables[] = $table->TABLE_NAME;
	    	}
    	}

    	if($AdmDBCon->DBPROVIDER == 'PostgreSQL'){
    		$result = $connection->table('information_schema.tables')->orderBy('table_schema','asc')->orderBy('table_name','asc')->get();

	    	foreach($result as $table){
	    		// $tables[] = $table->table_schema.'.'.$table->table_name;
	    		$tables[] = $table->table_name;
	    	}
	    }

    	$DBConnection->disconnect($AdmDBCon->DBPROVIDER);

    	echo json_encode($tables);
    }

    public function getTableDefinition($dbconnid,$tablename,DBConnection $DBConnection){
    	$AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->first();

    	$connection =  $DBConnection->connect($AdmDBCon->DBPROVIDER,$AdmDBCon);

    	$columns = array();

    	// $TABLE_SCHEMA = explode('.', $tablename)[0];
    	// $TABLE_NAME = explode('.', $tablename)[1];
    	$TABLE_NAME = explode('.', $tablename);

    	if($AdmDBCon->DBPROVIDER == 'SQL Server'){
    		$result = $connection->table('INFORMATION_SCHEMA.COLUMNS')->where('TABLE_NAME',$TABLE_NAME)->where('TABLE_SCHEMA','dbo')->orderBy('COLUMN_NAME','asc')->get();
        $i = 1;
	    	foreach($result as $column){
	    		$columns[] = array(
            'id' => $i++,
	    			'COLUMN_NAME'=>$column->COLUMN_NAME,
	    			'DATA_TYPE'=>$column->DATA_TYPE,
	    		);
	    	}
    	}

    	if($AdmDBCon->DBPROVIDER == 'PostgreSQL'){
    		$result = $connection->table('information_schema.columns')->where('table_name',$TABLE_NAME)->where('table_schema','public')->orderBy('column_name','asc')->get();
        $i = 1;
	    	foreach($result as $column){
	    		$columns[] = array(
            'id' => $i++,
	    			'COLUMN_NAME'=>$column->column_name,
	    			'DATA_TYPE'=>$column->udt_name,
	    		);
	    	}
	    }

    	$DBConnection->disconnect($AdmDBCon->DBPROVIDER);
      // return count($columns);
    	echo json_encode($columns);
    }


    public function getTableFromEngine(CurlGenerator $curlGen, $dbconnid){
      ini_set('memory_limit', '-1');

      $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->first();

      $url = "/datasnap/rest/TRESTMethods/GetTableList/".$dbconnid;
      $param = $curlGen->getIndex($url);

      $tables = explode(';', $param);

      echo json_encode($tables);

    }
    public function getTableFromEngineView($dbconnid){
      ini_set('memory_limit', '-1');

      $curlGen = new CurlGenerator();
      $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->first();

      $url = "/datasnap/rest/TRESTMethods/GetTableList/".$dbconnid;
      $param = $curlGen->getIndex($url);

      $tables = explode(';', $param);

      return $tables;

    }

    public function getTableDefinitionFromEngine(CurlGenerator $curlGen, $dbconnid, $tablename){

      ini_set('memory_limit', '-1');

      $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->first();

      $url = "/datasnap/rest/TRESTMethods/GetFieldsList/".$dbconnid.";".$tablename;

      $param = $curlGen->getIndex($url);
      //return $param;
      $parsing = explode(';', $param);
      $columns = array();
      $i = 1;
      foreach($parsing as $pas){
        $hasil_parsing = explode(',', $pas);

        $columns[] = array(
          'id' => $i++,
          'COLUMN_NAME' => $hasil_parsing[0],
          'DATA_TYPE'=> $hasil_parsing[1],
        );

      }

      echo json_encode($columns);

    }

    public function getTableDefinitionFromEngineView($dbconnid, $tablename){

      ini_set('memory_limit', '-1');

      $curlGen  = new CurlGenerator;
      $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->first();

      $url = "/datasnap/rest/TRESTMethods/GetFieldsList/".$dbconnid.";".$tablename;

      $param = $curlGen->getIndex($url);
      //return $param;
      $parsing = explode(';', $param);
      $columns = array();
      $i = 1;
      foreach($parsing as $pas){
        $hasil_parsing = explode(',', $pas);

        $columns[] = array(
          'id' => $i++,
          'COLUMN_NAME' => $hasil_parsing[0],
          'DATA_TYPE'=> $hasil_parsing[1],
        );

      }

      return $columns;

    }

    public function changeFieldFromEngine(CurlGenerator $curlGen, $dbconnid, $tablename, $field){

      ini_set('memory_limit', '-1');

      $expField = explode('-', $field);

      $AdmDBCon = AdmDBCon::where('DBCONNID',$dbconnid)->first();

      $url = "/datasnap/rest/TRESTMethods/GetFieldType/".$dbconnid.";".$expField[1].";".$tablename;

      $param = $curlGen->getIndex($url);
      // return $param;
      $columns = array();

        $columns[] = array(
          'id' => 1,
          'COLUMN_NAME' => $field,
          'DATA_TYPE'=> $param,
        );

      echo json_encode($columns[0]);

    }


}
