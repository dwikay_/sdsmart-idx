<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\BackupExport;
use App\Model\AdmMapTable;
use App\Model\AdmMapField;
use App\Model\AdmDBCon;
use App\Model\AdmTask;
use App\Model\Log;
use App\Model\Audit;
use App\Model\AdmRowNum;
use App\Model\activityUser;
use App\Model\AdmTimeStamp;
use App\Library\DBConnection;
use App\Library\CurlGenerator;
use Config;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use App\Library\activityLog;

class UserBckLogController extends Controller
{
  public function index(activityLog $activityLog){

    $act = "Backup User Log Activity";
    $desc = "Opening Backup User Log Activity";
    $activityLog->logUser($act, $desc);

    return view('users.backupLog.index');
  }

  public function downloadNew(Request $request, activityLog $activityLog,$startdate, $enddate)
  {
    ini_set('max_execution_time',7200);
    $push = [];
    $log = activityUser::with('users')->whereBetween('date', [$startdate, $enddate])->orderby('id','desc')->get();
	   // return $log;
    $act = "Download Backup User Log Activity";
    $desc = "Download Backup User Log Activity on date ".$startdate." until ".$enddate;
    $activityLog->logUser($act, $desc);

     $export = new BackupExport([
       $push,
     ]);
     $i = 1;
     foreach ($log as $key => $item) {
       
       array_push($push, array(
          $i++,
          $item->username,
          $item->group,
          $item->activity,
          $item->activityDescription,
          $item->date,
          $item->time,
       ));
     }

     $export = new BackupExport([
       $push,
     ]);
     // dd($export);
    Excel::store($export, "file/excelLogUser/User Activities Log-SDSMARTS ".$startdate." sd ".$enddate.".csv");

    return json_encode("User Activities Log-SDSMARTS ".$startdate." sd ".$enddate.".csv");
  }

  function retriveFile($filename){
    $responses = response()->download(public_path("file/excelLogUser/".$filename));
    return $responses;
  }
  function removeFile($filename){
    unlink(public_path("file/excelLogUser/".$filename));
    return json_encode(true);
  }

  public function download(Request $request, activityLog $activityLog,$startdate, $enddate){

    ini_set('max_execution_time',7200);
    $log = activityUser::with('users')->whereBetween('date', [$startdate, $enddate])->get();
	// return $log;
    $act = "Download Backup User Log Activity";
    $desc = "Download Backup User Log Activity on date ".$startdate." until ".$enddate;
    $activityLog->logUser($act, $desc);

    Excel::create("User Activities Log-SDSMARTS ".$startdate." sd ".$enddate, function ($result) use ($log) {
  				$result->sheet('SheetName', function ($sheet) use ($log) {
            $i = 1;
  						foreach ($log as $key => $item) {

  							$data=[];
  							array_push($data, array(
                					$i++,
							$item->username,
							$item->group,
							$item->activity,
							$item->activityDescription,
							$item->date,
							$item->time,
  						));
  						$sheet->fromArray($data, null, 'A2', false, false);
  						}
  						$sheet->row(1, array(
                				'No',
                				'Username',
                				'Group',
                				'Activity',
                				'Activity Description',
                				'Date',
                				'Time'));

  							$sheet->setBorder('A1:G1', 'thin');
  								$sheet->cells('A1:G1', function ($cells) {
  								$cells->setBackground('#0070c0');
  								$cells->setFontColor('#ffffff');
  								$cells->setValignment('center');
  								$cells->setFontSize('11');
  							});
  								$sheet->setHeight(array(
  							'1' => '20'
  						));
  						$sheet->setWidth('A', '10');
  						$sheet->setWidth('B', '25');
  						$sheet->setWidth('C', '25');
  						$sheet->setWidth('D', '25');
  						$sheet->setWidth('E', '100');
  						$sheet->setWidth('F', '25');
  						$sheet->setWidth('G', '25');
  						});
  					ob_end_clean();
  			})->store('csv', public_path('file/excelLogUser/'));


        return json_encode("User Activities Log-SDSMARTS ".$startdate." sd ".$enddate.".csv");



  }

}
