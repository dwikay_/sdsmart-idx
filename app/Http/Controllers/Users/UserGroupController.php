<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Role;
use App\Model\Roleacl;
use App\User;
use App\Model\Module;
use Validator;
use Session;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Library\activityLog;

class UserGroupController extends Controller
{
  public function index(activityLog $activityLog){

    $act = "Administrator";
    $desc = "Opening User Group";
    $activityLog->logUser($act, $desc);

    return view('users.userGroup.index');
  }

  public function getUsers(){

    $user = Role::all();
    return Datatables::of($user)->escapeColumns([])->make(true);
  }

  public function create(activityLog $activityLog){

    $act = "Administrator";
    $desc = "Opening Form User Group";
    $activityLog->logUser($act, $desc);

    $modules = Module::where('menu_parent', '0')->get();
    return view('users.userGroup.create')
    ->with('module', $modules);
  }

  public function store(activityLog $activityLog, Request $request){

    // return $request->all();

      $role_name = $request->input('role_name');
      $description = $request->input('description');

      $validator = Validator::make($request->all(), [
        'role_name' => 'required',
        'description' => 'required'
      ]);

      if($validator->fails()) {
        return redirect(url('usersGadai/userGroup/create'));
      }

      $role = Role::create([
        'role_name' => $role_name,
        'description' => $description
      ]);

      $dataRole[] = array(
        'roleName' => $role_name,
        'description' => $description
      );

      $act = "Task Configuration";
      $desc = "Save data userGroup with value ".json_encode($dataRole);
      $activityLog->logUser($act, $desc);

      $roles = Module::where('menu_parent', '!=', 0)->get();

      $dataModule[] = array(
        "moduleList" => [],
        'userInsert' => Session::get('users')->email,
        'insertDate' => date('Y-m-d H:i:s')
      );

      foreach ($roles as $key => $module) {

        $cekModule = Module::where('kdModule', $module->kdModule)->first();

        Roleacl::create([
          'module_id' => $module->kdModule,
          'role_id' => $role->id,
          'create_acl' => $request->input($module->kdModule.'_create'),
          'read_acl' => $request->input($module->kdModule.'_read'),
          'update_acl' => $request->input($module->kdModule.'_update'),
          'delete_acl' => $request->input($module->kdModule.'_delete'),
          'module_parent' =>  $module->menu_parent,
        ]);

        $dataModule["moduleList"][$key] = array(
            "Module"=> $cekModule->module_name,
            "Role"=> $role->role_name,
            "allowCreate"=> $request->input($module->kdModule.'_create') != "" ? "Enable" : "Disable",
            "allowRead"=> $request->input($module->kdModule.'_read') != "" ? "Enable" : "Disable",
            "allowUpdate"=> $request->input($module->kdModule.'_update') != "" ? "Enable" : "Disable",
            "allowDelete"=> $request->input($module->kdModule.'_delete') != "" ? "Enable" : "Disable"
          );

      }

      $cek_group = Role::orderby('id','desc')->first();
      $act = "Administrator";
      $desc = "Save data Module with value ".json_encode($dataModule);
      $activityLog->logUser($act, $desc);

      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Saved');

      return redirect(url('users/group'));

  }
  public function edit(activityLog $activityLog, $id){

    $role = Role::find($id);
    $module = Module::where('menu_parent', '0')->get();

    $act = "Administrator";
    $desc = "Opening form editor userGroup Role ".$role->role_name;
    $activityLog->logUser($act, $desc);

    return view('users.userGroup.edit')
    ->with('role', $role)->with('module', $module);
  }
  public function countUsers($role_id){

  $cek_user = User::where('role_id', $role_id)->count();

  if($cek_user==0){$result = "kosong";}
  else{$result = "ada";}

  return json_encode($result);

  }
  public function update(Request $r,activityLog $activityLog, $id){

    $role_name = $r->input('role_name');
    $description = $r->input('description');

    $role = Role::find($id)->update([
      'role_name' => $role_name,
      'description' => $description
    ]);

    $dataRole[] = array(
      'roleName' => $role_name,
      'description' => $description
    );

    $act = "Task Configuration";
    $desc = "Update data userGroup with value ".json_encode($dataRole);
    $activityLog->logUser($act, $desc);

    $roles = Module::where('menu_parent', '!=', 0)->get();

    $dataModule[] = array(
      "moduleList" => [],
      'userInsert' => Session::get('users')->email,
      'insertDate' => date('Y-m-d H:i:s')
    );

    foreach ($roles as $key => $modules) {
      $cekModule = Module::where('kdModule', $modules->kdModule)->first();
        $data = [
            'module_id' => $modules->kdModule,
            'role_id' => $id,
            'create_acl' => $r->input($modules->kdModule.'_create'),
            'read_acl' => $r->input($modules->kdModule.'_read'),
            'update_acl' => $r->input($modules->kdModule.'_update'),
            'delete_acl' => $r->input($modules->kdModule.'_delete'),
            'module_parent' => $modules->menu_parent,
        ];
        $cek = RoleAcl::where('module_id', $modules->kdModule)->where('role_id', $id)->first();
        if ($cek == null) {
            $rolenya = RoleAcl::create($data);
            $acts = "Save";
        } else {
            $rolenya = RoleAcl::find($cek->id);
            $rolenya->update($data);
            $acts = "Update";
        }

        $dataModule["moduleList"][$key] = array(
            "Module"=> $cekModule->module_name,
            "Role"=> $role_name,
            "allowCreate"=> $r->input($modules->kdModule.'_create') != "" ? "Enable" : "Disable",
            "allowRead"=> $r->input($modules->kdModule.'_read') != "" ? "Enable" : "Disable",
            "allowUpdate"=> $r->input($modules->kdModule.'_update') != "" ? "Enable" : "Disable",
            "allowDelete"=> $r->input($modules->kdModule.'_delete') != "" ? "Enable" : "Disable"
          );

    }

    $cek_group = Role::orderby('id','desc')->first();
    $act = "Administrator";
    $desc = "".$acts." data Module with value ".json_encode($dataModule);
    $activityLog->logUser($act, $desc);

    $cek_group = Role::find($id);

    Session::flash('info', 'Success');
    Session::flash('colors', 'green');
    Session::flash('icons', 'fas fa-check-circle');
    Session::flash('alert', 'Saved');

    $act = "Administrator";
    $desc = "Update data User Group ".$cek_group;
    $activityLog->logUser($act, $desc);

    return redirect(url('users/group'));

  }

  public function delete(activityLog $activityLog,$id)
    {

      $role = Role::where('id', $id)->first();
      $act = "Administrator";
      $desc = "Delete User Group role ".$role->role_name;
      $activityLog->logUser($act, $desc);

      Role::where('id', $id)->delete();
      Roleacl::where('role_id', $id)->delete();



      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Deleted');
      return redirect(url('users/group'));
    }

    public function checkingDelete(activityLog $activityLog,$id)
    {

      $role = Role::where('id', $id)->first();
      $act = "Administrator";
      $desc = "Delete User Group role ".$role->role_name;
      $activityLog->logUser($act, $desc);

      Role::where('id', $id)->delete();
      Roleacl::where('role_id', $id)->delete();



      Session::flash('info', 'Success');
      Session::flash('colors', 'green');
      Session::flash('icons', 'fas fa-check-circle');
      Session::flash('alert', 'Saved');
      return redirect(url('users/group'));
    }
}
