<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AdmMapTable;
use App\Model\AdmMapField;
use App\Model\AdmDBCon;
use App\Model\AdmTask;
use App\Model\Log;
use App\Model\Role;
use App\Model\Audit;
use App\Model\AdmRowNum;
use App\Model\AdmTimeStamp;
use App\Model\activityUser;
use App\Library\DBConnection;
use App\Library\CurlGenerator;
use Config;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;
use App\Library\activityLog;

class UserActLogController extends Controller
{
    public function index(activityLog $activityLog){

      $act = "User Log Activity";
      $desc = "Opening User Log Activity";
      $activityLog->logUser($act, $desc);

      return view('users.userLog.index');
    }
    public function getIndex(Request $request){

      $audit = activityUser::with('role')->whereBetween('date', [$request->startDate, $request->endDate])->orderby('id','desc')->get();

      foreach ($audit as $key => $value) {
        
        $timestamp = strtotime($value->date);
        $date = date('d-M-y', $timestamp);

        $data[] = array(
          'id' => $value->id,
          'username' => $value->username,
          'group' => $value->group,
          'activity' => $value->activity,
          'activityDescription' => $value->activityDescription,
          'date' => $date,
          'time' => $value->time,
          'role_name' => $value->group
        );
      }

      return Datatables::of($data)->escapeColumns([])->make(true);

    }
}
