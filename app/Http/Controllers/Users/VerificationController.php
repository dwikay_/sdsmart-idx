<?php

namespace App\Http\Controllers\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Session;

class VerificationController extends Controller
{
  public function getVerification($username){

    try {

    $cek_user = User::where('email', $username)->where('verify_password','0')->firstOrFail();

    return view('auth.verification')
    ->with('username', $username);

    } catch (ModelNotFoundException $e) {

      Session::flash('info', 'Error');
      Session::flash('colors', 'red');
      Session::flash('icons', 'fas fa-times');
      Session::flash('alert', 'Username not found');
      return redirect()->back();

    }


  }

  public function postVerification(Request $request, $username){

    try {

    $cek_user = User::where('email', $username)->where('verify_password','0')->firstOrFail();

    $cek_user->password = bcrypt($request->password);
    $cek_user->verify_password = 1;
    $cek_user->save();

    Session::flash('info', 'Error');
    Session::flash('colors', 'red');
    Session::flash('icons', 'fas fa-times');
    Session::flash('alert', 'Password changed, Please login');
    // dd("masuk");
    return redirect(url('monitoring/monJobs'));

    } catch (ModelNotFoundException $e) {

      Session::flash('info', 'Error');
      Session::flash('colors', 'red');
      Session::flash('icons', 'fas fa-times');
      Session::flash('alert', 'Username not found');
      return redirect()->back();

    }


  }

}
