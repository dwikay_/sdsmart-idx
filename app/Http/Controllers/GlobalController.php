<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Artisan;
use Config;

class GlobalController extends Controller
{
  public function sessionSetStayAlive(){
    config(['session.lifetime' => 99999]);
    $sessionLifetime = Config::get('session.lifetime');
    echo $sessionLifetime;
     Artisan::call('config:cache');
     return json_encode(true);
  }
  public function sessionSetRollback(){
    Artisan::call('config:cache');
    config(['session.lifetime' => env('SESSION_LIFETIME')]);
    $sessionLifetime = env('SESSION_LIFETIME');
    echo $sessionLifetime;
    return json_encode(true);
  }
}
