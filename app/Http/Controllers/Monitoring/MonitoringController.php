<?php

namespace App\Http\Controllers\Monitoring;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\AdmMapTable;
use App\Model\AdmMapField;
use App\Model\AdmDBCon;
use App\Model\AdmTask;
use App\Model\AdmRowNum;
use App\Model\AdmTimeStamp;
use App\Library\DBConnection;
use App\Library\CurlGenerator;
use Config;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;
use Session;
use App\Library\activityLog;

class MonitoringController extends Controller
{
    public function index(activityLog $activityLog){

      $code = "Q2FrcmExMjMk";
      $pass = "P@ssw0rd";
	 //return base64_encode($pass);

      if(Session::get('users')==null){
        return redirect(url('/'));
      }
      $act = "Monitoring Jobs";
      $desc = "Opening Monitoring Jobs";
      $activityLog->logUser($act, $desc);

      return view('monitoring.monJobs');
    }

    public function startJob(activityLog $activityLog, CurlGenerator $curlGen, $id){

      $url = "/datasnap/rest/TRESTMethods/starttask/".$id;
      $param = $curlGen->getIndex($url);
      
      $task = AdmTask::where('TASKID', $id)->first();

      $act = "Monitoring Jobs";
      $desc = "Start Job Monitoring Jobs ".$task->TASKNAME."(".$id.")";
      $activityLog->logUser($act, $desc);

      return json_encode($param);

    }
    public function cancelJob(activityLog $activityLog, CurlGenerator $curlGen, $id){

      $url = "/datasnap/rest/TRESTMethods/stoptask/".$id;
      $param = $curlGen->getIndex($url);
      
      $task = AdmTask::where('TASKID', $id)->first();
     
      $act = "Monitoring Jobs";
      $desc = "Stop Job Monitoring Jobs ".$task->TASKNAME."(".$id.")";
      $activityLog->logUser($act, $desc);

      return json_encode($param);

    }
    public function initialData(activityLog $activityLog, CurlGenerator $curlGen, $id){

      $url = "/datasnap/rest/TRESTMethods/InitialData/".$id;
      $param = $curlGen->getIndex($url);

      // return $param;
      $explodeResult = explode(',', $param);
      // return $explodeResult[0];

      if($explodeResult[0]=="00" || $explodeResult[0]==00){
        $updateRownum = AdmRowNum::where('TASKID', $explode[1])->first();
        $updateRownum->ROWNUM = 0;
        $updateRownum->save();
      }

      $task = AdmTask::where('TASKID', $id)->first();
      $act = "Monitoring Jobs";
      $desc = "Initial Data on Monitoring Jobs ".$task->TASKNAME."(".$id.")";
      $activityLog->logUser($act, $desc);

      return json_encode($explodeResult[1]);

    }
    public function getOneRecord(CurlGenerator $curlGen, $id){

      $url = "/datasnap/rest/TRESTMethods/GetDataTask/".$id;
      $param = $curlGen->getField($url);

	$expd2 = explode(',', $param);

        //return $param;
        $dates = str_replace('/','-', $expd2[8]);
        $var = $expd2[8];
        $vars = date("Y-m-d", strtotime($var));
        $timestamp = strtotime($vars);
        $date = date('d-M-y', $timestamp);

        $expd2 = explode(',', $param);

            $data[] = array(
              'TASKID' => $expd2[0],
              'TASKNAME' => $expd2[1],
              'PROSES_TYPE' => $expd2[2],
              'INTERVAL' => $expd2[3],
              'DBSOURCE' => $expd2[4],
              'TBLSOURCE' => $expd2[5],
              'DBDEST' => $expd2[6],
              'TBLDEST' => $expd2[7],
              'DATE' => $date ,
              'COUNTRECORD' => $expd2[9],
              'STATUS' => $expd2[10]
            );

      return $data;
    }
    public function getIndex(CurlGenerator $curlGen) {

      $url = "/datasnap/rest/TRESTMethods/GetDataTasks";
      $param = $curlGen->getField($url);
       
       //return $param;

      $expd1 = explode(';', $param);
      // return $expd1;
      foreach($expd1 as $key => $value){

        

        $expd2 = explode(',', $value);
        // return $expd2;
        $dates = str_replace('/','-', $expd2[8]);
        $var = $expd2[8];
        $vars = date("Y-m-d", strtotime($var));
        $timestamp = strtotime($vars);
        $date = date('d-M-y', $timestamp);
        $cekTask = AdmTask::where('TASKID',$expd2[0])->first();
        $cekConfigSource = AdmDBCon::where('DBCONNID',$cekTask->DBSOURCEID)->first();
        $cekConfigDest = AdmDBCon::where('DBCONNID',$cekTask->DBDESTID)->first();


            $data[] = array(
              'TASKID' => $expd2[0],
              'TASKNAME' => $expd2[1],
              'PROSES_TYPE' => $expd2[2],
              'INTERVAL' => $expd2[3],
              'DBSOURCE' => $cekConfigSource->DBSERVERNAME.' - '.$expd2[4],
              'TBLSOURCE' => $expd2[5],
              'DBDEST' => $cekConfigDest->DBSERVERNAME.' - '.$expd2[6],
              'TBLDEST' => $expd2[7],
              'DATE' => $date,
              'COUNTRECORD' => $expd2[9],
              'STATUS' => $expd2[10]
            );
      }

      return Datatables::of($data)->escapeColumns([])->make(true);
    }
}
