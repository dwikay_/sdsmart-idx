<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AdmDBCon;
use App\Model\AdmTask;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;
use App\Library\DBConnection;
use App\Library\CurlGenerator;
use Config;
use Session;
use DB;
use App\Library\activityLog;

class ConfigController extends Controller
{

  public function checkTask($serverName){

    //$cek_task = AdmTask::where('TASKNAME', $taskName)->first();
    $cekServerName = AdmDBCon::where(DB::raw('BINARY `DBSERVERNAME`'), $serverName)->first();

    if($cekServerName==null){
      $status = "kosong";
    }else{
      $status = "ada";
    }
    $data[] = array(
      "status" => $status
    );

    return json_encode($data);

  }
  public function checkTaskUpdate($serverName, $id){

    $cekDBConName = AdmDBCon::where('TASKID', $id)->first();

    if ($serverName != $cekDBConName->DBSERVERNAME){

      $cekServerName = AdmDBCon::where(DB::raw('BINARY `DBSERVERNAME`'), $serverName)->first();

      if($cekServerName==null){
        $status = "kosong";
      }else{
        $status = "ada";
      }
    }else{
      $status = "kosong";
    }

    $data[] = array(
      "status" => $status
    );

    return json_encode($data);

  }
   public function Checkupdate(CurlGenerator $curlGen,$id, $typeDB,activityLog $activityLog){

      if($typeDB==0){
        $cekTask = AdmTask::where('DBSOURCEID', $id)->count();
      }else{
        $cekTask = AdmTask::where('DBDESTID', $id)->count();
      }
      if($cekTask==0){
         $notif = "kosong";

        $data[] = array(
          "notif" => $notif
        );
        return json_encode($data);
      }else{
          if($typeDB==0){
              $cekTaskWithDBConfig = AdmTask::where('DBSOURCEID', $id)->get();
          }else{
              $cekTaskWithDBConfig = AdmTask::where('DBDESTID', $id)->get();
          }
          //dd($cekTaskWithDBConfig);
          foreach($cekTaskWithDBConfig as $cekTask){
               $url = "/datasnap/rest/TRESTMethods/GetDataTask/".$cekTask->TASKID;
               $param = $curlGen->getField($url);
               $expd2 = explode(',', $param);
               //dd($param);
            if($expd2[10]!="Stop"){
                $notif = "already";
                $data[] = array(
                "notif" => $notif
                );
               return json_encode($data);
            }
          }
          $notif = "kosong";
          $data[] = array(
            "notif" => $notif
          );
              return json_encode($data);
      }
    }

    public function CheckupdateConfirm($id, $typeDB){

      if($typeDB==0){
        $cekTask = AdmTask::where('DBSOURCEID', $id)->count();
      }else{
        $cekTask = AdmTask::where('DBDESTID', $id)->count();
      }


      if($cekTask==0){
        $notif = "kosong";
      }else{
        $notif = "already";
      }

      $data[] = array(
        "notif" => $notif
      );

      return json_encode($data);
    }

    public function CheckdeleteConfirm($id, $typeDB){

      if($typeDB==0){
        $cekTask = AdmTask::where('DBSOURCEID', $id)->count();
      }else{
        $cekTask = AdmTask::where('DBDESTID', $id)->count();
      }


      if($cekTask==0){
        $notif = "kosong";
      }else{
        $notif = "already";
      }

      $data[] = array(
        "notif" => $notif
      );

      return json_encode($data);
    }

   public function Checkdelete(CurlGenerator $curlGen,$id, $typeDB,activityLog $activityLog){

      if($typeDB==0){
        $cekTask = AdmTask::where('DBSOURCEID', $id)->count();
      }else{
        $cekTask = AdmTask::where('DBDESTID', $id)->count();
      }

      if($cekTask==0){
        $notif = "kosong";
      }else{
        $notif = "already";
      }

      $data[] = array(
        "notif" => $notif
      );

      return json_encode($data);
    }
    public function indexConfig(activityLog $activityLog){

      $act = "DB Config";
      $desc = "Opening DB Config";
      $activityLog->logUser($act, $desc);

      return view('management.dbconfig.index');
    }

    public function testConnection(Request $request, CurlGenerator $curlGen, activityLog $activityLog){

      $provider = $request->providerName;
      $ip = $request->ipAddress;
      $port = $request->port;
      $db = $request->databaseName;
      $username = $request->userID;
      $password = $request->password;
      $urls = "/datasnap/rest/TRESTMethods/TestConnection"."/".$provider.";".$ip.";".$port.";".$db.";".$username.";".$password.";";

      $param = $curlGen->testConnection($urls);

      $parameterLog[] = array(
        'Provider' => $request->providerName,
        'ipAddress' => $request->ipAddress,
        'port' => $request->port,
        'database' => $request->databaseName,
        'username' => $request->userID
      );

      $act = "Test Connection";
      $desc = "Testing Connection to service with parameter ".json_encode($parameterLog)." and this result : ".$param;
      $activityLog->logUser($act, $desc);

      return json_encode($param);

    }

    public function delete($id, activityLog $activityLog){

      $AdmDBCon = AdmDBCon::where('DBCONNID', $id)->first();

      $parameterLog[] = array(
        'Provider' => $AdmDBCon->DBPROVIDER,
        'ipAddress' => $AdmDBCon->DBHOSTNAME,
        'port' => $AdmDBCon->DBPORT,
        'database' => $AdmDBCon->DBNAME,
        'username' => $AdmDBCon->DBUSERID
      );

      $act = "DB Config";
      $desc = "Deleted Config with data ".json_encode($parameterLog);
      $activityLog->logUser($act, $desc);

      AdmDBCon::where('DBCONNID', $id)->delete();

      return redirect(url('management/config'));
    }

    public function checkingDelete($id, activityLog $activityLog){

      $AdmDBCon = AdmDBCon::where('DBCONNID', $id)->first();

      $parameterLog[] = array(
        'Provider' => $AdmDBCon->DBPROVIDER,
        'ipAddress' => $AdmDBCon->DBHOSTNAME,
        'port' => $AdmDBCon->DBPORT,
        'database' => $AdmDBCon->DBNAME,
        'username' => $AdmDBCon->DBUSERID
      );

      $act = "DB Config";
      $desc = "Deleted Config with data ".json_encode($parameterLog);
      $activityLog->logUser($act, $desc);

      AdmDBCon::where('DBCONNID', $id)->delete();

      return redirect(url('management/config'));
    }
    public function checkingUpdate($id, activityLog $activityLog){

      return redirect(url('management/config'));
    }

    public function getIndex(){

      $user = AdmDBCon::all();


      return Datatables::of($user)->escapeColumns([])->make(true);

    }
    public function createConfig(activityLog $activityLog){

      $act = "DB Config";
      $desc = "Opening Form Create Config";
      $activityLog->logUser($act, $desc);

      return view('management.dbconfig.config');
    }
    public function store(Request $request, activityLog $activityLog){

      date_default_timezone_set('Asia/Jakarta');

      // return $request->all();
      $dbcon = new AdmDBCon();
      $dbcon->DBSERVERNAME = $request->systemName;
      $dbcon->DBPROVIDER = $request->providerName;
      $dbcon->DBHOSTNAME = $request->ipAddress;
      $dbcon->DBUSERID = $request->userID;
      $dbcon->DBPASSWORD = base64_encode($request->password);
      $dbcon->DBPORT = $request->port;
      $dbcon->DBNAME = $request->databaseName;
      $dbcon->TYPEDB = $request->jenisDB;
      $dbcon->INSUI = Session::get('users')->name;
      $dbcon->INSDA = date('Y-m-d H:i:s');
      $dbcon->save();

      $parameterLog[] = array(
        'Server Name' => $request->systemName,
        'Provider' => $request->providerName,
        'ipAddress' => $request->ipAddress,
        'port' => $request->port,
        'database' => $request->databaseName,
        'username' => $request->userID
      );

      $act = "DB Config";
      $desc = "Saving Config with Parameter ".json_encode($parameterLog);
      $activityLog->logUser($act, $desc);

      $info = "success";
      $colors = "green";
      $icons = "fas fa-check-circle";
      $alert = "DB Config is succesfully save!";

      return redirect(url('management/config'))
      ->with('info', $info)
      ->with('alert', $alert)
      ->with('icons', $icons)
      ->with('alert', $alert);


    }
    public function edit($id, activityLog $activityLog){

      try {

        $dbcon = AdmDBCon::findOrFail($id);

        $act = "DB Config";
        $desc = "Opening Form Edit Config with ID : ".$id." and Server Name ".$dbcon->DBSERVERNAME;
        $activityLog->logUser($act, $desc);

        return view('management.dbconfig.edit')
        ->with('dbcon', $dbcon);

      } catch (ModelNotFoundException $e) {

        return redirect(url('management/config'))
        ->with('errmsg','Provider Name cant find')
        ->with('classerr','danger');
      }

    }
    public function update(Request $request, activityLog $activityLog, $id){

      try {

        $dbcon = AdmDBCon::findOrFail($id);

        $dbcon->DBSERVERNAME = $request->systemName;
        $dbcon->DBPROVIDER = $request->providerName;
        $dbcon->DBNAME = $request->databaseName;
        $dbcon->DBHOSTNAME = $request->ipAddress;
        $dbcon->DBPORT = $request->port;
        $dbcon->DBUSERID = $request->userID;
        $dbcon->DBPASSWORD = base64_encode($request->password);
        $dbcon->MODUI = Session::get('users')->name;
        $dbcon->TYPEDB = $request->jenisDB;
        $dbcon->MODDA = date('Y-m-d H:i:s');
        $dbcon->save();

        $dbconNew = AdmDBCon::orderby('DBCONNID','desc')->first();

        $parameterLog[] = array(
          'Server Name' => $request->systemName,
          'Provider' => $request->providerName,
          'ipAddress' => $request->ipAddress,
          'port' => $request->port,
          'database' => $request->databaseName,
          'username' => $request->userID
        );

        $act = "DB Config";
        $desc = "Update Config with Parameter ".json_encode($parameterLog);
        $activityLog->logUser($act, $desc);

        $info = "success";
        $colors = "green";
        $icons = "fas fa-check-circle";
        $alert = "DB Config is succesfully save!";

        return redirect(url('management/config'))
        ->with('info', $info)
        ->with('alert', $alert)
        ->with('icons', $icons)
        ->with('alert', $alert);


      } catch (ModelNotFoundException $e) {
        $dbcon = AdmDBCon::findOrFail($id);
        return view('management.dbconfig.edit')
        ->with('dbcon', $dbcon);
      }

    }
    public function indexMapping(activityLog $activityLog){

      $act = "Task Configuration";
      $desc = "Opening Form Task Configuration";
      $activityLog->logUser($act, $desc);

      return view('management.dbconfig.mapping');
    }



}
