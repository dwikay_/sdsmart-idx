<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\AdmMapTable;
use App\Model\AdmMapField;
use App\Model\AdmDBCon;
use App\Model\RecordLog;
use App\Model\AdmTask;
use App\Model\AdmRowNum;
use App\Model\AdmTimeStamp;
use App\Library\DBConnection;
use DB;
use App\Library\CurlGenerator;
use Config;
use yajra\Datatables\Datatables;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Auth;
use App\Library\activityLog;

class JobController extends Controller
{

  public function getOneRecord(CurlGenerator $curlGen, $id){

  $url = "/datasnap/rest/TRESTMethods/GetDataTask/".$id;
  $param = $curlGen->getField($url);

	$expd2 = explode(',', $param);
	$cek_task = AdmTask::where('TASKID', $expd2[0])->first();
        //return $param;

	$from = $cek_task->INSDA;
	$dt = new \DateTime($from);
	$date = $dt->format('d-M-y');

	$var = $expd2[8];

        $vars = date("Y-m-d", strtotime($var));
        $timestamp = strtotime($vars);
        $datejob = date('d-M-y', $timestamp);

	$vars1 = date("Y-m-d", strtotime($var));
        $timestamps = strtotime($vars1);
        $datesjob = date('Y-m-d', $timestamps);

	$expd2 = explode(',', $param);

            $data[] = array(
              'TASKID' => $expd2[0],
              'TASKNAME' => $expd2[1],
              'PROSES_TYPE' => $expd2[2],
              'INTERVAL' => $expd2[3],
              'DBSOURCE' => $expd2[4],
              'TBLSOURCE' => $expd2[5],
              'DBDEST' => $expd2[6],
              'TBLDEST' => $expd2[7],
              'DATE' => $date,
	      'DATEJOB' => $datejob,
	      'DATESJOB' => $datesjob,
              'COUNTRECORD' => $expd2[9],
              'STATUS' => $expd2[10]
            );

      return $data;
    }

  public function indexJob(activityLog $activityLog){
    $task = DB::table('adm_task')->orderByRaw('LENGTH(TASKNAME)','asc')->orderby('TASKNAME','asc')->get();

    $act = "Manual Run";
    $desc = "Opening Manual Run";
    $activityLog->logUser($act, $desc);

    return view('management.manualRun.index')
    ->with('task', $task);
  }

  public function checkRecord($id, $tanggal){

    $query = RecordLog::where('TASKID', $id)->where('RECORDDATE', $tanggal)->first();
    if($query == null){
      $record = 0;
    }else{
    $record = $query->RECORDCOUNT;
    }
    return json_encode($record);
  }
  public function startJob(activityLog $activityLog, CurlGenerator $curlGen, $id, $tanggal){

    $url = "/datasnap/rest/TRESTMethods/ManualJob/".$id.";".$tanggal;
    $param = $curlGen->getIndex($url);

    $cek_task = AdmTask::where('TASKID', $id)->first();
    $act = "Manual Jobs";
    $desc = "Start Manual Job ".$cek_task->TASKNAME."(".$id.")";
    $activityLog->logUser($act, $desc);

    return json_encode($param);

  }
  public function cancelJob(activityLog $activityLog, CurlGenerator $curlGen, $id){

    $url = "/datasnap/rest/TRESTMethods/stoptask/".$id;
    $param = $curlGen->getIndex($url);

    $cek_task = AdmTask::where('TASKID', $id)->first();
    $act = "Manual Jobs";
    $desc = "Stop Manual Job ".$cek_task->TASKNAME."(".$id.")";
    $activityLog->logUser($act, $desc);

    return json_encode($param);

  }
  public function initialData(activityLog $activityLog, CurlGenerator $curlGen, $id, $tanggal){

    $url = "/datasnap/rest/TRESTMethods/InitialData/".$id.";".$tanggal;
    $param = $curlGen->getIndex($url);

    // return $param;
    $explodeResult = explode(',', $param);
    // return $explodeResult[0];

    $cek_task = AdmTask::where('TASKID', $id)->first();

    if($cek_task->ISINSERT == "1" || $cek_task->ISINSERT == 1){
      if($explodeResult[0]=="00" || $explodeResult[0]==00){
        $updateRownum = AdmRowNum::where('TASKID', $id)->first();
        // return $updateRownum;
        $updateRownum->ROWNUM = 0;
        $updateRownum->save();
      }
    }


    $act = "Manual Jobs";
    $desc = "Initial Data on Manual Jobs ".$cek_task->TASKNAME."(".$id.")";
    $activityLog->logUser($act, $desc);

    return json_encode($explodeResult[1]);

  }


}
