<?php
namespace App\Library;
use Config;
use DB;

class DBConnection {

    public function connect($driver,$param){
        if($driver == 'SQL Server'){
            config(['database.connections.'.$driver => [
                'driver' => 'sqlsrv',
                'host' => $param->DBHOSTNAME,
                // 'port' => $param->DBPORT,
                'database' => $param->DBNAME,
                'username' => $param->DBUSERID,
                'password' => $param->DBPASSWORD,
                'charset' => 'utf8',
                'prefix' => '',
            ]]);
        }

        if($driver == 'PostgreSQL'){
            config(['database.connections.'.$driver => [
                'driver' => 'pgsql',
                'host' => $param->DBHOSTNAME,
                'port' => $param->DBPORT,
                'database' => $param->DBNAME,
                'username' => $param->DBUSERID,
                'password' => $param->DBPASSWORD,
                'charset' => 'utf8',
                'prefix' => '',
                'schema' => 'public',
                'sslmode' => 'prefer',
            ]]);
        }

        return DB::connection($driver);
    }

    public function disconnect($driver){
        return DB::disconnect($driver);
    }
}

?>
