<?php

namespace App\Library;

class CurlGenerator {

  public function testConnection($urls){

    $url = config('custom.api_url').$urls;

    date_default_timezone_set("Asia/Jakarta");

    $content_type = "application/json";

    // dd($url);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4000);

      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);
      $resultParam = json_decode($output);

      // dd($resultParam);
      if($resultParam==null){
        $asd = "Please check Engine";
      }else{
        return $resultParam->result[0];
      }


  }

  public function getField($urls){

    $url = config('custom.api_url').$urls;

    date_default_timezone_set("Asia/Jakarta");

    $content_type = "application/json";

    // dd($url);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4000);

      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);
      $resultParam = json_decode($output);
      //return $asd;
      //dd($resultParam);
      if($resultParam==null){
        if($urls=="/datasnap/rest/TRESTMethods/versioninfo"){
          $asd = "Software Version : -,Aplication Version : -";
        }else{
          $asd = $resultParam->result[0];

        }
      }else{

        $asd = $resultParam->result[0];
      }


      return $asd;

  }

  public function getIndex($urls){

    $url = config('custom.api_url').$urls;

    date_default_timezone_set("Asia/Jakarta");

    $content_type = "application/json";

    // dd($url);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 4000);

      $output = curl_exec($ch);
      $info = curl_getinfo($ch);
      $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

      curl_close($ch);
      $asd = json_decode($output);
       //dd($urls);
      return $asd->result[0];

  }

}

?>
