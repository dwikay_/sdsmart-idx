<?php
namespace App\Library;

use App\Model\activityUser;
use App\Model\Role;
use Auth;
use Session;

class activityLog {

  public function logUser($activitys, $desc){

    date_default_timezone_set('asia/jakarta');

    $cek_role = Role::find(Session::get('role_id'));
    //dd($cek_role);
    $activity = new activityUser();
    $activity->username = Session::get('email');
    $activity->group = $cek_role->role_name;
    $activity->activity = $activitys;
    $activity->activityDescription = $desc;
    $activity->date = date('Y-m-d');
    $activity->time = date('H:i:s');
    $activity->save();
  }

}
?>
