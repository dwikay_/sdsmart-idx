<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Module extends Model implements Auditable
{
  use \OwenIt\Auditing\Auditable;
  protected $table = 'modules';
  protected $primaryKey = 'id';

  protected $fillable = [
    'menu_parent',
    'module_name',
    'menu_mask',
    'menu_path',
    'menu_icon',
    'menu_order',
    'divider'
  ];
}
