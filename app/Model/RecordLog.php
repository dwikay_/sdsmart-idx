<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RecordLog extends Model
{
  protected $table = 'record_log';
  protected $primaryKey = 'RECORDID';
  public $timestamps = false;
  
  protected $fillable = [
    'TASKID',
    'TASKNAME',
    'RECORDCOUNT',
    'RECORDDATE',
    'INSUI',
    'INSDA',
    'MODUI',
    'MODDA'
  ];
}
