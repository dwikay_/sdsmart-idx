<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Role extends Model implements Auditable
{
  use \OwenIt\Auditing\Auditable;
  protected $table = 'roles';
  protected $primaryKey = 'id';

  protected $fillable = [
    'role_name',
    'description',
    'akses'
  ];
}
