<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @property int $DBCONNID
 * @property string $DBSERVERNAME
 * @property string $DBPROVIDER
 * @property string $DBHOSTNAME
 * @property int $DBPORT
 * @property string $DBNAME
 * @property string $DBUSERID
 * @property string $DBPASSWORD
 * @property string $INSUI
 * @property string $INSDA
 * @property string $MODUI
 * @property string $MODDA
 */
class AdmDBCon extends Model implements Auditable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    use \OwenIt\Auditing\Auditable;

    protected $table = 'adm_dbcon';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'DBCONNID';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['DBSERVERNAME', 'DBPROVIDER', 'DBHOSTNAME', 'DBPORT', 'DBNAME', 'DBUSERID', 'DBPASSWORD', 'INSUI', 'INSDA', 'MODUI', 'MODDA','TYPEDB'];

}
