<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @property int $ROWNUMID
 * @property int $TASKID
 * @property int $ROWNUM
 * @property string $INSUI
 * @property string $INSDA
 * @property string $MODUI
 * @property string $MODDA
 */
class AdmRowNum extends Model implements Auditable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
     use \OwenIt\Auditing\Auditable;
    protected $table = 'adm_rownum';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ROWNUMID';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['TASKID', 'ROWNUM', 'INSUI', 'INSDA', 'MODUI', 'MODDA'];

}
