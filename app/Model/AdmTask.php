<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @property int $TASKID
 * @property string $TASKNAME
 * @property int $TASKINTERVAL
 * @property int $ISROWNUM
 * @property int $DBSOURCEID
 * @property int $DBDESTID
 * @property string $INSUI
 * @property string $INSDA
 * @property string $MODUI
 * @property string $MODDA
 */
class AdmTask extends Model implements Auditable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    use \OwenIt\Auditing\Auditable;
    protected $table = 'adm_task';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'TASKID';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['TASKID','TASKNAME', 'TASKINTERVAL', 'ISROWNUM', 'DBSOURCEID', 'DBDESTID', 'INSUI', 'INSDA', 'MODUI', 'MODDA'];

    public function dbsource()
    {
        return $this->belongsTo('App\Model\AdmDBCon', 'DBSOURCEID', 'DBCONNID');
    }

    public function dbtarget()
    {
        return $this->belongsTo('App\Model\AdmDBCon', 'DBDESTID', 'DBCONNID');
    }

    public function mapTable(){
        return $this->belongsTo('App\Model\AdmMapTable', 'TASKID', 'TASKID');
    }

    public function rownum(){
        return $this->belongsTo('App\Model\AdmRowNum', 'TASKID', 'TASKID');
    }

    public function timestamp(){
        return $this->belongsTo('App\Model\AdmTimeStamp', 'TASKID', 'TASKID');
    }

}
