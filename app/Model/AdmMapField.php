<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
/**
 * @property int $MAPFIELDID
 * @property int $MAPTBLID
 * @property string $FIELDSOURCE
 * @property string $FIELDDEST
 * @property string $FIELDDESTTYPE
 * @property string $INSUI
 * @property string $INSDA
 * @property string $MODUI
 * @property string $MODDA
 */
class AdmMapField extends Model implements Auditable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    use \OwenIt\Auditing\Auditable;

    protected $table = 'adm_map_field';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'MAPFIELDID';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['MAPTBLID', 'FIELDSOURCE', 'FIELDDEST', 'FIELDDESTTYPE', 'INSUI', 'INSDA', 'MODUI', 'MODDA','ISPRIMARYKEYSOURCE','ISPRIMARYKEYDEST','FIELDSOURCETYPE'];

    // public function mapTable()
    // {
    //     return $this->belongsTo('App\Model\AdmMapTable', 'MAPTBLID', 'MAPTBLID');
    // }

}
