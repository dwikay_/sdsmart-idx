<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * @property int $TIMESTAMPID
 * @property int $TASKID
 * @property string $TIMESTART
 * @property string $INSUI
 * @property string $INSDA
 * @property string $MODUI
 * @property string $MODDA
 */
class AdmTimeStamp extends Model implements Auditable
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    use \OwenIt\Auditing\Auditable;
    protected $table = 'adm_timestamp';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'TIMESTAMPID';
    public $timestamps = false;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['TASKID', 'TIMESTART', 'INSUI', 'INSDA', 'MODUI', 'MODDA'];

}
