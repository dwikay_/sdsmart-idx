<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Log extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    protected $table = 'view_log';
    protected $primaryKey = 'LOGID';

    public $timestamps = false;

    protected $fillable = [
      'TASKID',
      'TASKNAME',
      'DBSOURCEHOST',
      'DBDESTHOST',
      'TBLSOURCE',
      'TBLDEST',
      'SCRIPTQUERY',
      'PKSOURCE',
      'PKDESTINATION',
      'RECORDCOUNT',
      'INSUI',
      'INSDA',
      'MODUI',
      'MODDA'
    ];
}
