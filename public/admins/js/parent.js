$( document ).ready(function(){
    initAutoNumeric();
    initDatePicker();
    changeInputSmall();
	$('input').on('focus',function(){
        $(this).attr('autocomplete', 'off');
    });
}); 

function initAutoNumeric(){
    $('.percent').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            new AutoNumeric(this,{
                currencySymbol: "%",
                currencySymbolPlacement: "s",
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalPlaces: 2,
                unformatOnSubmit: true,
                watchExternalChanges: true
            });
        }
    });
    $('.percentReadOnly').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            new AutoNumeric(this,{
                currencySymbol: "%",
                currencySymbolPlacement: "s",
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalPlaces: 2,
                noEventListeners: true,
                readOnly: true,
                unformatOnSubmit: true,
                watchExternalChanges: true
            });
        }
    });
    $('.percentNoComma').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            new AutoNumeric(this,{
                currencySymbol: "%",
                currencySymbolPlacement: "s",
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalPlaces: 0,
                unformatOnSubmit: true,
                watchExternalChanges: true
            });
        }
    });
    $('.percentNoCommaReadOnly').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            new AutoNumeric(this,{
                currencySymbol: "%",
                currencySymbolPlacement: "s",
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalPlaces: 0,
                noEventListeners: true,
                readOnly: true,
                unformatOnSubmit: true,
                watchExternalChanges: true
            });
        }
    });
    $('.currency').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            new AutoNumeric(this,{
                currencySymbol: "",
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalPlaces: 2,
                unformatOnSubmit: true,
                watchExternalChanges: true
            });
        }
    });
    $('.currencyNoComma').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            if($(this).is('input:not([readonly])')){
                new AutoNumeric(this,{
                    currencySymbol: "",
                    digitGroupSeparator: '.',
                    decimalCharacter: ',',
                    decimalPlaces: 0,
                    unformatOnSubmit: true,
                    watchExternalChanges: true,
                    minimumValue:0
                });
            }else{
                new AutoNumeric(this,{
                    currencySymbol: "",
                    digitGroupSeparator: '.',
                    decimalCharacter: ',',
                    decimalPlaces: 0,
                    unformatOnSubmit: true,
                    watchExternalChanges: true,
                });
            }
        }
    });
    $('.currencyNoCommaReadOnly').each(function(){
        if(!AutoNumeric.isManagedByAutoNumeric(this)){
            new AutoNumeric(this,{
                currencySymbol: "",
                digitGroupSeparator: '.',
                decimalCharacter: ',',
                decimalPlaces: 0,
                noEventListeners: true,
                readOnly: true,
                unformatOnSubmit: true,
                watchExternalChanges: true
            });
        }
    });
}

function initDatePicker(){
    $(".datepicker").datepicker({
        format: 'dd-M-yyyy',
        daysOfWeekDisabled: '0,6',
    });

    $(".datepicker").each(function(){
        dateStr = $(this).val();
        if(dateStr.length > 0){
            $(this).datepicker("setDate", new Date(dateStr));   
        }
    });
    //$(".datepicker").datepicker("setDate", new Date());
}

function changeInputSmall(){
    $('select.form-control').each(function(){
        $(this).addClass('form-control-sm');
    });
    $('input.form-control').each(function(){
        $(this).addClass('form-control-sm');
    });

    $('.input-group').each(function(){
        $(this).addClass('input-group-sm');
    })
}