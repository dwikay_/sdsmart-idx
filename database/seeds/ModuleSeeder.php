<?php

use Illuminate\Database\Seeder;
use App\Model\Module as Module;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      Module::create([
          'menu_parent' =>  '0',
          'module_name' =>  'Monitoring Jobs',
          'menu_mask' =>  'Monitoring Jobs',
          'menu_path' =>  '',
          'menu_icon' =>  'icon-screen-desktop',
          'menu_order'  =>  '1',
          'divider' =>  0,
      ]);
      Module::create([
          'menu_parent' =>  '0',
          'module_name' =>  'Management Job',
          'menu_mask' =>  'Management Job',
          'menu_path' =>  '',
          'menu_icon' =>  'icon-briefcase',
          'menu_order'  =>  '2',
          'divider' =>  0,
      ]);
      Module::create([
          'menu_parent' =>  '0',
          'module_name' =>  'Log Manager',
          'menu_mask' =>  'Log Manager',
          'menu_path' =>  '',
          'menu_icon' =>  'icon-docs',
          'menu_order'  =>  '3',
          'divider' =>  0,
      ]);
      Module::create([
          'menu_parent' =>  '0',
          'module_name' =>  'User Management',
          'menu_mask' =>  'User Management',
          'menu_path' =>  '',
          'menu_icon' =>  'icon-setting',
          'menu_order'  =>  '4',
          'divider' =>  0,
      ]);

      //Monitoring Child
      Module::create([
          'menu_parent' =>  '1',
          'module_name' =>  'Monitoring Job',
          'menu_mask' =>  'Monitoring Job',
          'menu_path' =>  'monitoring/monJobs',
          'menu_icon' =>  '',
          'menu_order'  =>  '1',
          'divider' =>  0,
      ]);

      //Management Jobs Child
      Module::create([
          'menu_parent' =>  '2',
          'module_name' =>  'DB Config',
          'menu_mask' =>  'DB Config',
          'menu_path' =>  'management/config',
          'menu_icon' =>  '',
          'menu_order'  =>  '1',
          'divider' =>  0,
      ]);
      Module::create([
          'menu_parent' =>  '2',
          'module_name' =>  'DB Config',
          'menu_mask' =>  'DB Config',
          'menu_path' =>  'management/mapping/table',
          'menu_icon' =>  '',
          'menu_order'  =>  '2',
          'divider' =>  0,
      ]);
      Module::create([
          'menu_parent' =>  '2',
          'module_name' =>  'DB Config',
          'menu_mask' =>  'DB Config',
          'menu_path' =>  'management/job/manual-run',
          'menu_icon' =>  '',
          'menu_order'  =>  '3',
          'divider' =>  0,
      ]);

      //Log Manager Child
      Module::create([
          'menu_parent' =>  '3',
          'module_name' =>  'Log Jobs',
          'menu_mask' =>  'Log Jobs',
          'menu_path' =>  'log/logJob',
          'menu_icon' =>  '',
          'menu_order'  =>  '1',
          'divider' =>  0,
      ]);
      Module::create([
          'menu_parent' =>  '3',
          'module_name' =>  'Backup Log Jobs',
          'menu_mask' =>  'Backup Log Jobs',
          'menu_path' =>  'log/logBackup',
          'menu_icon' =>  '',
          'menu_order'  =>  '2',
          'divider' =>  0,
      ]);

      //Administrator

      Module::create([
          'menu_parent' =>  '4',
          'module_name' =>  'User Management',
          'menu_mask' =>  'User Management',
          'menu_path' =>  'users/management',
          'menu_icon' =>  '',
          'menu_order'  =>  '1',
          'divider' =>  0,
      ]);

      Module::create([
          'menu_parent' =>  '4',
          'module_name' =>  'User Group',
          'menu_mask' =>  'User Group',
          'menu_path' =>  'users/group',
          'menu_icon' =>  '',
          'menu_order'  =>  '2',
          'divider' =>  0,
      ]);

      Module::create([
          'menu_parent' =>  '4',
          'module_name' =>  'User Activities Log',
          'menu_mask' =>  'User Activities Log',
          'menu_path' =>  'users/activitiesLog',
          'menu_icon' =>  '',
          'menu_order'  =>  '3',
          'divider' =>  0,
      ]);

      Module::create([
          'menu_parent' =>  '4',
          'module_name' =>  'Backup User Activities Log',
          'menu_mask' =>  'Backup User Activities Log',
          'menu_path' =>  'users/logBackup',
          'menu_icon' =>  '',
          'menu_order'  =>  '4',
          'divider' =>  0,
      ]);

    }
}
